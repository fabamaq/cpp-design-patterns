/******************************************************************************
 * @file StandardLibrary.hpp
 * @copyright Copyright (c) 2020 Fabamaq. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#pragma once

// C Standard Library
#include <cmath>
//#include <cstddef>
#include <cstdint>
#include <cstdio>
//#include <cstring>

// C++ Standard Library
#include <algorithm>
#include <array>
//#include <atomic>
//#include <bitset>
//#include <chrono>
//#include <deque>
#include <exception>
//#include <fstream>
#include <functional>
#if defined(__cplusplus) && __cplusplus > 201703L
//#include <filesystem>
#endif
#include <iostream>
//#include <iterator>
//#include <limits>
//#include <list>
//#include <map>
#include <memory>
////#include <new>
//#include <numeric>
#include <random>
//#include <set>
//#include <sstream>
//#include <stdexcept>
//#include <string>
#if defined(__cplusplus) && __cplusplus > 201703L
//#include <string_view>
#endif
////#include <thread>
//#include <type_traits>
#include <unordered_map>
//#include <unordered_set>
//#include <utility>
#include <vector>

/// @note C++11 make_unique polyfill
#if defined(__cplusplus) && __cplusplus < 201401L
namespace fmq {
	template <typename T, typename... Ts>
	std::unique_ptr<T> make_unique(Ts &&...params) {
		return std::unique_ptr<T>(new T(std::forward<T>(params)...));
	}
} // namespace fmq
using fmq::make_unique;
#else
using std::make_unique;
#endif
