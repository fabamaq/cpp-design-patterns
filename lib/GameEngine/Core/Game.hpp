#pragma once

/// ===========================================================================
/// @implements Singleton design pattern
/// ===========================================================================
class Game {
  public:
	static Game &GetInstance() { return *sInstance; }

	virtual ~Game() = default;

	virtual int OnStaticMain() = 0;
	virtual int OnStaticExit() = 0;

  protected:
	Game() {}
	Game(const Game &) = delete;
	Game &operator=(const Game &) = delete;
	Game(Game &&)				  = default;
	Game &operator=(Game &&) = default;

	static Game *sInstance;
};

/// ===========================================================================
/// @implements Factory Method design pattern
/// @todo implement in a specific game
/// ===========================================================================
Game *CreateGame();
