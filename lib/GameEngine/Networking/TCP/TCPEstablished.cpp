/******************************************************************************
 * @file TCPEstablished.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "TCPEstablished.hpp"
#include "TCPListen.hpp"

TCPState *TCPEstablished::_instance = nullptr;

TCPState *TCPEstablished::Instance() {
    if (!_instance) {
        _instance = new TCPEstablished();
    }
    return _instance;
}

void TCPEstablished::Transmit(TCPConnection *t, TCPOctetStream *o) {
    t->ProcessOctet(o);
}

void TCPEstablished::Close(TCPConnection *t) {
    // send FIN, receive ACK of FIN
    ChangeState(t, TCPListen::Instance());
}
