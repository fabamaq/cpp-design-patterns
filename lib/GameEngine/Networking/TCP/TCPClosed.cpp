/******************************************************************************
 * @file TCPClosed.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "TCPClosed.hpp"
#include "TCPEstablished.hpp"
#include "TCPListen.hpp"

TCPState *TCPClosed::_instance = nullptr;

TCPState *TCPClosed::Instance() {
    if (!_instance) {
        _instance = new TCPClosed();
    }
    return _instance;
}

void TCPClosed::ActiveOpen(TCPConnection *t) {
    // send SYN, receive SYN, ACK, etc.
    ChangeState(t, TCPEstablished::Instance());
}

void TCPClosed::PassiveOpen(TCPConnection *t) {
    ChangeState(t, TCPListen::Instance());
}
