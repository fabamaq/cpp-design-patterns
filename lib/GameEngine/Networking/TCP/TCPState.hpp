#pragma once

#include "TCPConnection.hpp"

class TCPState {
  public:
	virtual ~TCPState() = default;
    
	virtual void Transmit(TCPConnection *, TCPOctetStream *);

	virtual void ActiveOpen(TCPConnection *);

	virtual void PassiveOpen(TCPConnection *);

	virtual void Close(TCPConnection *);

	virtual void Acknowledge(TCPConnection *);

	virtual void Synchronize(TCPConnection *);

	virtual void Send(TCPConnection *);

  protected:
	void ChangeState(TCPConnection *, TCPState *);
};
