/******************************************************************************
 * @file TCPListen.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "TCPListen.hpp"
#include "TCPEstablished.hpp"

TCPState *TCPListen::_instance = nullptr;

TCPState *TCPListen::Instance() {
    if (!_instance) {
        _instance = new TCPListen();
    }
    return _instance;
}

void TCPListen::Send(TCPConnection *t) {
    // send SYN, receive SYN, ACK, etc.

    ChangeState(t, TCPEstablished::Instance());
}
