// C++ Standard Library
#include "StandardLibrary.hpp"

// Game Engine Library
#include "GameEngine/GameEngine.hpp"

/// @implements Factory Method design pattern
extern Game *CreateGame();

/// ===========================================================================
/// @brief Application Entry Point
/// - The main function builds the objects necessary for the system,
/// ... then passes them to the application, which simply uses them.
/// ===========================================================================
int main() {
	try {
		auto game = std::unique_ptr<Game>(CreateGame());

		auto connection = TCPConnection();
		connection.ActiveOpen();

		return game->OnStaticMain();

	} catch (...) {
		return EXIT_FAILURE;
	}
}
