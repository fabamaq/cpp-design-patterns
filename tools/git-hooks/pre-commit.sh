#!/usr/bin/env bash

# =========================================================================== #
# Logger Setup
# =========================================================================== #

# Include Logger
DIRNAME=$(dirname "$0")
# shellcheck disable=SC1090
. "${DIRNAME}"/../../tools/scripts/sh/Logger.sh

SCRIPT_NAME=$(awk -F/ '{printf $NF}' <<<"$0")
CMAKE_CURRENT_LOG_LEVEL=${CMAKE_LOG_LEVEL_DEBUG}

function _LogDebug() {
    if [ "${CMAKE_CURRENT_LOG_LEVEL}" -le "${CMAKE_LOG_LEVEL_DEBUG}" ]; then
        LogDebug "${SCRIPT_NAME}" "$1"
    fi
}
function _LogInfo() {
    if [ "${CMAKE_CURRENT_LOG_LEVEL}" -le "${CMAKE_LOG_LEVEL_INFO}" ]; then
        LogInfo "${SCRIPT_NAME}" "$1"
    fi
}
function _LogWarn() {
    if [ "${CMAKE_CURRENT_LOG_LEVEL}" -le "${CMAKE_LOG_LEVEL_WARNING}" ]; then
        LogWarn "${SCRIPT_NAME}" "$1"
    fi
}
function _LogError() {
    if [ "${CMAKE_CURRENT_LOG_LEVEL}" -le "${CMAKE_LOG_LEVEL_ERROR}" ]; then
        LogError "${SCRIPT_NAME}" "$1"
    fi
}

# =========================================================================== #
# Initial Setup
# =========================================================================== #

