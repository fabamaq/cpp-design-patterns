/******************************************************************************
 *
 * @file UpdateMethodPatternTest.cpp
 *
 * @brief Update Method design pattern
 * - "Simulate a collectino of independent objects by
 * ... telling each to process one frame of behavior at a time"
 *****************************************************************************/


#include <gtest/gtest.h>

class Entity {
    double _x;
public:
    void setX(double x) { _x = x; }
};

TEST(UpdateMethodPatternTest, Unimplemented) {
    Entity skeleton;
    bool patrollingLeft = false;
    double x = 0;

    // safety net
    int counter = 600;

    // Main game loop:
    while (counter > 0) {
        if (patrollingLeft) {
            x--;
            if (x == 0) patrollingLeft = false;
        } else {
            x++;
            if (x == 100) patrollingLeft = true;
        }

        skeleton.setX(x);

        // Handle user input and render game...

        --counter;
    }
}