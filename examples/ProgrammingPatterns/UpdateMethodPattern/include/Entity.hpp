#ifndef GAMEPROGRAMMINGPATTERNS_ENTITY_HPP
#define GAMEPROGRAMMINGPATTERNS_ENTITY_HPP

class Entity {
public:
    Entity()
    : x_(0), y_(0) {}

    virtual ~Entity() {}
    virtual void update() = 0;

    double x() const { return x_; }
    double y() const { return y_; }

    void setX(double x) { x_ = x; }
    void setY(double y) { y_ = y; }

private:
    double x_, y_;
};

#endif //GAMEPROGRAMMINGPATTERNS_ENTITY_HPP
