#ifndef GAMEPROGRAMMINGPATTERNS_SKELETON_HPP
#define GAMEPROGRAMMINGPATTERNS_SKELETON_HPP

#include <Entity.hpp>

class Skeleton : public Entity {
public:
    Skeleton()
            : patrollingLeft_(false) {}

    virtual void update() {
        if (patrollingLeft_) {
            setX(x() - 1);
            if (x() == 0) patrollingLeft_ = false;
        } else {
            setX(x() + 1);
            if (x() == 100) patrollingLeft_ = true;
        }
    }

    // Take into account variable time step
    void update(double elapsed) {
        if (patrollingLeft_) {
            x -= elapsed;
            if (x <= 0) {
                patrollingLeft_ = false;
                x = -x;
            }
        } else {
            x += elapsed;
            if (x >= 100) {
                patrollingLeft_ = true;
                x = 100 - (x - 100);
            }
        }
    }

    // Using the State pattern
    // void update() {
    //  // Forward to state object
    //  state_->update();
    // }

private:
    bool patrollingLeft_;
};

#endif //GAMEPROGRAMMINGPATTERNS_SKELETON_HPP
