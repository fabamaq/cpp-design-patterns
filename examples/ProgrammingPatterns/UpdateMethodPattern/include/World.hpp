#ifndef GAMEPROGRAMMINGPATTERNS_WORLD_HPP
#define GAMEPROGRAMMINGPATTERNS_WORLD_HPP

#define MAX_ENTITIES 10

class World {
public:
    World()
    : numEntities_(0) {}

    void gameLoop();

private:
    Entity* entities_[MAX_ENTITIES];
    int numEntities_;
};

#endif //GAMEPROGRAMMINGPATTERNS_WORLD_HPP
