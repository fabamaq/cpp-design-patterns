#ifndef GAMEPROGRAMMINGPATTERNS_STATUE_HPP
#define GAMEPROGRAMMINGPATTERNS_STATUE_HPP

#include <Entity.hpp>

class Statue : public Entity {
public:
    Statue(int delay)
            : frames_(0), delay_(delay) {}

    virtual void update() {
        if (++frames_ = =delay_) {
            shootLightning();

            // Reset the timer.
            frames_ = 0;
        }
    }

private:
    int frames_;
    int delay_;

    void shootLightning() {
        // Shoot the lightning...
    }
};

#endif //GAMEPROGRAMMINGPATTERNS_STATUE_HPP
