#include <Heroine.hpp>
#include <HeroineState.hpp>
#include <StandingState.hpp>

Heroine::Heroine()
        : yVelocity_(0.0), graphics_(IMAGE_STAND), state_(new StandingState()), equipment_(nullptr) {}

void Heroine::handleInput(Input input) {
    // TODO: Apply Chain of Responsibility design pattern
    HeroineState *state = state_->handleInput(*this, input);
    HeroineState *equipment = nullptr;

    if (equipment_ != nullptr) {
        equipment = equipment_->handleInput(*this, input);
    }

    if (state != nullptr) {
        // state_->exit(...);
        delete state_;
        state_ = state;

        state_->enter(*this);
    } else if (equipment != nullptr) {
        // equipment_->exit(...);
        delete equipment_;
        equipment_ = equipment;

        // TODO: Pass Equipment instead of Heroine
        equipment_->enter(*this);
    }
}

void Heroine::update() {
    state_->update(*this);
}

HeroineState *Heroine::getState() const {
    return state_;
}

Graphics Heroine::getGrahics() const {
    return graphics_;
}

void Heroine::setGraphics(Graphics graphics) {
    graphics_ = graphics;
}

void Heroine::superBomb() {

}
