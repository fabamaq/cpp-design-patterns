#include <HeroineState.hpp>
#include <StandingState.hpp>

HeroineState::~HeroineState() = default;

HeroineState *HeroineState::handleInput(Heroine &heroine, Input input) {
    return new StandingState();
}

void HeroineState::update(Heroine &heroine) {}

void HeroineState::enter(Heroine &heroine) {}