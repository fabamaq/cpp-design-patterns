#include <StandingState.hpp>
#include <DuckingState.hpp>
#include <JumpingState.hpp>

StandingState::StandingState() = default;

HeroineState *StandingState::handleInput(Heroine &heroine, Input input) {
    if (input == PRESS_DOWN) {
        // Other code
//        heroine.setGraphics(IMAGE_DUCK);
        return new DuckingState();
    } else if (input == PRESS_B) {
//        heroine.setGraphics(IMAGE_JUMP);
        return new JumpingState();
    }

    // Stay in this state
    return nullptr;
}

void StandingState::update(Heroine &heroine) {
    HeroineState::update(heroine);
}

void StandingState::enter(Heroine &heroine) {
    heroine.setGraphics(IMAGE_STAND);
}
