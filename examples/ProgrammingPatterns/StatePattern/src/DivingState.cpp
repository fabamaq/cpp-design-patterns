#include <DivingState.hpp>

DivingState::DivingState() = default;

HeroineState *DivingState::handleInput(Heroine &heroine, Input input) {
    return HeroineState::handleInput(heroine, input);
}

void DivingState::update(Heroine &heroine) {
    HeroineState::update(heroine);
}

void DivingState::enter(Heroine &heroine) {
    heroine.setGraphics(IMAGE_DIVING);
}
