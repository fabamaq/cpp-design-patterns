#include <JumpingState.hpp>
#include <DivingState.hpp>

JumpingState::JumpingState() = default;

HeroineState *JumpingState::handleInput(Heroine &heroine, Input input) {
    if (input == PRESS_DOWN) {
        return new DivingState();
    }

    return nullptr;
}

void JumpingState::update(Heroine &heroine) {
    HeroineState::update(heroine);
}

void JumpingState::enter(Heroine &heroine) {
    heroine.setGraphics(IMAGE_JUMP);
}
