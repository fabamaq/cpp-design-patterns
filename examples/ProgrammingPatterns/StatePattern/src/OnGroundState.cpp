#include <OnGroundState.hpp>
#include <JumpingState.hpp>
#include <DuckingState.hpp>

HeroineState *OnGroundState::handleInput(Heroine &heroine, Input input) {
    if (input == PRESS_B) {
        return new JumpingState();
    }
    else if (input == PRESS_DOWN) {
        return new DuckingState();
    }
    else {
        return nullptr;
    }
}
