#include <DuckingState.hpp>

#include <StandingState.hpp>

DuckingState::DuckingState() = default;

HeroineState *DuckingState::handleInput(Heroine &heroine, Input input) {
    if (input == RELEASE_DOWN) {
        return new StandingState();
    }
    else {
        // Didn't handle input, so walk up hierarchy.
        return OnGroundState::handleInput(heroine, input);
    }
}

void DuckingState::update(Heroine &heroine) {
    chargeTime_++;
    if (chargeTime_ > MAX_CHARGE) {
        heroine.superBomb();
    }
}

void DuckingState::enter(Heroine &heroine) {
    heroine.setGraphics(IMAGE_DUCK);
}
