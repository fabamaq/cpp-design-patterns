/******************************************************************************
 *
 * @file StatePatternTest.cpp
 *
 * @brief State design pattern
 * - "Allow an object to alter its behaviour when its internal state changes.
 * ... The object will appear to change its class."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// State includes
#include <Heroine.hpp>
#include <DuckingState.hpp>
#include <StandingState.hpp>
#include <JumpingState.hpp>
#include <DivingState.hpp>

#include <typeinfo>

#include <gtest/gtest.h>

bool operator==(const HeroineState &lhs, const HeroineState &rhs) {
    return typeid(lhs).name() == typeid(rhs).name();
}

bool operator!=(const HeroineState &lhs, const HeroineState &rhs) {
    return !(lhs == rhs);
}

TEST(StatePatternTest, StateEquality) {
    HeroineState *s1 = new StandingState();
    HeroineState *s2 = new StandingState();
    EXPECT_EQ(*s1, *s2);
}

TEST(StatePatternTest, StateInequality) {
    HeroineState *s1 = new StandingState();
    HeroineState *s2 = new DuckingState();
    EXPECT_NE(*s1, *s2);
}

TEST(StatePatternTest, HeroineTest) {
    // SetUp
    HeroineState* expectedState;
    HeroineState* returnedState;

    // Arrange
    Heroine heroine;
    expectedState = new StandingState();
    returnedState = heroine.getState();
    ASSERT_NE(nullptr, expectedState);
    ASSERT_NE(nullptr, returnedState);
    EXPECT_EQ(*expectedState, *returnedState);
    EXPECT_EQ(IMAGE_STAND, heroine.getGrahics());

    // Act
    Input input = PRESS_DOWN;
    heroine.handleInput(input);

    // Assert
    expectedState = new DuckingState();
    returnedState = heroine.getState();
    ASSERT_NE(nullptr, expectedState);
    ASSERT_NE(nullptr, returnedState);
    EXPECT_EQ(*expectedState, *returnedState);
    EXPECT_EQ(IMAGE_DUCK, heroine.getGrahics());

    // Act
    input = RELEASE_DOWN;
    heroine.handleInput(input);

    // Assert
    expectedState = new StandingState();
    returnedState = heroine.getState();
    ASSERT_NE(nullptr, expectedState);
    ASSERT_NE(nullptr, returnedState);
    EXPECT_EQ(*expectedState, *returnedState);
    EXPECT_EQ(IMAGE_STAND, heroine.getGrahics());

    // Act
    input = PRESS_B;
    heroine.handleInput(input);

    // Assert
    expectedState = new JumpingState();
    returnedState = heroine.getState();
    ASSERT_NE(nullptr, expectedState);
    ASSERT_NE(nullptr, returnedState);
    EXPECT_EQ(*expectedState, *returnedState);
    EXPECT_EQ(IMAGE_JUMP, heroine.getGrahics());

    // Act
    input = PRESS_DOWN;
    heroine.handleInput(input);

    // Assert
    expectedState = new DivingState();
    returnedState = heroine.getState();
    ASSERT_NE(nullptr, expectedState);
    ASSERT_NE(nullptr, returnedState);
    EXPECT_EQ(*expectedState, *returnedState);
    EXPECT_EQ(IMAGE_DIVING, heroine.getGrahics());

    // TearDown
    delete expectedState;
    delete returnedState;
}
