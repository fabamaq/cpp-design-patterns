#ifndef GAMEPROGRAMMINGPATTERNS_ONGROUNDSTATE_HPP
#define GAMEPROGRAMMINGPATTERNS_ONGROUNDSTATE_HPP

#include <HeroineState.hpp>

class OnGroundState : public HeroineState {
public:
    virtual HeroineState* handleInput(Heroine& heroine, Input input);
};

#endif //GAMEPROGRAMMINGPATTERNS_ONGROUNDSTATE_HPP
