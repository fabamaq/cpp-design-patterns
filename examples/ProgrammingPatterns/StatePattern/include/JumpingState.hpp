#ifndef GAMEPROGRAMMINGPATTERNS_JUMPINGSTATE_HPP
#define GAMEPROGRAMMINGPATTERNS_JUMPINGSTATE_HPP

#include <HeroineState.hpp>
#include <Heroine.hpp>

class JumpingState : public HeroineState {
public:
    JumpingState();

    virtual HeroineState *handleInput(Heroine &heroine, Input input);

    virtual void update(Heroine &heroine);

    void enter(Heroine &heroine) override;
};

#endif //GAMEPROGRAMMINGPATTERNS_JUMPINGSTATE_HPP
