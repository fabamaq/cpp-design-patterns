#ifndef GAMEPROGRAMMINGPATTERNS_DUCKINGSTATE_HPP
#define GAMEPROGRAMMINGPATTERNS_DUCKINGSTATE_HPP

#include <HeroineState.hpp>
#include <OnGroundState.hpp>
#include <Heroine.hpp>

class DuckingState : public OnGroundState {
public:
    DuckingState();

    virtual HeroineState *handleInput(Heroine &heroine, Input input);

    virtual void update(Heroine &heroine);

    void enter(Heroine &heroine) override;

private:
    int chargeTime_;
};

#endif //G{}AMEPROGRAMMINGPATTERNS_DUCKINGSTATE_HPP
