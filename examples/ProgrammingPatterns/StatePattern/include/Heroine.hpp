#ifndef GAMEPROGRAMMINGPATTERNS_HEROINE_HPP
#define GAMEPROGRAMMINGPATTERNS_HEROINE_HPP

class HeroineState;
#include <Graphics.hpp>
#include <Input.hpp>

#include <iostream>

const double MAX_CHARGE = 10.0;
const double JUMP_VELOCITY = 12.0;

class Heroine {
    double yVelocity_;
    Graphics graphics_;
    HeroineState *state_;
    HeroineState *equipment_;
public:
    Heroine();

    void handleInput(Input input);

    // Update Method design pattern
    void update();

    HeroineState* getState() const;
    Graphics getGrahics() const;

    void setGraphics(Graphics graphics);

    void superBomb();
};


#endif //GAMEPROGRAMMINGPATTERNS_HEROINE_HPP
