#ifndef GAMEPROGRAMMINGPATTERNS_DIVINGSTATE_HPP
#define GAMEPROGRAMMINGPATTERNS_DIVINGSTATE_HPP

#include <HeroineState.hpp>
#include <Heroine.hpp>

class DivingState : public HeroineState {
public:
    DivingState();

    virtual HeroineState *handleInput(Heroine &heroine, Input input);

    virtual void update(Heroine &heroine);

    void enter(Heroine &heroine) override;
};

#endif //GAMEPROGRAMMINGPATTERNS_DIVINGSTATE_HPP
