#ifndef GAMEPROGRAMMINGPATTERNS_STANDINGSTATE_HPP
#define GAMEPROGRAMMINGPATTERNS_STANDINGSTATE_HPP

#include <HeroineState.hpp>
#include <OnGroundState.hpp>
#include <Heroine.hpp>

class StandingState : public OnGroundState {
public:
    StandingState();

    virtual void enter(Heroine& heroine);

    virtual HeroineState *handleInput(Heroine &heroine, Input input);

    virtual void update(Heroine &heroine);
};

#endif //GAMEPROGRAMMINGPATTERNS_STANDINGSTATE_HPP
