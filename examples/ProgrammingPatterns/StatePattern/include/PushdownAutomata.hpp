#ifndef GAMEPROGRAMMINGPATTERNS_PUSHDOWNAUTOMATA_HPP
#define GAMEPROGRAMMINGPATTERNS_PUSHDOWNAUTOMATA_HPP

#include <stack>

class AutomataState {};

class PushdownAutomata {
    std::stack<AutomataState*> states_;
};

#endif //GAMEPROGRAMMINGPATTERNS_PUSHDOWNAUTOMATA_HPP
