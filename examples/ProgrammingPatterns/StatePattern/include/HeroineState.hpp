#ifndef GAMEPROGRAMMINGPATTERNS_HEROINESTATE_HPP
#define GAMEPROGRAMMINGPATTERNS_HEROINESTATE_HPP

#include <Heroine.hpp>

class HeroineState {
public:
    virtual ~HeroineState();
    virtual HeroineState* handleInput(Heroine& heroine, Input input);
    virtual void update(Heroine& heroine);
    virtual void enter(Heroine& heroine);
};

#endif //GAMEPROGRAMMINGPATTERNS_HEROINESTATE_HPP
