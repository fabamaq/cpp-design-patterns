/******************************************************************************
 *
 * @file PrototypePatternTest.cpp
 *
 * @brief Prototype design pattern
 * - "Specify the kinds of objects to create using a prototypical instance,
 * ... and create new objects by copying this prototype."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include <Monster.hpp>
#include <Ghost.hpp>
#include <Demon.hpp>
#include <Sorcerer.hpp>
#include <Spawner.hpp>

#include <gtest/gtest.h>

#define SUBCLASS(T, obj) dynamic_cast<T*>(obj)

static const int kDefaultHealth = 10;

// SpawnCallback
Monster* spawnDemon() {
    return new Demon(kDefaultHealth);
}

TEST(PrototypePatternTest, CloneVersion) {
    Monster *ghostPrototype = new Ghost(kDefaultHealth, 3);
    Spawner *ghostSpawner = new Spawner(ghostPrototype);
    Monster *ghost = ghostSpawner->cloneMonster();
    ASSERT_NE(nullptr, ghostPrototype);
    ASSERT_NE(nullptr, ghostSpawner);
    ASSERT_NE(nullptr, ghost);
    EXPECT_EQ(SUBCLASS(Ghost, ghostPrototype)->getHealth(),
              SUBCLASS(Ghost, ghost)->getHealth()
    );
    delete ghost;
    delete ghostSpawner;
    delete ghostPrototype;
}

TEST(PrototypePatternTest, CallbackVersion) {
    Spawner *demonSpawner = new Spawner(spawnDemon);
    Monster *demon = demonSpawner->callSpawnFunction();
    ASSERT_NE(nullptr, demonSpawner);
    ASSERT_NE(nullptr, demon);
    EXPECT_EQ(kDefaultHealth, demon->getHealth());
    delete demon;
    delete demonSpawner;
}

TEST(PrototypePatternTest, TemplateVersion) {
    Spawner *sorcererSpawner = new SpawnerFor<Sorcerer>(10);
    Monster *sorcerer = sorcererSpawner->spawnTemplate();
    ASSERT_NE(nullptr, sorcererSpawner);
    ASSERT_NE(nullptr, sorcerer);
    EXPECT_EQ(kDefaultHealth, sorcerer->getHealth());
    delete sorcerer;
    delete sorcererSpawner;
}
