#ifndef GAMEPROGRAMMINGPATTERNS_DEMON_HPP
#define GAMEPROGRAMMINGPATTERNS_DEMON_HPP

#include <Monster.hpp>

class Demon : public Monster {
public:
    Demon(int health) {
        this->health_ = health;
    }

    virtual Monster *clone() {
        return new Demon(this->health_);
    }
};

#endif //GAMEPROGRAMMINGPATTERNS_DEMON_HPP
