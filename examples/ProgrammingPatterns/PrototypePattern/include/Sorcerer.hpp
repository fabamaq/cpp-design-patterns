#ifndef GAMEPROGRAMMINGPATTERNS_SORCERER_HPP
#define GAMEPROGRAMMINGPATTERNS_SORCERER_HPP

#include <Monster.hpp>

class Sorcerer : public Monster {
public:
    Sorcerer(int health) { this->health_ = health; }

    virtual Monster *clone() {
        return new Sorcerer(this->health_);
    }
};

#endif //GAMEPROGRAMMINGPATTERNS_SORCERER_HPP
