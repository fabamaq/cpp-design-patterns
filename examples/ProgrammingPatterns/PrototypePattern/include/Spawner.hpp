#ifndef GAMEPROGRAMMINGPATTERNS_SPAWNER_HPP
#define GAMEPROGRAMMINGPATTERNS_SPAWNER_HPP

#include <Monster.hpp>

typedef Monster* (*SpawnCallback)();

class Spawner {
public:
    Spawner() = default;
    Spawner(SpawnCallback spawn) : spawn_(spawn) {}
    Spawner(Monster* prototype) : prototype_(prototype) {}
    virtual ~Spawner() = default;

    Monster* callSpawnFunction() { return spawn_(); }
    virtual Monster* cloneMonster() {
        return prototype_->clone();
    }

    virtual Monster* spawnTemplate() {
        return nullptr;
    }

private:
    Monster* prototype_;
    SpawnCallback spawn_;
};

template <class T>
class SpawnerFor : public Spawner {
    int health_;
public:
    SpawnerFor(int health) : health_(health) {}
    Monster* spawnTemplate() { return new T(health_); }
};

//class GhostSpawner : public Spawner {
//public:
//    virtual Monster* spawnMonster() {
//        return new Ghost();
//    }
//};
//
//class DemonSpawner : public Spawner {
//public:
//    virtual Monster* spawnMonster() {
//        return new Demon();
//    }
//};

// You get the idea...

#endif //GAMEPROGRAMMINGPATTERNS_SPAWNER_HPP
