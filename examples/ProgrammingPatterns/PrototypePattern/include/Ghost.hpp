#ifndef GAMEPROGRAMMINGPATTERNS_GHOST_HPP
#define GAMEPROGRAMMINGPATTERNS_GHOST_HPP

#include <Monster.hpp>

class Ghost : public Monster {
public:
    Ghost(int health, int speed)
            : speed_(speed) {
        this->health_ = health;
    }

    virtual Monster *clone() {
        return new Ghost(health_, speed_);
    }

    int getSpeed() const {
        return speed_;
    }

private:
    int speed_;
};

#endif //GAMEPROGRAMMINGPATTERNS_GHOST_HPP
