#ifndef GAMEPROGRAMMINGPATTERNS_MONSTER_HPP
#define GAMEPROGRAMMINGPATTERNS_MONSTER_HPP

class Monster {
public:
    virtual ~Monster() {}
    virtual Monster* clone() = 0;

    virtual int getHealth() const {
        return health_;
    }

    // Other stuff
protected:
    int health_;
};



#endif //GAMEPROGRAMMINGPATTERNS_MONSTER_HPP
