/******************************************************************************
 *
 * @file SingletonPatternTest.cpp
 *
 * @brief Singleton design pattern
 * - "Ensure a class has one instance,
 * ... and provide a global point of access to it."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Singleton includes
#include <FileSystem.hpp>

// Google Test Framework
#include <gtest/gtest.h>

#define PLAYSTATION_3 1
#define WII 2

template<class T>
bool isInstanceOf(FileSystem *subclass) {
    return dynamic_cast<T *>(subclass) != nullptr;
}

TEST(SingletonPatternTest, PS3FileSystem) {
#define PLATFORM PLAYSTATION_3
    FileSystem *fileSystem = &FileSystem::createFileSystemFor(PLAYSTATION_3);
//    FileSystem *fileSystem = &FileSystem::instance();
    EXPECT_TRUE(isInstanceOf<PS3FileSystem>(fileSystem));
#undef PLATFORM
}

TEST(SingletonPatternTest, WiiFileSystem) {
#define PLATFORM WII
    FileSystem *fileSystem = &FileSystem::createFileSystemFor(WII);
//    FileSystem *fileSystem = &FileSystem::instance();
    EXPECT_TRUE(isInstanceOf<WiiFileSystem>(fileSystem));
#undef PLATFORM
}