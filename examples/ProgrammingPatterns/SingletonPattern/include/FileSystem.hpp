#ifndef GAMEPROGRAMMINGPATTERNS_FILESYSTEM_HPP
#define GAMEPROGRAMMINGPATTERNS_FILESYSTEM_HPP

#include <iostream>

//#define PLATFORM
#define PLAYSTATION_3 1
#define WII 2

class FileSystem {
public:
    static FileSystem &createFileSystemFor(int platform);

    static FileSystem &instance();

    virtual ~FileSystem() {}

    virtual char *read(char *path) = 0;

    virtual void write(char *path, char *text) = 0;

protected:
    FileSystem() {}
};

class PS3FileSystem : public FileSystem {
public:
    char *read(char *path) override {
        // Use Sony file IO API...
        return nullptr;
    }

    void write(char *path, char *text) override {
        // Use Sony file IO API...
    }
};

class WiiFileSystem : public FileSystem {
public:
    char *read(char *path) override {
        // Use Nintendo file IO API...
        return nullptr;
    }

    void write(char *path, char *text) override {
        // Use Nintendo file IO API...
    }
};

FileSystem &FileSystem::createFileSystemFor(int platform) {
    static FileSystem *instance;
    switch (platform) {
        case PLAYSTATION_3: {
            instance = new PS3FileSystem();
            break;
        }
        case WII: {
            instance = new WiiFileSystem();
            break;
        }
        default: {
            instance = nullptr;
            break;
        }
    }
    return *instance;
}

//FileSystem &FileSystem::instance() {
//#if PLATFORM == PLAYSTATION_3
//    static FileSystem *instance = new PS3FileSystem();
//#elif PLATFORM == WII
//    static FileSystem * instance = new WiiFileSystem();
//#endif
//    return *instance;
//}

class LazyFileSystem {
public:
//    Lazy initialization
    static LazyFileSystem &instance() {
        if (instance_ == NULL) {
            instance_ = new LazyFileSystem();
        }
        return *instance_;
    }

private:
    LazyFileSystem() {
        std::cout << "LazyFileSystem()\n";
    }

    static LazyFileSystem *instance_;
};

class ModernFileSystem {
public:
    static ModernFileSystem &instance() {
        static ModernFileSystem *instance = new ModernFileSystem();
        return *instance;
    }

private:
    ModernFileSystem() {
        std::cout << "ModernFileSystem()\n";
    }
};

#endif //GAMEPROGRAMMINGPATTERNS_FILESYSTEM_HPP
