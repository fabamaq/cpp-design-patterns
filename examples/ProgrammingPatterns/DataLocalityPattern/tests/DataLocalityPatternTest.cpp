/******************************************************************************
 *
 * @file DataLocalityPatternTest.cpp
 *
 * @brief Data Locality design pattern
 * - "Accelerate memory access by arranging data to
 * ... take advantage of CPU caching."
 *****************************************************************************/
#include "AiComponent.hpp"
#include "PhysicsComponent.hpp"
#include "RenderComponent.hpp"
#include "GameEntity.hpp"
#include "ParticleSystem.hpp"

#include <gtest/gtest.h>

void badGameLoop() {
    const int numEntities = 10;
    GameEntity *entities[numEntities]{};

    bool gameOver = false;
    while (!gameOver) {
        for (int i = 0; i < numEntities; i++) {
            entities[i]->ai()->update();
        }

        for (int i = 0; i < numEntities; i++) {
            entities[i]->physics()->update();
        }

        for (int i = 0; i < numEntities; i++) {
            entities[i]->render()->render();
        }

        // Other game loop machinery for timing...
        gameOver = true;
    }
}

void goodGameLoop() {
    const int MAX_ENTITIES = 10;
    AIComponent *aiComponents = new AIComponent[MAX_ENTITIES];
    PhysicsComponent *physicsComponents = new PhysicsComponent[MAX_ENTITIES];
    RenderComponent *renderComponents = new RenderComponent[MAX_ENTITIES];

    bool gameOver = false;
    while (!gameOver) {
        for (int i = 0; i < MAX_ENTITIES; i++) {
            aiComponents[i].update();
        }

        for (int i = 0; i < MAX_ENTITIES; i++) {
            physicsComponents[i].update();
        }

        for (int i = 0; i < MAX_ENTITIES; i++) {
            renderComponents[i].render();
        }

        gameOver = true;
    }

    delete[] aiComponents;
    delete[] physicsComponents;
    delete[] renderComponents;
}

TEST(DataLocalityPatternTest, badGameLoopPerformanceTest) {
    badGameLoop();
}

TEST(DataLocalityPatternTest, goodGameLoopPerformanceTest) {
    goodGameLoop();
}

TEST(DataLocalityPatternTest, particleSystem) {
    ParticleSystem p;
}