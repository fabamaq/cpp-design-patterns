#ifndef GAMEPROGRAMMINGPATTERNS_RENDERCOMPONENT_HPP
#define GAMEPROGRAMMINGPATTERNS_RENDERCOMPONENT_HPP

class RenderComponent {
public:
    void render() {
        // Work with and modify state...
    }

private:
    // Mesh, textures, shaders, etc. ...
};

#endif //GAMEPROGRAMMINGPATTERNS_RENDERCOMPONENT_HPP
