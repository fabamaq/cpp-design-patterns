#ifndef GAMEPROGRAMMINGPATTERNS_GAMEENTITY_HPP
#define GAMEPROGRAMMINGPATTERNS_GAMEENTITY_HPP

#include "AiComponent.hpp"
#include "PhysicsComponent.hpp"
#include "RenderComponent.hpp"

class GameEntity {
public:
    GameEntity(AIComponent *ai, PhysicsComponent *physics, RenderComponent *render)
            : ai_(ai), physics_(physics), render_(render) {}

    AIComponent *ai() { return ai_; }

    PhysicsComponent *physics() { return physics_; }

    RenderComponent *render() { return render_; }

private:
    AIComponent *ai_;
    PhysicsComponent *physics_;
    RenderComponent *render_;
};

#endif //GAMEPROGRAMMINGPATTERNS_GAMEENTITY_HPP
