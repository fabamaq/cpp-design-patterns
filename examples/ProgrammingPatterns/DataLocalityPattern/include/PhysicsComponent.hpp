#ifndef GAMEPROGRAMMINGPATTERNS_PHYSICSCOMPONENT_HPP
#define GAMEPROGRAMMINGPATTERNS_PHYSICSCOMPONENT_HPP

class PhysicsComponent {
public:
    void update() {
        // Work with and modify state...
    }

private:
    // Rigid body, velocity, mass, etc. ...
};

#endif //GAMEPROGRAMMINGPATTERNS_PHYSICSCOMPONENT_HPP
