#ifndef GAMEPROGRAMMINGPATTERNS_AICOMPONENT_HPP
#define GAMEPROGRAMMINGPATTERNS_AICOMPONENT_HPP

class Animation {};

class Vector {};

class LootType {};

class AIComponent {
public:
    void update() {
        // Work with and modify state...
    }

private:
    // Goals, mood, etc. ...
    Animation *animation_;
    double energy_;
    Vector goalPos_;

    // Self loot data
    LootType drop_;
    int minDrops_;
    int maxDrops_;
    double chanceOfDrop_;
};

class AIComponentHotData {
public:
    void update() {
        // Work with and modify state...
    }

private:
    // Goals, mood, etc. ...
    Animation *animation_;
    double energy_;
    Vector goalPos_;

    LootType* loot_;
};

class LootDrop {
    friend class AIComponentHotData;

    LootType drop_;
    int minDrops_;
    int maxDrops_;
    double chanceOfDrop_;
};

#endif //GAMEPROGRAMMINGPATTERNS_AICOMPONENT_HPP
