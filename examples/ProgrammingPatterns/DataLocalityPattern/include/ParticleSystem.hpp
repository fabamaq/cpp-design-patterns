#ifndef GAMEPROGRAMMINGPATTERNS_PARTICLESYSTEM_HPP
#define GAMEPROGRAMMINGPATTERNS_PARTICLESYSTEM_HPP


class Particle {
public:
    void update() { /* Gravity, etc. ... */ }
    // Position, velocity, etc. ...

    bool isActive() { return false; }
};

class ParticleSystem {
public:
    ParticleSystem()
            : numParticles_(0) {}

    void update();
    void activateParticle(int index);

private:
    static const int MAX_PARTICLES = 100000;

    int numParticles_;
    Particle particles_[MAX_PARTICLES];
    int numActive_;
};


#endif //GAMEPROGRAMMINGPATTERNS_PARTICLESYSTEM_HPP
