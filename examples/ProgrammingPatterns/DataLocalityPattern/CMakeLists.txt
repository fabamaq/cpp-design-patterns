# =============================================================================
# General Settings
# -----------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.0)
project(DataLocalityPattern)

# =============================================================================
# Compiler settings
# -----------------------------------------------------------------------------
set(CMAKE_CXX_STANDARD 11)

# =============================================================================
# Workspace settings
# -----------------------------------------------------------------------------
set(DataLocalityPatternSourcesFolder ${DataLocalityPatternProjectFolder}/src)
set(DataLocalityPatternIncludeFolder ${DataLocalityPatternProjectFolder}/include)
set(DataLocalityPatternTestsFolder ${DataLocalityPatternProjectFolder}/tests)

# =============================================================================
# Targets settings
# -----------------------------------------------------------------------------
# Target Executable
add_executable(DataLocalityPatternTest ${DataLocalityPatternTestsFolder}/DataLocalityPatternTest.cpp include/AiComponent.hpp include/PhysicsComponent.hpp include/RenderComponent.hpp include/GameEntity.hpp include/ParticleSystem.hpp src/ParticleSystem.cpp)
target_include_directories(DataLocalityPatternTest PUBLIC ${DataLocalityPatternIncludeFolder})
target_sources(DataLocalityPatternTest PUBLIC ${DataLocalityPatternSourcesFolder}/ParticleSystem.cpp
        )
target_link_libraries(DataLocalityPatternTest PUBLIC gtest gtest_main)