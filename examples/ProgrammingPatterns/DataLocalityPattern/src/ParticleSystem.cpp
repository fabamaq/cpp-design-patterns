#include "ParticleSystem.hpp"

#include <cassert>

void ParticleSystem::update() {
//    for (int i = 0; i < numParticles_; i++) {
//        if (particles_[i].isActive()) {
//            particles_[i].update();
//        }
//    }
    for (int i = 0; i < numActive_; i++) {
        particles_[i].update();
    }
}

void ParticleSystem::activateParticle(int index) {
    // Shouldn't already be active!
    assert(index >= numActive_);

    // Swap it with the first inactive particle right
    // after the active ones.
    Particle temp = particles_[numActive_];
    particles_[numActive_] = particles_[index];
    particles_[index] = temp;

    numActive_++;
}
