#include <FrameBuffer.hpp>

void FrameBuffer::clear() {
    for (int i = 0; i < WIDTH; i++) {
        pixels_[i] = WHITE;
    }
}

void FrameBuffer::draw(int x, int y) {
    pixels_[(WIDTH * y) + x] = BLACK;
}

const char* FrameBuffer::getPixels() {
    return pixels_;
}