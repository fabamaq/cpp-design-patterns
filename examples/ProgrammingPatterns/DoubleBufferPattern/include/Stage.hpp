#ifndef GAMEPROGRAMMINGPATTERNS_STAGE_HPP
#define GAMEPROGRAMMINGPATTERNS_STAGE_HPP

#include <Actor.hpp>

class Stage {
public:
    void add(Actor *actor, int index) {
        actors_[index] = actor;
    }

    void update() {
        for (int i = 0; i < NUM_ACTORS; i++) {
            actors_[i]->update();
        }
        for (int i = 0; i < NUM_ACTORS; i++) {
            actors_[i]->swap();
        }
    }

private:
    static const int NUM_ACTORS = 3;

    Actor *actors_[NUM_ACTORS];
};

#endif //GAMEPROGRAMMINGPATTERNS_STAGE_HPP
