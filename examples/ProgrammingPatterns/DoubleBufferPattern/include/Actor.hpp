#ifndef GAMEPROGRAMMINGPATTERNS_ACTOR_HPP
#define GAMEPROGRAMMINGPATTERNS_ACTOR_HPP

class Actor {
public:
    Actor() : currentSlapped_(false) {}

    virtual ~Actor() {}

    virtual void update() = 0;

    void swap()
    {
        // Swap the buffer.
        currentSlapped_ = nextSlapped_;

        // Clear the new "next" buffer.
        nextSlapped_ = false;
    }

    void slap() { nextSlapped_ = true; }

    bool wasSlapped() { return currentSlapped_; }

private:
    bool currentSlapped_;
    bool nextSlapped_;
};

#endif //GAMEPROGRAMMINGPATTERNS_ACTOR_HPP
