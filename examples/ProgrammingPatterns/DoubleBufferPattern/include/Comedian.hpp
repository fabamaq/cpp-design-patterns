#ifndef GAMEPROGRAMMINGPATTERNS_COMEDIAN_HPP
#define GAMEPROGRAMMINGPATTERNS_COMEDIAN_HPP

#include <Actor.hpp>

class Comedian : public Actor {
public:
    void face(Actor *actor) { facing_ = actor; }

    virtual void update() {
        if (wasSlapped()) facing_->slap();
    }

private:
    Actor *facing_;
};

#endif //GAMEPROGRAMMINGPATTERNS_COMEDIAN_HPP
