#ifndef GAMEPROGRAMMINGPATTERNS_FRAMEBUFFER_HPP
#define GAMEPROGRAMMINGPATTERNS_FRAMEBUFFER_HPP

class FrameBuffer {
public:
    // Constructor and methods...
    void clear();

    void draw(int x, int y);

    const char *getPixels();

private:
    static const int WIDTH = 160;
    static const int HEIGHT = 120;
    static const char WHITE = 'c';
    static const char BLACK = 'a';

    char pixels_[WIDTH * HEIGHT];
};

#endif //GAMEPROGRAMMINGPATTERNS_FRAMEBUFFER_HPP
