/******************************************************************************
 *
 * @file DoubleBufferPatternTest.cpp
 *
 * @brief Double Buffer design pattern
 * - "Cause a series of sequential operations to appear instantaneous or
 * ... simultaneous."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include <FrameBuffer.hpp>
#include <Scene.hpp>

#include <gtest/gtest.h>

TEST(DoubleBufferPatternTest, Scene) {
    //
}

#include <Actor.hpp>
#include <Comedian.hpp>
#include <Stage.hpp>

TEST(DoubleBufferPatternTest, AI) {
    Stage stage;

    Comedian *harry = new Comedian();
    Comedian *baldy = new Comedian();
    Comedian *chump = new Comedian();

    harry->face(baldy);
    baldy->face(chump);
    chump->face(harry);

    stage.add(harry, 0);
    stage.add(baldy, 1);
    stage.add(chump, 2);

//    stage.add(harry, 2);
//    stage.add(baldy, 1);
//    stage.add(chump, 0);

    harry->slap();
    stage.update();

}