/******************************************************************************
 *
 * @file GameLoopPatternTest.cpp
 * @brief Game Loop design pattern
 * - "Decouple the progression of game time
 * ... from user input and processor speed"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// C++ Standard library
#include <chrono>
#include <iostream>
#include <thread>

// Run game at 60 FPS
#define MS_PER_FRAME std::chrono::milliseconds(16)

std::chrono::time_point<std::chrono::system_clock> getCurrentTime() {
    return std::chrono::system_clock::now();
};

void processInput() {
    std::cout << "::processInput()...\n";
}

void update() {
    std::cout << "::update()...\n";
}

void render() {
    std::cout << "::render()...\n";
}

void testSimpleGameLoop() {
    auto begin = getCurrentTime();
    // safety net
    int counter = 600;


    while (counter > 0) {
        auto start = getCurrentTime();

        processInput();
        update();
        render();

        std::this_thread::sleep_for(start + MS_PER_FRAME - getCurrentTime());

        --counter;
    }

    auto end = getCurrentTime();
    std::chrono::duration<double> diff = end - begin;
    std::cout << "Run time: " << diff.count() << "s\n";
}

void update(std::chrono::duration<double> elapsed) {
    std::cout << "::update(" << elapsed.count() << ")...\n";
}

void testFluidGameLoop() {
    auto begin = getCurrentTime();

    // safety net
    int counter = 600;

    auto lastTime = getCurrentTime();

    while (counter > 0) {
        auto current = getCurrentTime();
        std::chrono::duration<double> elapsed = current - lastTime;

        processInput();
        update(elapsed);
        render();

        lastTime = current;

        --counter;
    }

    auto end = getCurrentTime();
    std::chrono::duration<double> diff = end - begin;
    std::cout << "Run time: " << diff.count() << "s\n";
}


// MS_PER_UPDATE is just the granularity we use to update the game
// ... the shorter this step is, the more processing time it takes to catch up to real time
#define MS_PER_UPDATE std::chrono::milliseconds(16)

void render(std::chrono::duration<double> elapsed) {
    std::cout << "::render(" << elapsed.count() << ")...\n";
}

void testFixedGameLoop() {
    auto begin = getCurrentTime();

    // safety net
    int counter = 600;

    auto previous = getCurrentTime();
    std::chrono::duration<double> lag(0.0);

    while (counter > 0) {
        auto current = getCurrentTime();
        std::chrono::duration<double> elapsed = current - previous;
        previous = current;
        lag += elapsed;
        processInput();

        while (lag >= MS_PER_UPDATE) {
            update();
            lag -= MS_PER_UPDATE;
        }

        // We divide by MS_PER_UPDATE
        render(static_cast<std::chrono::duration<double>>(lag / MS_PER_UPDATE));

        --counter;
    }

    auto end = getCurrentTime();
    std::chrono::duration<double> diff = end - begin;
    std::cout << "Run time: " << diff.count() << "s\n";
}

int main() {
    testSimpleGameLoop();
    testFluidGameLoop();
    testFixedGameLoop();
    return 0;
}