#include <InputHandler.hpp>
#include <FireCommand.hpp>
#include <JumpCommand.hpp>
#include <MoveUnitCommand.hpp>

InputHandler::InputHandler(bool _BUTTON_UP, bool _BUTTON_DOWN, bool _BUTTON_LEFT, bool _BUTTON_RIGHT,
                           bool _X, bool _Y, bool _A, bool _B)
    : BUTTON_UP(_BUTTON_UP), BUTTON_DOWN(_BUTTON_DOWN), BUTTON_LEFT(_BUTTON_LEFT), BUTTON_RIGHT(_BUTTON_RIGHT), BUTTON_X(_X), BUTTON_Y(_Y), BUTTON_A(_A), BUTTON_B(_B)
{
    buttonA_ = new JumpCommand();
    buttonB_ = new JumpCommand();
    buttonY_ = new FireCommand();
    buttonX_ = new FireCommand();
}

bool InputHandler::isPressed(bool BUTTON)
{
    return BUTTON;
}

Unit *getSelectedUnit()
{
    return new Unit(0, 0);
}

Command *InputHandler::handleInput()
{
    Unit *unit = getSelectedUnit();

    if (isPressed(BUTTON_UP))
    {
        // Move the unit up one.
        int destY = unit->y() - 1;
        return new MoveUnitCommand(unit, unit->x(), destY);
    }

    if (isPressed(BUTTON_DOWN))
    {
        // Move the unit down one.
        int destY = unit->y() + 1;
        return new MoveUnitCommand(unit, unit->x(), destY);
    }

    // Other moves...
    if (isPressed(BUTTON_A))
    {
        return buttonA_;
    }

    // Other moves...
    if (isPressed(buttonY_))
    {
        return buttonA_;
    }

    return NULL;
}