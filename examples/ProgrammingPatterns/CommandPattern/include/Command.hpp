#ifndef COMMAND_PATTERN_HPP
#define COMMAND_PATTERN_HPP

#include <GameActor.hpp>

class Command
{
public:
    virtual ~Command() {}
    virtual void execute(GameActor &actor) = 0;
    virtual void execute() = 0;
    virtual void undo() = 0;
};

#endif // COMMAND_PATTERN_HPP