#ifndef GAME_ACTOR_HPP
#define GAME_ACTOR_HPP

#include <iostream>

class GameActor
{
public:
    virtual void jump()
    {
        std::cout << "GameActor jumps...\n";
    }
    virtual void fireGun()
    {
        std::cout << "GameActor fires gun...\n";
    }
};

#endif // GAME_ACTOR_HPP