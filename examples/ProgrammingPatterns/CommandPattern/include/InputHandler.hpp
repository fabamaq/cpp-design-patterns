// -------------------------------------------------------------- Include Guard
#ifndef INPUT_HANDLER_HPP
#define INPUT_HANDLER_HPP

// ------------------------------------------------------------------- Includes
#include <Command.hpp>

// C Standard Library
#include <cstdlib> // NULL

// ---------------------------------------------------------- Class Declaration
class InputHandler
{
    bool BUTTON_UP;
    bool BUTTON_DOWN;
    bool BUTTON_LEFT;
    bool BUTTON_RIGHT;

    bool BUTTON_X;
    bool BUTTON_Y;
    bool BUTTON_A;
    bool BUTTON_B;

public:
    InputHandler(
        bool _BUTTON_UP, bool _BUTTON_DOWN, bool _BUTTON_LEFT, bool _BUTTON_RIGHT,
        bool _X, bool _Y, bool _A, bool _B);

    Command *handleInput();

private:
    Command *buttonX_;
    Command *buttonY_;
    Command *buttonA_;
    Command *buttonB_;
    Command *buttonUp_;
    Command *buttonDown_;
    Command *buttonLeft_;
    Command *buttonRight_;

    bool isPressed(bool BUTTON);
};

#endif // INPUT_HANDLER_HPP