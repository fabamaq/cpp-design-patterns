#ifndef MOVE_UNIT_COMMAND_HPP
#define MOVE_UNIT_COMMAND_HPP

#include <Command.hpp>
#include <GameActor.hpp>

class Unit
{
    int x_;
    int y_;

public:
    Unit(int x, int y) : x_(x), y_(y) {}
    void moveTo(int x, int y)
    {
        x_ = x;
        y_ = y;
    }
    int x() { return x_; }
    int y() { return y_; }
};

class MoveUnitCommand : public Command
{
public:
    MoveUnitCommand(Unit *unit, int x, int y)
        : unit_(unit),
          x_(x),
          y_(y) {}

    virtual void execute()
    {
        // Remember the unit's position before the move
        // so we can restore it
        xBefore_ = x_;
        yBefore_ = y_;
        unit_->moveTo(x_, y_);
    }

    virtual void execute(GameActor &actor)
    {
        // do nothing
    }

    virtual void undo()
    {
        unit_->moveTo(xBefore_, yBefore_);
    }

private:
    Unit *unit_;
    int x_, y_;
    int xBefore_, yBefore_;
};

#endif // MOVE_UNIT_COMMAND_HPP