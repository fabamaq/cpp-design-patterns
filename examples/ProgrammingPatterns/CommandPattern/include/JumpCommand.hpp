#ifndef JUMP_COMMAND_HPP
#define JUMP_COMMAND_HPP

#include "Command.hpp"

class JumpCommand : public Command
{
public:
    virtual void execute(GameActor &actor) { actor.jump(); }
    virtual void execute() {}
    virtual void undo() {}
};

#endif // JUMP_COMMAND_HPP