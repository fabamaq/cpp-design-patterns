#ifndef FIRE_COMMAND_HPP
#define FIRE_COMMAND_HPP

#include <Command.hpp>
#include <GameActor.hpp>

class FireCommand : public Command
{
public:
    virtual void execute(GameActor &actor) { actor.fireGun(); }
    virtual void execute() {}
    virtual void undo() {}
};

#endif // FIRE_COMMAND_HPP