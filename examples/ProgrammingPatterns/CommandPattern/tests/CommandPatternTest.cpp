/******************************************************************************
 * @file CommandPatternTest.cpp
 *
 * @brief Command design patter - "Encapsulate a request as an object,
 * ... thereby letting users parameterize clients with different requests,
 * ... queue or log requests, and support undoable operations".
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Command includes
#include <Command.hpp>
#include <InputHandler.hpp>
#include <GameActor.hpp>

// Google Test Framework
#include "gtest/gtest.h"

// ---------------------------------------------------------------------- Mocks
class MockActor : public GameActor
{

public:
    bool didJump = false;
    bool didFire = false;

    void jump() override
    {
        didJump = true;
    }

    void fireGun() override
    {
        didFire = true;
    }
};

class TestingCommandPattern : public ::testing::Test
{
protected:
    MockActor *actor;
    InputHandler *inputHandler;
    Command *command;

    bool BUTTON_UP = false;
    bool BUTTON_DOWN = false;
    bool BUTTON_LEFT = false;
    bool BUTTON_RIGHT = false;

    bool BUTTON_X = false;
    bool BUTTON_Y = false;
    bool BUTTON_A = false;
    bool BUTTON_B = false;

    void SetUp() override
    {
        actor = new MockActor();
    }

    void clickOnButton(bool &button)
    {
        button = true;
    }

    Command *handleInput()
    {
        inputHandler = new InputHandler(BUTTON_UP, BUTTON_DOWN, BUTTON_LEFT, BUTTON_RIGHT, BUTTON_X, BUTTON_Y, BUTTON_A, BUTTON_B);
        return inputHandler->handleInput();
    }

    void TearDown() override
    {
        delete actor;
        delete inputHandler;
    }
};

// ---------------------------------------------------------------------- Tests
TEST_F(TestingCommandPattern, ExecuteJumpCommand)
{
    clickOnButton(BUTTON_A);
    command = handleInput();
    if (command)
        command->execute(*actor);
    delete command;
    EXPECT_TRUE(actor->didJump);
}

TEST_F(TestingCommandPattern, ExecuteFireCommand)
{
    clickOnButton(BUTTON_B);
    command = handleInput();
    if (command)
        command->execute(*actor);
    delete command;
    EXPECT_TRUE(actor->didJump);
}


