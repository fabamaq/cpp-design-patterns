/******************************************************************************
 *
 * @file DirtyFlagPatternTest.cpp
 *
 * @brief Dirt Flag design pattern
 * - "Avoid unnecessary work by deferring it until the result is needed."
 *****************************************************************************/

#include <gtest/gtest.h>

class Transform {
public:
  static Transform origin() { return Transform(); }

  Transform combine(Transform &other) { return other; }
};

class Mesh;

class GraphNode {
public:
  GraphNode(Mesh *mesh)
      : mesh_(mesh), local_(Transform::origin()), dirty_(true) {}

  void render(Transform parentWorld, bool dirty);

  void setTransform(Transform local);

private:
  Transform world_;
  bool dirty_;

  static const unsigned int MAX_CHILDREN = 100u;
  Transform local_;
  Mesh *mesh_;

  GraphNode *children_[MAX_CHILDREN];
  int numChildren_;
};

void renderMesh(Mesh *mesh, Transform transform) { puts("rendering ..."); }

void GraphNode::render(Transform parentWorld, bool dirty) {
  dirty |= dirty_;
  if (dirty) {
    world_ = local_.combine(parentWorld);
    dirty_ = false;
  }

  if (mesh_)
    renderMesh(mesh_, world_);

  for (int i = 0; i < numChildren_; i++) {
    children_[i]->render(world_, dirty);
  }
}

void GraphNode::setTransform(Transform local) {
  local_ = local;
  dirty_ = true;
}

TEST(DirtyFlagPatternTest, Assertion) {
  GraphNode *graph_ = new GraphNode(NULL);
  // Add children to root graph node...

  // To draw an entire scene graph, we kick off the process at the root node:
  graph_->render(Transform::origin(), true);

  delete graph_;
  SUCCEED();
}
