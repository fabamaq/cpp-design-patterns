// ------------------------------------------------------------------- Includes
#include <TreeModel.hpp>

TreeModel *TreeModel::instance_ = nullptr;

TreeModel *TreeModel::GetInstance() {
    if (instance_ == nullptr) {
        instance_ = new TreeModel();
    }
    return instance_;
}

TreeModel::TreeModel() {
    mesh_ = Mesh();
    bark_ = Texture();
    leaves_ = Texture();
}