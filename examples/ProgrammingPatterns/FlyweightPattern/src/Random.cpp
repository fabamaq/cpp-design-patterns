#include <Random.hpp>

int random(int i) {
    std::uniform_int_distribution<int> d(0, 10);
    std::random_device rd1;
    int result = d(rd1);
    return result;
}