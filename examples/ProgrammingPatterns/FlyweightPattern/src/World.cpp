// ------------------------------------------------------------------- Includes
// Flyweight includes
#include <World.hpp>
#include <Terrain.hpp>
#include <Random.hpp>

World::World()
        : grassTerrain_(1, false, GRASS_TEXTURE),
          hillTerrain_(3, false, HILL_TEXTURE),
          riverTerrain_(2, true, RIVER_TEXTURE) {
    generateTerrain();
}

void World::generateTerrain() {
    // Fill the ground with grass.
    for (int x = 0; x < WIDTH;
         x++) {
        for (int y = 0; y < HEIGHT;
             y++) {
            // Sprinkle some hills.
            if (random(10) == 0) {
                tiles_[x][y] = &hillTerrain_;
            } else {
                tiles_[x][y] = &grassTerrain_;
            }
        }
    }

    // Lay a river
    int x = random(WIDTH);
    for (int y = 0; y < HEIGHT; y++) {
        tiles_[x][y] = &riverTerrain_;
    }
}

const Terrain &World::getTile(int x, int y) const {
    return *tiles_[x][y];
}
