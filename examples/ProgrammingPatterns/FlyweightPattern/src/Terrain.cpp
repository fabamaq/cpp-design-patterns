// ------------------------------------------------------------------- Includes
#include <Terrain.hpp>

Terrain::Terrain(int moveCost, bool isWater, Texture texture)
        : moveCost_(moveCost), isWater_(isWater), texture_(texture) {}

int Terrain::getMoveCost() const {
    return moveCost_;
}

bool Terrain::isWater() const {
    return isWater_;
}

const Texture &Terrain::getTexture() const {
    return texture_;
}
