// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_TERRAIN_HPP
#define GAMEPROGRAMMINGPATTERNS_TERRAIN_HPP

// ------------------------------------------------------------------- Includes
#include <Texture.hpp>

// Extracted a Terrain class and moved Terrain logic in class World into it
//enum Terrain
//{
//    TERRAIN_GRASS,
//    TERRAIN_HILL,
//    TERRAIN_RIVER
//    // Other terrains...
//};

class Terrain {
public:
    explicit Terrain(int moveCost, bool isWater, Texture texture);

    int getMoveCost() const;

    bool isWater() const;

    const Texture &getTexture() const;

protected:
    /// A movement cost that determines how quickly players can move through it
    int moveCost_;

    /// A flag for whether it's a watery terrain that can be crossed by boats
    bool isWater_;

    /// A texture used to render the terrain
    Texture texture_;
};

#endif // GAMEPROGRAMMINGPATTERNS_TERRAIN_HPP