#ifndef GAMEPROGRAMMINGPATTERNS_RANDOM_HPP
#define GAMEPROGRAMMINGPATTERNS_RANDOM_HPP

// ------------------------------------------------------------------- Includes
// C++ Standard library
#include <random>

int random(int i);

#endif //GAMEPROGRAMMINGPATTERNS_RANDOM_HPP
