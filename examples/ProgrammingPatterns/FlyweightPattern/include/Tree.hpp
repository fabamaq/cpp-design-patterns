// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_TREE_HPP
#define GAMEPROGRAMMINGPATTERNS_TREE_HPP

// ------------------------------------------------------------------- Includes
#include <Color.hpp>
#include <Mesh.hpp>
#include <Texture.hpp>
#include <Vector.hpp>

class Tree {
private:
    Mesh mesh_;
    Texture bark_;
    Texture leaves_;
    Vector position_;
    double height_;
    double thickness_;
    Color barkTint_;
    Color leafTint_;

public:
    Tree(/* dependency injection */) {
        position_ = Vector{0, 0, 0};
        height_ = 1.0;
        thickness_ = 1.0;
        barkTint_ = Brown;
        leafTint_ = Green;
    }
};

#endif //GAMEPROGRAMMINGPATTERNS_TREE_HPP
