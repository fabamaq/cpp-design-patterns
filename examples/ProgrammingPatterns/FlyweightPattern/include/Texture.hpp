// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_TEXTURE_HPP
#define GAMEPROGRAMMINGPATTERNS_TEXTURE_HPP

enum class TextureType {
    GRASS,
    HILL,
    RIVER
};

struct Texture {
    TextureType textureType;
    bool operator==(const Texture& rhs) const {
        return this->textureType == rhs.textureType;
    }
};


static const Texture GRASS_TEXTURE { TextureType::GRASS };
static const Texture HILL_TEXTURE { TextureType::HILL };
static const Texture RIVER_TEXTURE { TextureType::RIVER };

#endif //GAMEPROGRAMMINGPATTERNS_TEXTURE_HPP
