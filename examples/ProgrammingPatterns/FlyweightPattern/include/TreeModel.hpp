// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_TREE_MODEL_HPP
#define GAMEPROGRAMMINGPATTERNS_TREE_MODEL_HPP

// ------------------------------------------------------------------- Includes
#include <Mesh.hpp>
#include <Texture.hpp>

/******************************************************************************
 * @class TreeModel
 *
 * @brief Represents the intrinsic state of a Tree
 * Implements the Singleton design pattern
 *****************************************************************************/
class TreeModel
{
    static TreeModel* instance_;
    TreeModel();
public:
    static TreeModel* GetInstance();
private:
    Mesh mesh_;
    Texture bark_;
    Texture leaves_;
};

#endif // GAMEPROGRAMMINGPATTERNS_TREE_MODEL_HPP