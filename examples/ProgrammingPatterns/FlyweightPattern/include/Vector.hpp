// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_VECTOR_HPP
#define GAMEPROGRAMMINGPATTERNS_VECTOR_HPP

struct Vector {
    int x_;
    int y_;
    int z_;
};

#endif //GAMEPROGRAMMINGPATTERNS_VECTOR_HPP
