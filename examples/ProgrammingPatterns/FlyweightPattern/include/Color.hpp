// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_COLOR_HPP
#define GAMEPROGRAMMINGPATTERNS_COLOR_HPP

struct Color {
    int r_;
    int g_;
    int b_;
    int a_;
};

static const Color Brown{ 1, 1, 0, 1 };
static const Color Green{ 0, 1, 0, 1 };

#endif //GAMEPROGRAMMINGPATTERNS_COLOR_HPP
