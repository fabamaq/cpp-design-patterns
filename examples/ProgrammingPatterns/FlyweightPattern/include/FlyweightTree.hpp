// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_FLYWEIGHTTREE_HPP
#define GAMEPROGRAMMINGPATTERNS_FLYWEIGHTTREE_HPP

// ------------------------------------------------------------------- Includes
// Flyweight includes
#include <Color.hpp>
#include <TreeModel.hpp>
#include <Vector.hpp>

// C++ Standard library
#include <iostream>

/******************************************************************************
 * @class Tree
 *
 * @brief
 * Implements the Flyweight design pattern by
 * ... having a pointer to a Tree Model that is
 * ... shared across multiple instances of a Tree
 *****************************************************************************/
class FlyweightTree {
private:
    // Intrinsic state: data shared across instances
    TreeModel *model_; // Singleton

    // Extrinsic state: data unique to each instance
    Vector position_;
    double height_;
    double thickness_;
    Color barkTint_;
    Color leafTint_;

public:
    FlyweightTree() {
        model_ = TreeModel::GetInstance();
        position_ = Vector{0, 0, 0};
        height_ = 1.0;
        thickness_ = 1.0;
        barkTint_ = Brown;
        leafTint_ = Green;
    }

    TreeModel *getModel() {
        return model_;
    }

    void *operator new(size_t size) {
        std::cout << "Overloading new operator with size: " << size << std::endl;
        void *p = ::new FlyweightTree();
        return p;
    }

    void operator delete(void *p) {
        std::cout << "Overloading delete operator " << std::endl;
        free(p);
    }
};

#endif //GAMEPROGRAMMINGPATTERNS_FLYWEIGHTTREE_HPP