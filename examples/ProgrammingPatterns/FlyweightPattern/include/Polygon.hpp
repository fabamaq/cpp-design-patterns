// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_POLYGON_HPP
#define GAMEPROGRAMMINGPATTERNS_POLYGON_HPP

// ------------------------------------------------------------------- Includes
#include <Vector.hpp>

struct Polygon {
    Vector vertexA;
    Vector vertexB;
    Vector vertexC;
};

#endif //GAMEPROGRAMMINGPATTERNS_POLYGON_HPP
