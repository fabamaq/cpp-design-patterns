// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_MESH_HPP
#define GAMEPROGRAMMINGPATTERNS_MESH_HPP

// ------------------------------------------------------------------- Includes
#include <Polygon.hpp>

struct Mesh {
    Polygon polygons[1000];
};

#endif //GAMEPROGRAMMINGPATTERNS_MESH_HPP
