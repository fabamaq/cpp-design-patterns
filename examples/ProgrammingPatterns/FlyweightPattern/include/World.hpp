// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_WORLD_HPP
#define GAMEPROGRAMMINGPATTERNS_WORLD_HPP

// ------------------------------------------------------------------- Includes
#include <Terrain.hpp>

static const size_t WIDTH = 10;
static const size_t HEIGHT = 10;

class World {
    Terrain *tiles_[WIDTH][HEIGHT];
    // Other stuff...
public:
    World();

    void generateTerrain();

    const Terrain &getTile(int x, int y) const;

private:
    Terrain grassTerrain_;
    Terrain hillTerrain_;
    Terrain riverTerrain_;
    // Other stuff...
};

#endif // GAMEPROGRAMMINGPATTERNS_WORLD_HPP