/******************************************************************************
 * @file FlyweightPatternTest.cpp
 *
 * @brief Flyweight design pattern
 * - "Use sharing to support large numbers of fine-grained objects efficiently."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Flyweight includes
#include <FlyweightTree.hpp>
#include <Tree.hpp>
#include <World.hpp>
#include <Random.hpp>

// Google Test Framework
#include <gtest/gtest.h>

//TEST(TestingFlyweightPattern, TestRandom) {
//    int x = random(10);
//    int y = random(10);
//    EXPECT_NE(x, y);
//}

// unknown file: error: SEH exception with code 0xc00000fd thrown in the test body.
static const unsigned int MAX_NUMBER_OF_TREES = 28; // must not exceed MEMORY_LIMIT
static const unsigned int MAX_NUMBER_OF_FLYWEIGHT_TREES = 1000;
static const unsigned int MEMORY_LIMIT = 1020000l;

TEST(TestingFlyweightPattern, TestCreationOfTree) {
    Tree trees[MAX_NUMBER_OF_TREES];
    ASSERT_LT(sizeof(trees), MEMORY_LIMIT);
}

TEST(TestingFlyweightPattern, TestCreationOfFlyweightTree) {
    FlyweightTree trees[MAX_NUMBER_OF_FLYWEIGHT_TREES];
    ASSERT_LT(sizeof(trees), MEMORY_LIMIT);
}

TEST(TestingFlyweightPattern, TestWorld) {
    World world;
    int x = random(WIDTH);
    int y = random(HEIGHT);
    auto terrain = world.getTile(x, y);
    const Texture &texture = terrain.getTexture();
    if (texture == RIVER_TEXTURE) {
        EXPECT_TRUE(terrain.isWater());
    }
    else {
        EXPECT_FALSE(terrain.isWater());
    }
}