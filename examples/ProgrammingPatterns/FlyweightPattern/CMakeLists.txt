# =============================================================================
# General Settings
# -----------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.0)
project(FlyweightPattern)

# =============================================================================
# Compiler settings
# -----------------------------------------------------------------------------
set(CMAKE_CXX_STANDARD 11)

# =============================================================================
# Workspace settings
# -----------------------------------------------------------------------------
set(FlyweightPatternSourcesFolder ${FlyweightPatternProjectFolder}/src)
set(FlyweightPatternIncludeFolder ${FlyweightPatternProjectFolder}/include)
set(FlyweightPatternTestsFolder ${FlyweightPatternProjectFolder}/tests)


# =============================================================================
# Targets settings
# -----------------------------------------------------------------------------
# Target Executable
add_executable(FlyweightPatternTest ${FlyweightPatternTestsFolder}/FlyweightPatternTest.cpp)
target_include_directories(FlyweightPatternTest PUBLIC ${FlyweightPatternIncludeFolder})
target_sources(FlyweightPatternTest PUBLIC
        ${FlyweightPatternSourcesFolder}/World.cpp
        ${FlyweightPatternSourcesFolder}/TreeModel.cpp
        ${FlyweightPatternSourcesFolder}/Terrain.cpp
        ${FlyweightPatternSourcesFolder}/Random.cpp
        )
target_link_libraries(FlyweightPatternTest PUBLIC gtest gtest_main)