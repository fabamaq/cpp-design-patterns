# =============================================================================
# General Settings
# -----------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.0)
project(SpatialPartitionPattern)

# =============================================================================
# Compiler settings
# -----------------------------------------------------------------------------
set(CMAKE_CXX_STANDARD 11)

# =============================================================================
# Workspace settings
# -----------------------------------------------------------------------------
set(SpatialPartitionPatternSourcesFolder ${SpatialPartitionPatternProjectFolder}/src)
set(SpatialPartitionPatternIncludeFolder ${SpatialPartitionPatternProjectFolder}/include)
set(SpatialPartitionPatternTestsFolder ${SpatialPartitionPatternProjectFolder}/tests)

# =============================================================================
# Targets settings
# -----------------------------------------------------------------------------
# Target Executable
add_executable(SpatialPartitionPatternTest ${SpatialPartitionPatternTestsFolder}/SpatialPartitionPatternTest.cpp)
target_include_directories(SpatialPartitionPatternTest PUBLIC ${SpatialPartitionPatternIncludeFolder})
target_sources(SpatialPartitionPatternTest PUBLIC
        ${SpatialPartitionPatternSourcesFolder}/Grid.cpp
        ${SpatialPartitionPatternSourcesFolder}/Unit.cpp
        )
target_link_libraries(SpatialPartitionPatternTest PUBLIC gtest gtest_main)