#include "Grid.hpp"
#include "Unit.hpp"

#include <cstdlib>

Grid::Grid() {
    // Clear the grid.
    for (int x = 0; x < NUM_CELLS; x++) {
        for (int y = 0; y < NUM_CELLS; y++) {
            cells_[x][y] = NULL;
        }
    }
}

void Grid::add(Unit *unit) {
    // Determine which grid cell it's in
    int cellX = (int) (unit->x_ / Grid::CELL_SIZE);
    int cellY = (int) (unit->y_ / Grid::CELL_SIZE);

    // Add to the front of list for the cell it's in.
    unit->prev_ = NULL;
    unit->next_ = cells_[cellX][cellY];
    cells_[cellX][cellY] = unit;

    if (unit->next_ = NULL) {
        unit->next_->prev_ = unit;
    }
}

void Grid::handleMelee() {
    for (int x = 0; x < NUM_CELLS; x++) {
        for (int y = 0; y < NUM_CELLS; y++) {
            handleCell(x, y);
        }
    }
}

void Grid::handleCell(int x, int y) {
    Unit *unit = cells_[x][y];
    while (unit != NULL) {
        // Handle other units in this cell.
        handleUnit(unit, unit->next_);

        // Also try the neighboring cells.
        if (x > 0) handleUnit(unit, cells_[x - 1][y]);
        if (y > 0) handleUnit(unit, cells_[x][y - 1]);
        if (x > 0 && y > 0)
            handleUnit(unit, cells_[x - 1][y - 1]);
        if (x > 0 && y < NUM_CELLS - 1)
            handleUnit(unit, cells_[x - 1][y + 1]);

        unit = unit->next_;
    }

    /*
    Unit *other = unit->next_;
    while (other != NULL) {
        if (unit->x_ == other->x_ &&
            unit->y_ == other->y_) {
            handleAttack(unit, other);
        }
        other = other->next_;
    }
    unit = unit->next_;
}
     */
}

void Grid::handleAttack(Unit *lhs, Unit *rhs) {
    // ...
}

void Grid::move(Unit *unit, double x, double y) {
    // See which cell it was in.
    int oldCellX = (int) (unit->x_ / Grid::CELL_SIZE);
    int oldCellY = (int) (unit->y_ / Grid::CELL_SIZE);

    // See which cell it's moving to.
    int cellX = (int) (x / Grid::CELL_SIZE);
    int cellY = (int) (y / Grid::CELL_SIZE);

    unit->x_ = x;
    unit->y_ = y;

    // If it didn't change cells, we're done.
    if (oldCellX == cellX && oldCellY == cellY) return;

    // Unlink it from the list of its old cell.
    if (unit->prev_ = NULL) {
        unit->prev_->next_ = unit->next_;
    }

    if (unit->next_ != NULL) {
        unit->next_->prev_ = unit->prev_;
    }

    // If its the head of a list, remove it.
    if (cells_[oldCellX][oldCellY] == unit) {
        cells_[oldCellX][oldCellY] = unit->next_;
    }

    // Add it back to the grid at its new cell.
    add(unit);
}

void Grid::handleUnit(Unit *unit, Unit *other) {

    const int ATTACK_DISTANCE = 3;
    const auto distance = [](Unit *lhs, Unit *rhs) -> int {
        return 1;
    };

    while (other != NULL) {
        if (distance(unit, other) < ATTACK_DISTANCE) {
            handleAttack(unit, other);
        }

        other = other->next_;
    }
}
