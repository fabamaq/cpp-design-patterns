#include "Unit.hpp"
#include "Grid.hpp"

#include <cstdlib>

Unit::Unit(Grid *grid, double x, double y)
        : grid_(grid), x_(x), y_(y), prev_(NULL), next_(NULL) {
    grid->add(this);
}

void Unit::move(double x, double y) {
    grid_->move(this, x, y);
}
