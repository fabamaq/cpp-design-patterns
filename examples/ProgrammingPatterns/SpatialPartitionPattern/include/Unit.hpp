#ifndef GAMEPROGRAMMINGPATTERNS_UNIT_HPP
#define GAMEPROGRAMMINGPATTERNS_UNIT_HPP


class Unit {
    friend class Grid;

public:
    Unit(Grid *grid, double x, double y);

    void move(double x, double y);

private:
    double x_, y_;
    Grid *grid_;

private:
    Unit *prev_;
    Unit *next_;
};



#endif //GAMEPROGRAMMINGPATTERNS_UNIT_HPP
