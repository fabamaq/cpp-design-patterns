#ifndef GAMEPROGRAMMINGPATTERNS_GRID_HPP
#define GAMEPROGRAMMINGPATTERNS_GRID_HPP

class Unit;

class Grid {
public:
    Grid();

    void add(Unit *unit);

    void move(Unit* unit, double x, double y);

    void handleAttack(Unit *lhs, Unit *rhs);

    void handleMelee();

//    void handleCell(Unit *unit);
    void handleCell(int x, int y);

    void handleUnit(Unit* unit, Unit* other);

    static const int NUM_CELLS = 10;
    static const int CELL_SIZE = 20;

private:
    Unit *cells_[NUM_CELLS][NUM_CELLS];
};


#endif //GAMEPROGRAMMINGPATTERNS_GRID_HPP
