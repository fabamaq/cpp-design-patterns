/******************************************************************************
 *
 * @file SpatialPartitionPatternTest.cpp
 *
 * @brief Spatial Partition design pattern
 * - "Efficiently locate objects by storing them in
 * ... a data structure organized by their positions"
 *****************************************************************************/
#include "Unit.hpp"
#include "Grid.hpp"

#include <gtest/gtest.h>

TEST(SpatialPartitionPatternTest, Assertion) {
    Grid g;
    Unit u(&g, 0, 0);
}