#ifndef GAMEPROGRAMMINGPATTERNS_SKYLAUNCH_HPP
#define GAMEPROGRAMMINGPATTERNS_SKYLAUNCH_HPP

#include <Superpower.hpp>

class SkyLaunch : public Superpower {
protected:
    virtual void activate() {
        if (getHeroZ() == 0) {
            // On the ground, so spring into the air.
            playSound(SOUND_SPROING);
            spawnParticles(PARTICLE_DUST, 10);
            move(0, 0, 20);
        }
        else if (getHeroZ() < 10.0f) {
            // Near the ground, so do a double jump.
            playSound(SOUND_SWOOP);
            move(0, 0, getHeroZ() - 20);
        }
        else {
            // Way up in the air, so do a dive attack.
            playSound(SOUND_DIVE);
            spawnParticles(PARTICLE_SPARKLES, 1);
            move(0, 0, -getHeroZ());
        }
    }
};

#endif //GAMEPROGRAMMINGPATTERNS_SKYLAUNCH_HPP
