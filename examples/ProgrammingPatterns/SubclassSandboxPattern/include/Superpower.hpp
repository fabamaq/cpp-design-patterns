#ifndef GAMEPROGRAMMINGPATTERNS_SUPERPOWER_HPP
#define GAMEPROGRAMMINGPATTERNS_SUPERPOWER_HPP

#include <SoundPlayer.hpp>
#include <ParticleSystem.hpp>

enum ParticleType {
    PARTICLE_DUST,
    PARTICLE_SPARKLES
};

/**
 * Implements the Sandbox Subclass design pattern
 */
class Superpower {
public:
    // Removed to implement two-stage initialization
//    Superpower(ParticleSystem* particles)
//    : particles_(particles) {}
    virtual ~Superpower() {}

#if defined TwoStageInitialization
    void init(ParticleSystem* particles) {
        particles_ = particles;
    }
#elif defined StaticState
    static void init(ParticleSystem* particles) {
        particles_ = particles;
    }
#endif // StaticState
protected:
    // Moved to SoundPlayer ...
//    void playSound(SoundId sound) { /* Code here... */ }
//    void stopSound(SoundId sound) { /* Code here... */ }
//    void setVolume(SoundId sound) { /* Code here... */ }
    // ... but we get access to it
    SoundPlayer& getSoundPlayer() {
        return soundPlayer_;
    }

    double getHeroX() { /* Code here... */ }
    double getHeroY() { /* Code here... */ }
    double getHeroZ() { /* Code here... */ }

    // Sandbox Subclass method
    virtual void activate() = 0;
    void move(double x, double y, double z) {
        // Code here ...
    }

    void playSound(SoundId sound) {
        soundPlayer_.playSound(sound);
    }

    void spawnParticles(ParticleType type, int count) {
#if defined ServiceLocator
        ParticleSystem& particles = Locator::getParticles();
        particles.span(type, count);
#endif // ServiceLocator
    }

private:
    SoundPlayer soundPlayer_;
#if defined TwoStageInitialization
    ParticleSystem* particles_;
#elif defined StaticState
    static ParticleSystem* particles_;
#endif // StaticState

public:
#ifdef StaticState
    Superpower* createSkyLaunch(ParticleSystem* particles) {
        Superpower* power = new SkyLaunch();
        power->init(particles);
    }
#endif // StaticState
};

#endif //GAMEPROGRAMMINGPATTERNS_SUPERPOWER_HPP
