#ifndef GAMEPROGRAMMINGPATTERNS_SOUNDPLAYER_HPP
#define GAMEPROGRAMMINGPATTERNS_SOUNDPLAYER_HPP

enum SoundId {
    SOUND_SPROING,
    SOUND_SWOOP,
    SOUND_DIVE
};

class SoundPlayer {
public:
    void playSound(SoundId sound) { /* Code here... */ }
    void stopSound(SoundId sound) { /* Code here... */ }
    void setVolume(SoundId sound) { /* Code here... */ }
};

#endif //GAMEPROGRAMMINGPATTERNS_SOUNDPLAYER_HPP
