/******************************************************************************
 *
 * @file SubclassSandboxPatternTest.cpp
 *
 * @brief Subclass Sandbox design pattern
 * - "Define behavior in a subclass using a set of operations provided
 * ... by its base class."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include <Superpower.hpp>
#include <SkyLaunch.hpp>
#include <SoundPlayer.hpp>

#include <gtest/gtest.h>


TEST(SubclassSandboxPatternTest, BasicAssertion) {
#if defined TwoStageInitialization
    ParticleSystem particles;
    Superpower* power = new SkyLaunch();
    power->init(particles);
#endif

    SUCCEED();

#if defined TwoStageInitialization
    delete power;
#endif
}
