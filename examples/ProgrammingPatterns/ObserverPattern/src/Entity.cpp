#include <Entity.hpp>

Entity::Entity(bool isHero)
        : isHero_(isHero), isOnSurface_(true), velocity_(0.0) {}

void Entity::accelerate(double velocity) {
    velocity_ += velocity;
}

void Entity::update() {
    if (velocity_ > -1 || velocity_ < 1) {
        isOnSurface_ = true;
    }
}

bool Entity::isHero() const {
    return isHero_;
}

void Entity::jump() {
    velocity_ = -20.0;
    isOnSurface_ = false;
}

bool Entity::isOnSurface() const {
    return isOnSurface_;
}
