#include <Physics.hpp>

Physics* Physics::instance_ = nullptr;
Physics* Physics::GetInstance() {
    if (instance_ == nullptr) {
        instance_ = new Physics();
    }
    return instance_;
}

void Physics::updateEntity(Entity &entity) {
    bool wasOnSurface = entity.isOnSurface();
    entity.accelerate(GRAVITY);
    entity.update();
    if (wasOnSurface && !entity.isOnSurface()) {
        notify(entity, EVENT_START_FALL);
    }
    else if(!wasOnSurface && entity.isOnSurface()) {
        notify(entity, EVENT_ENTITY_FELL);
    }
}
