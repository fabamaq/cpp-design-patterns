/******************************************************************************
 * @file ObserverPatternTest.cpp
 *
 * @brief Observer design pattern
 * - "Define a one-to-many dependency between objects so that
 * ... when one object changes state,
 * ... all its dependents are notified and updated automatically
 *****************************************************************************/
#include <Entity.hpp>
#include <Physics.hpp>
#include <Achievements.hpp>

#include <gtest/gtest.h>

TEST(ObserverPatternTest, updateEntity) {
    Achievements achievements(true);
    Entity hero(true);
    hero.jump(); // velocity_ = -20.0; isOnSurface_ = false;
    // calls hero.accelerate, update; and notifies
    Physics::GetInstance()->updateEntity(hero); // hero.velocity_ = -10.0;
    // calls hero.accelerate, update; and notifies
    Physics::GetInstance()->updateEntity(hero); // hero.velocity_ = 0.0;
}