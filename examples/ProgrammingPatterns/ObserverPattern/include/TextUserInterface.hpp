#ifndef GAMEPROGRAMMINGPATTERNS_TEXTUSERINTERFACE_HPP
#define GAMEPROGRAMMINGPATTERNS_TEXTUSERINTERFACE_HPP

#include <iostream>

class TextUserInterface {
public:
    static void display(const char* message);
};

#endif //GAMEPROGRAMMINGPATTERNS_TEXTUSERINTERFACE_HPP
