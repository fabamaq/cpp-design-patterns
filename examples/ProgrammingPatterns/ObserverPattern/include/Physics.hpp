#ifndef GAMEPROGRAMMINGPATTERNS_PHYSICS_HPP
#define GAMEPROGRAMMINGPATTERNS_PHYSICS_HPP

#include <Entity.hpp>
#include <Subject.hpp>

static const double GRAVITY = 10;

class Physics : public Subject {
    static Physics* instance_;
    Physics() = default;
public:
    static Physics* GetInstance();
    void updateEntity(Entity & entity);
};
#endif //GAMEPROGRAMMINGPATTERNS_PHYSICS_HPP
