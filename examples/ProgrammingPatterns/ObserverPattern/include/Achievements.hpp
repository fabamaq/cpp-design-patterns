#ifndef GAMEPROGRAMMINGPATTERNS_ACHIEVEMENTS_HPP
#define GAMEPROGRAMMINGPATTERNS_ACHIEVEMENTS_HPP

#include <Observer.hpp>
#include <Entity.hpp>
#include <Event.hpp>
#include <TextUserInterface.hpp>

using tui = TextUserInterface;

enum Achievement {
    ACHIEVEMENT_FELL_OFF_BRIDGE
};

const char *stringifyAchievement(Achievement achievement) {
    switch (achievement) {
        case ACHIEVEMENT_FELL_OFF_BRIDGE:
            return "ACHIEVEMENT_FELL_OFF_BRIDGE";
        default:
            return "Unknown achievement";
    }
}

class Achievements : public Observer {
    virtual void onNotify(const Entity &entity, Event event) {
        switch (event) {
            case EVENT_ENTITY_FELL:
                if (entity.isHero() && heroIsOnBridge_) {
                    unlock(ACHIEVEMENT_FELL_OFF_BRIDGE);
                }
                break;
            case EVENT_START_FALL:
                tui::display("Entity started to fall");
                break;
                // Handle other events...
                // Update heroIsOnBridge_...
        }
    }

public:
    explicit Achievements(bool heroIsOnBridge) : heroIsOnBridge_(heroIsOnBridge) {
        Physics::GetInstance()->addObserver(this);

    }

    ~Achievements() {
        Physics::GetInstance()->removeObserver(this);
    }

private:
    void unlock(Achievement achievement) {
        tui::display(stringifyAchievement(achievement));
    }

    bool heroIsOnBridge_;
};

#endif //GAMEPROGRAMMINGPATTERNS_ACHIEVEMENTS_HPP
