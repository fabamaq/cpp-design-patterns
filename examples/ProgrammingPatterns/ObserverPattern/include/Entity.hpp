#ifndef GAMEPROGRAMMINGPATTERNS_ENTITY_HPP
#define GAMEPROGRAMMINGPATTERNS_ENTITY_HPP

class Entity {
public:
    explicit Entity(bool isHero);
    bool isOnSurface() const;
    void accelerate(double velocity);
    void update();
    bool isHero() const;
    void jump();
private:
    bool isHero_;
    bool isOnSurface_;
    double velocity_;
};

#endif //GAMEPROGRAMMINGPATTERNS_ENTITY_HPP
