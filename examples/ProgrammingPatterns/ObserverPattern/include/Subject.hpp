#ifndef GAMEPROGRAMMINGPATTERNS_SUBJECT_HPP
#define GAMEPROGRAMMINGPATTERNS_SUBJECT_HPP

#include <Observer.hpp>
#include <Entity.hpp>
#include <Event.hpp>

class Subject {
private:
    static const int MAX_OBSERVERS = 10;
    Observer *observers_[MAX_OBSERVERS];
    int numObservers_;
public:
    Subject() {
        for (int i = 0; i < MAX_OBSERVERS; i++) {
            observers_[i] = nullptr;
        }
        numObservers_ = 0;
    }

    void addObserver(Observer *observer) {
        if (numObservers_ < MAX_OBSERVERS) {
            observers_[numObservers_++] = observer;
        }
    }

    void removeObserver(Observer *observer) {
        // Remove from array...
        for (int i = 0; i < numObservers_; i++) {
            if (observers_[i] == observer) {
                observers_[i] = nullptr;
            }
        }

        // Remove from linked_list...
        /*
         * // singly linked list or doubly linked list (const time)
         * if (head_ == observer) {
         *      head_ = observer->next_;
         *      observer->next_ = NULL;
         *      return;
         * }
         *
         * Observer* current = head_;
         * while (current != NULL) {
         *      if (current->next_ == observer) {
         *          current->next_ = observer->next_;
         *          observer->next_ = NULL;
         *          return;
         *      }
         *
         *      current = current->next_;
         * }
         */
    }

    // Other stuff...

protected:
    void notify(const Entity &entity, Event event) {
        for (int i = 0; i < numObservers_; i++) {
            observers_[i]->onNotify(entity, event);
        }

        // using a linked list
        /*
         * Observer* observer = head_;
         * while (observer != NULL) {
         *      observer->onNotify(entity, event);
         *      observer = observer->next_;
         * }
         */
    }
};

#endif //GAMEPROGRAMMINGPATTERNS_SUBJECT_HPP
