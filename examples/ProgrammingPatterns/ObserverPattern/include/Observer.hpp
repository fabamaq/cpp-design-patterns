#ifndef GAMEPROGRAMMINGPATTERNS_OBSERVER_HPP
#define GAMEPROGRAMMINGPATTERNS_OBSERVER_HPP

#include <Entity.hpp>
#include <Event.hpp>

class Observer {
public:
    virtual ~Observer() {}
    virtual void onNotify(const Entity& entity, Event event) = 0;
};

#endif //GAMEPROGRAMMINGPATTERNS_OBSERVER_HPP
