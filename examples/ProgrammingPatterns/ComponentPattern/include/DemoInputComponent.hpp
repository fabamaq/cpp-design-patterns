#ifndef GAMEPROGRAMMINGPATTERNS_DEMOINPUTCOMPONENT_HPP
#define GAMEPROGRAMMINGPATTERNS_DEMOINPUTCOMPONENT_HPP

#include <InputComponent.hpp>

class DemoInputComponent : public InputComponent {
public:
    void update(Bjorn &bjorn) override;
};


#endif //GAMEPROGRAMMINGPATTERNS_DEMOINPUTCOMPONENT_HPP
