#ifndef GAMEPROGRAMMINGPATTERNS_BJORNGRAPHICSCOMPONENT_HPP
#define GAMEPROGRAMMINGPATTERNS_BJORNGRAPHICSCOMPONENT_HPP

#include <GraphicsComponent.hpp>

class BjornGraphicsComponent : public GraphicsComponent {
    void update(GameObject &object, Graphics &graphics) override;
private:
    Sprite spriteStand_;
    Sprite spriteWalkLeft_;
    Sprite spriteWalkRight_;
};

#endif //GAMEPROGRAMMINGPATTERNS_BJORNGRAPHICSCOMPONENT_HPP
