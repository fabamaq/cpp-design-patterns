#ifndef GAMEPROGRAMMINGPATTERNS_CONTROLLER_HPP
#define GAMEPROGRAMMINGPATTERNS_CONTROLLER_HPP

enum DIRECTION {
    DIR_LEFT,
    DIR_RIGHT
};

class Controller {
public:
    static int getJoystickDirection() {
        return DIR_LEFT;
    }
};

#endif //GAMEPROGRAMMINGPATTERNS_CONTROLLER_HPP
