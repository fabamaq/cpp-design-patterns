#ifndef GAMEPROGRAMMINGPATTERNS_GAMEOBJECT_HPP
#define GAMEPROGRAMMINGPATTERNS_GAMEOBJECT_HPP

#include <World.hpp>
#include <Graphics.hpp>
#include <InputComponent.hpp>
#include <PhysicsComponent.hpp>
#include <GraphicsComponent.hpp>

class GameObject {
public:
    int velocity;
    int x, y;

    GameObject(InputComponent *input,
               PhysicsComponent *physics,
               GraphicsComponent *graphics)
            : input_(input), physics_(physics), graphics_(graphics) {}

    void update(World& world, Graphics& graphics);

private:
    InputComponent *input_;
    PhysicsComponent *physics_;
    GraphicsComponent *graphics_;
};

#endif //GAMEPROGRAMMINGPATTERNS_GAMEOBJECT_HPP
