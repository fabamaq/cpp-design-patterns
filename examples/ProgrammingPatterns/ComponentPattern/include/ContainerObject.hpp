#ifndef GAMEPROGRAMMINGPATTERNS_CONTAINEROBJECT_HPP
#define GAMEPROGRAMMINGPATTERNS_CONTAINEROBJECT_HPP

#include <Component.hpp>

class ContainerObject {
public:
    void send(int message) {
        for (int i = 0; i < MAX_COMPONENTS; ++i) {
            if (components_[i] != NULL) {
                components_[i]->receive(message);
            }
        }
    }

private:
    static const int MAX_COMPONENTS = 10;
    Component *components_[MAX_COMPONENTS];
};

#endif //GAMEPROGRAMMINGPATTERNS_CONTAINEROBJECT_HPP
