#ifndef GAMEPROGRAMMINGPATTERNS_GRAPHICSCOMPONENT_HPP
#define GAMEPROGRAMMINGPATTERNS_GRAPHICSCOMPONENT_HPP

class GameObject;
//#include <GameObject.hpp>
#include <Graphics.hpp>
#include <Sprite.hpp>

class GraphicsComponent {
public:
    virtual ~GraphicsComponent() = default;
    virtual void update(GameObject &object, Graphics &graphics) = 0;
//
//private:
//    Sprite spriteStand_;
//    Sprite spriteWalkLeft_;
//    Sprite spriteWalkRight_;
};

#endif //GAMEPROGRAMMINGPATTERNS_GRAPHICSCOMPONENT_HPP
