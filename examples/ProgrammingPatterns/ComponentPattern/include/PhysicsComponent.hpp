#ifndef GAMEPROGRAMMINGPATTERNS_PHYSICSCOMPONENT_HPP
#define GAMEPROGRAMMINGPATTERNS_PHYSICSCOMPONENT_HPP

#include <Volume.hpp>

//#include <GameObject.hpp>
class GameObject;

//#include <World.hpp>
class World;

class PhysicsComponent {
public:
    virtual ~PhysicsComponent() = default;
    virtual void update(GameObject &object, World &world) = 0;

//private:
//    Volume volume_;
};

#endif //GAMEPROGRAMMINGPATTERNS_PHYSICSCOMPONENT_HPP
