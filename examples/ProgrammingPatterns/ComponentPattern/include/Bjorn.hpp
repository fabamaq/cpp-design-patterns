// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_BJORN_HPP
#define GAMEPROGRAMMINGPATTERNS_BJORN_HPP

#include <World.hpp>
#include <Graphics.hpp>

#include <InputComponent.hpp>
#include <PhysicsComponent.hpp>
#include <GraphicsComponent.hpp>

//class InputComponent;
//class PhysicsComponent;
//class GraphicsComponent;

//struct Volume {
//    int value;
//};

//enum DIRECTION {
//    DIR_LEFT,
//    DIR_RIGHT
//};

//class Controller {
//public:
//    static int getJoystickDirection() {
//        return DIR_LEFT;
//    }
//};

class Bjorn {
public:
    int velocity;
    int x, y;

    Bjorn(InputComponent *input)
            : input_(input) {}
    //Bjorn() : velocity_(0), x_(0), y_(0) {}

    void update(World &world, Graphics &graphics);

private:
//    static const int WALK_ACCELERATION = 1;
    InputComponent *input_;
    PhysicsComponent* physics_;
    GraphicsComponent* graphics_;

//    int velocity_;
//    int x_, y_;

    // Physics Component
//    Volume volume_;

    // Graphics Component
//    Sprite spriteStand_;
//    Sprite spriteWalkLeft_;
//    Sprite spriteWalkRight_;
};

#endif //GAMEPROGRAMMINGPATTERNS_BJORN_HPP
