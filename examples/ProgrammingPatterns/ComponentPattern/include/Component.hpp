#ifndef GAMEPROGRAMMINGPATTERNS_COMPONENT_HPP
#define GAMEPROGRAMMINGPATTERNS_COMPONENT_HPP

class Component {
public:
    virtual ~Component() = default;
    virtual void receive(int message) = 0;
};

#endif //GAMEPROGRAMMINGPATTERNS_COMPONENT_HPP
