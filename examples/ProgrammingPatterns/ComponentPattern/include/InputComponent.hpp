#ifndef GAMEPROGRAMMINGPATTERNS_INPUTCOMPONENT_HPP
#define GAMEPROGRAMMINGPATTERNS_INPUTCOMPONENT_HPP

//#include <Bjorn.hpp>
class Bjorn;

class InputComponent
{
public:
    virtual ~InputComponent() = default;
    virtual void update(Bjorn& bjorn) = 0;
//private:
//    static const int WALK_ACCELERATION = 1;
};

#endif //GAMEPROGRAMMINGPATTERNS_INPUTCOMPONENT_HPP
