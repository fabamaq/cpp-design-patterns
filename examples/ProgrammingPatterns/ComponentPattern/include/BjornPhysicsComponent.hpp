#ifndef GAMEPROGRAMMINGPATTERNS_FAKEPHYSICSCONTROLLER_HPP
#define GAMEPROGRAMMINGPATTERNS_FAKEPHYSICSCONTROLLER_HPP

#include <PhysicsComponent.hpp>

class BjornPhysicsComponent : public PhysicsComponent {
    void update(GameObject &object, World &world) override;

private:
    Volume volume_;
};

#endif //GAMEPROGRAMMINGPATTERNS_FAKEPHYSICSCONTROLLER_HPP
