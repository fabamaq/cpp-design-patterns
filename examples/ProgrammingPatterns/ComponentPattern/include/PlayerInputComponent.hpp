#ifndef GAMEPROGRAMMINGPATTERNS_PLAYERINPUTCOMPONENT_HPP
#define GAMEPROGRAMMINGPATTERNS_PLAYERINPUTCOMPONENT_HPP

#include <InputComponent.hpp>

class PlayerInputComponent : public InputComponent {
public:
    void update(Bjorn &bjorn) override;

private:
    static const int WALK_ACCELERATION = 1;
};

#endif //GAMEPROGRAMMINGPATTERNS_PLAYERINPUTCOMPONENT_HPP
