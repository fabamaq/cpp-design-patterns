/******************************************************************************
 *
 * @file ComponentPatternTest.cpp
 * @brief Component design pattern
 * - "Allow a single entity to span multiple domains without
 * ... coupling the domains to each other"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Component includes
//#include <Bjorn.hpp>
#include <GameObject.hpp>
#include <gtest/gtest.h>
#include <PlayerInputComponent.hpp>
#include <DemoInputComponent.hpp>
#include <BjornPhysicsComponent.hpp>
#include <BjornGraphicsComponent.hpp>

GameObject *createBjorn() {
    return new GameObject(
            new PlayerInputComponent(),
            new BjornPhysicsComponent(),
            new BjornGraphicsComponent()
    );
}

TEST(ComponentPatternTest, AComponent) {
    GameObject *bjorn = createBjorn();
    delete bjorn;
}