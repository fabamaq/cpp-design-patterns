#include <Bjorn.hpp>

void Bjorn::update(World &world, Graphics &graphics) {
    // MOVED TO INPUT COMPONENT
//    Apply user input ot hero's velocity.
//    switch (Controller::getJoystickDirection())
//    {
//        case DIR_LEFT:
//            velocity_ -= WALK_ACCELERATION;
//            break;
//        case DIR_RIGHT:
//            velocity_ += WALK_ACCELERATION;
//            break;
//    }

    input_->update(*this);
    physics_.update(*this, world);
    graphics_.update(*this, graphics);

    // Modify position by velocity.
//    x_ += velocity_;
//    world.resolveCollision(volume_, x_, y_, velocity_);

    // Draw the appropriate sprite.
//    Sprite* sprite = &spriteStand_;
//    if (velocity_ < 0) sprite = &spriteWalkLeft_;
//    else if (velocity_ > 0) sprite = &spriteWalkRight_;
//    graphics.draw(*sprite, x_, y_);
}