#include <BjornPhysicsComponent.hpp>
#include <GameObject.hpp>

void BjornPhysicsComponent::update(GameObject& object, World& world)
{
    object.x += object.velocity;
    world.resolveCollision(volume_, object.x, object.y, object.velocity);
}