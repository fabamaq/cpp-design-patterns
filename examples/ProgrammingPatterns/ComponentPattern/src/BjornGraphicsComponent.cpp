#include <BjornGraphicsComponent.hpp>
#include <GameObject.hpp>

void BjornGraphicsComponent::update(GameObject &object, Graphics &graphics) {
    Sprite *sprite = &spriteStand_;
    if (object.velocity < 0) {
        sprite = &spriteWalkLeft_;
    } else if (object.velocity > 0) {
        sprite = &spriteWalkRight_;
    }
    graphics.draw(*sprite, object.x, object.y);
}
