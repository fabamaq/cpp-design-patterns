#include <PlayerInputComponent.hpp>
#include <Controller.hpp>
#include <Bjorn.hpp>

void PlayerInputComponent::update(Bjorn &bjorn) {
    // Apply user input ot hero's velocity.
    switch (Controller::getJoystickDirection()) {
        case DIR_LEFT:
            bjorn.velocity -= WALK_ACCELERATION;
            break;
        case DIR_RIGHT:
            bjorn.velocity += WALK_ACCELERATION;
            break;
    }
}
