/******************************************************************************
 *
 * @file BytecodePatternTest.cpp
 *
 * @brief Bytecode design pattern
 * - "Give behavior the flexibility of data by encoding it as
 * ... instructions for a virtual machine."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include <VM.hpp>

#include <gtest/gtest.h>

TEST(BytecodePatternTest, BasicAssertion) {

}
