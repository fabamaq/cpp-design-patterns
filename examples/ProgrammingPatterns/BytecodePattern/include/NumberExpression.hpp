#ifndef GAMEPROGRAMMINGPATTERNS_NUMBEREXPRESSION_HPP
#define GAMEPROGRAMMINGPATTERNS_NUMBEREXPRESSION_HPP

#include <Expresssion.hpp>

class NumberExpression : public Expression {
public:
    NumberExpression(double value) : value_(value) {}

    virtual double evaluate() {
        return value_;
    }

private:
    double value_;
};

#endif //GAMEPROGRAMMINGPATTERNS_NUMBEREXPRESSION_HPP
