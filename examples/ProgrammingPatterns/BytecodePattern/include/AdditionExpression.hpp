#ifndef GAMEPROGRAMMINGPATTERNS_ADDITIONEXPRESSION_HPP
#define GAMEPROGRAMMINGPATTERNS_ADDITIONEXPRESSION_HPP

#include <Expresssion.hpp>

class AdditionExpression : public Expression {
public:
    AdditionExpression(Expression *lhs, Expression *rhs)
            : lhs_(lhs), rhs_(rhs) {}

    virtual double evaluate() {
        // Evaluate the operands.
        double lhs = lhs_->evaluate();
        double rhs = lhs_->evaluate();

        // Add them.
        return lhs + rhs;
    }

private:
    Expression *lhs_;
    Expression *rhs_;
};

#endif //GAMEPROGRAMMINGPATTERNS_ADDITIONEXPRESSION_HPP
