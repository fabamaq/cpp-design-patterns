#ifndef GAMEPROGRAMMINGPATTERNS_MULTIPLICATIONEXPRESSION_HPP
#define GAMEPROGRAMMINGPATTERNS_MULTIPLICATIONEXPRESSION_HPP

#include <Expresssion.hpp>

class MultiplicationExpression : public Expression {
public:
    MultiplicationExpression(Expression *lhs, Expression *rhs)
            : lhs_(lhs), rhs_(rhs) {}

    virtual double evaluate() {
        // Evaluate the operands.
        double lhs = lhs_->evaluate();
        double rhs = lhs_->evaluate();

        // Add them.
        return lhs * rhs;
    }

private:
    Expression *lhs_;
    Expression *rhs_;
};

#endif //GAMEPROGRAMMINGPATTERNS_MULTIPLICATIONEXPRESSION_HPP
