#ifndef GAMEPROGRAMMINGPATTERNS_EXPRESSSION_HPP
#define GAMEPROGRAMMINGPATTERNS_EXPRESSSION_HPP

class Expression
{
public:
    virtual ~Expression() = default;
    virtual double evaluate() = 0;
};

#endif //GAMEPROGRAMMINGPATTERNS_EXPRESSSION_HPP
