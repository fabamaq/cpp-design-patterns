// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_BREED_HPP
#define GAMEPROGRAMMINGPATTERNS_BREED_HPP

#include <Monster.hpp>

class Breed {
public:
    Monster *newMonster() {
        return new Monster(*this);
    }

    Breed(Breed *parent, int health, const char *attack)
            : parent_(parent), health_(health), attack_(attack) {
        // Inherit non-overridden attributes.
        if (parent != nullptr) {
            if (health == 0) {
                health_ = parent->getHealth();
            }

            if (attack == NULL) {
                attack_ = parent->getAttack();
            }
        }
    }

    int getHealth() {
        // Override.
        if (health_ != 0 || parent_ == nullptr) {
            return health_;
        }
        // Inherit.
        return parent_->getHealth();
    }

    const char *getAttack() {
        // Override.
        if (attack_ != NULL || parent_ == nullptr) {
            return attack_;
        }
        // Inherit.
        return parent_->getAttack();
    }

private:
    Breed *parent_;
    int health_; // Starting health.
    const char *attack_;
};

#endif //GAMEPROGRAMMINGPATTERNS_BREED_HPP
