// -------------------------------------------------------------- Include Guard
#ifndef GAMEPROGRAMMINGPATTERNS_MONSTER_HPP
#define GAMEPROGRAMMINGPATTERNS_MONSTER_HPP

// ------------------------------------------------------------------- Includes
#include <Breed.hpp>

class Monster {
    friend class Breed;

public:
    const char *getAttack() {
        if (health_ < LOW_HEALTH)
        {
            return "The monster flails weakly.";
        }
        
        return breed_.getAttack();
    }

private:
    Monster(Breed &breed)
            : health_(breed.getHealth()), breed_(breed) {}

    // Current health.
    int health_;
    Breed &breed_;
};

#endif //GAMEPROGRAMMINGPATTERNS_MONSTER_HPP
