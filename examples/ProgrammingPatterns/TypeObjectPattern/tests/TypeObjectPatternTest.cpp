/******************************************************************************
 *
 * @file TypeObjectPatternTest.cpp
 * @brief Type Object design pattern
 * - "Allow the flexible creation of new "classes" by creating a class,
 * ... each instance of which represents a different type of object"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Type Object includes
#include <Breed.hpp>
#include <Monster.hpp>

// Google Test Framework
#include <gtest/gtest.h>

TEST(TypeObjectPatternTest, CreateMonster) {
    Breed* dragon = new Breed(nullptr, 230, "Fire Breath");
    Monster* monster = dragon->newMonster();

    delete dragon;
    delete monster;
}

// Typical OOP answer
//class Monster {
//public:
//    virtual ~Monster() {}
//
//    virtual const char *getAttack() = 0;
//
//protected:
//    Monster(int startingHealth)
//            : health_(startingHealth) {}
//
//private:
//    int health_; // Current health.
//};
//
//class Dragon : public Monster {
//public:
//    Dragon() : Monster(239) {}
//    virtual const char* getAttack()
//    {
//        return "The dragon breathes fire!";
//    }
//};
//
//class Troll : public Monster
//{
//public:
//    Troll() : Monster(48) {}
//
//    virtual const char* getAttack()
//    {
//        return "The troll clubs you!";
//    }
//};
//

