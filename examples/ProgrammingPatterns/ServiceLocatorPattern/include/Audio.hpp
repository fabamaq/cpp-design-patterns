#ifndef GAMEPROGRAMMINGPATTERNS_AUDIO_HPP
#define GAMEPROGRAMMINGPATTERNS_AUDIO_HPP

class Audio
{
public:
    virtual ~Audio() {}
    virtual void playSound(int soundID) = 0;
    virtual void stopSound(int soundID) = 0;
    virtual void stopAllSounds() = 0;
};

class NullAudio : public Audio{
public:
    virtual void playSound(int soundID) {}
    virtual void stopSound(int soundID) {}
    virtual void stopAllSounds() {}
};

class LoggedAudio : public Audio
{
public:
    LoggedAudio(Audio &wrapped) : wrapped_(wrapped) {}

    virtual void playSound(int soundID)
    {
        log("play sound");
        wrapped_.playSound(soundID);
    }

    virtual void stopSound(int soundID)
    {
        log("stop sound");
        wrapped_.stopSound(soundID);
    }

    virtual void stopAllSounds() {
        log("stop all sounds");
        wrapped_.stopAllSounds();
    }

private:
    void log(const char* message)
    {
        // Code to log message ...
    }

    Audio &wrapped_;
};

#endif //GAMEPROGRAMMINGPATTERNS_AUDIO_HPP
