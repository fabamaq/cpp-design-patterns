#ifndef GAMEPROGRAMMINGPATTERNS_CONSOLEAUDIO_HPP
#define GAMEPROGRAMMINGPATTERNS_CONSOLEAUDIO_HPP

#include "Audio.hpp"

class ConsoleAudio : public Audio
{
public:
    virtual void playSound(int soundID)
    {
        // Play sound using console audio api...
    }

    virtual void stopSound(int soundID)
    {
        // Stop sound using console audio api...
    }

    virtual void stopAllSounds()
    {
        // Stop all sounds using console audio api...
    }
};

#endif //GAMEPROGRAMMINGPATTERNS_CONSOLEAUDIO_HPP
