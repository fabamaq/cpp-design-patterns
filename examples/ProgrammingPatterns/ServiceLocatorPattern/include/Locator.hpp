#ifndef GAMEPROGRAMMINGPATTERNS_LOCATOR_HPP
#define GAMEPROGRAMMINGPATTERNS_LOCATOR_HPP

#include "Audio.hpp"

class Locator
{
public:
    static void initialize()
    {
        service_ = &nullService_;
    }

    static Audio& getAudio() { return *service_; }

    static void provide(Audio* service)
    {
        // Revert to null service.
        if (service == nullptr) service = &nullService_;
        service_ = service;
    }

private:
    /*
     * Another approach: Bind at compile time
#if DEBUG
    static DebugAudio service_;
#else
    static ReleaseAudio service_;
#endif
     */
    static Audio* service_;
    static NullAudio nullService_;
};

#endif //GAMEPROGRAMMINGPATTERNS_LOCATOR_HPP
