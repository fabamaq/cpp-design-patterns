/******************************************************************************
 *
 * @file ServiceLocatorPatternTest.cpp
 *
 * @brief Service Locator design pattern
 * - "Provide a global point of access to a service without
 * ... coupling users to the concrete class that implements it."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Locator.hpp"
Audio* Locator::service_ = nullptr;
NullAudio Locator::nullService_ = NullAudio();

#include <gtest/gtest.h>

#include "ConsoleAudio.hpp"


class TestAudio : public Audio {
public:
    ~TestAudio() override = default;

    void playSound(int soundID) override {
        SUCCEED();
    }

    void stopSound(int soundID) override {}

    void stopAllSounds() override {}
};

constexpr int VERY_LOUD_BANG = 1;

[[maybe_unused]] void enableAudioLogging()
{
    // Decorate the existing service.
    Audio *service = new LoggedAudio(Locator::getAudio());

    // Swap it in.
    Locator::provide(service);
}

TEST(ServiceLocatorPatternTest, Assertion) {
    Locator::initialize();

    Locator::provide(new ConsoleAudio());
    Locator::getAudio().playSound(VERY_LOUD_BANG);

    Locator::provide(new TestAudio());
    Locator::getAudio().playSound(VERY_LOUD_BANG);
}
