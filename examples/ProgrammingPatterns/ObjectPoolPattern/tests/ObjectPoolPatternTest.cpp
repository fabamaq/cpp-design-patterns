/******************************************************************************
 *
 * @file ObjectPoolPatternTest.cpp
 *
 * @brief Object Pool design pattern
 * - "Improve performance and memory use by reusing objects from
 * ... a fixed pool instead of allocating and freeing them individually"
 *****************************************************************************/

#include <gtest/gtest.h>

/******************************************************************************
 * Particle
 *****************************************************************************/
class Particle {
public:
    Particle() : framesLeft_(0) {}

    void init(double x, double y, double xVel, double yVel, int lifetime);

    bool animate();

    bool inUse() const { return framesLeft_ > 0; }

    Particle *getNext() const { return state_.next; }

    void setNext(Particle *next) {
        state_.next = next;
    }

private:
    int framesLeft_;

    union {
        // State when it's in use.
        struct {
            double x, y, xVel, yVel;
        } live;

        // State when it's available.
        Particle *next;
    } state_;

//    double x_, y_;
//    double xVel_, yVel_;
};

void Particle::init(double x, double y, double xVel, double yVel, int lifetime) {
    state_.live.x = x;
    state_.live.y = y;
    state_.live.xVel = xVel;
    state_.live.yVel = yVel;
    framesLeft_ = lifetime;
}

bool Particle::animate() {
    if (!inUse()) return false;

    framesLeft_--;
    state_.live.x = state_.live.xVel;
    state_.live.y = state_.live.yVel;

    return framesLeft_ == 0;
}

/******************************************************************************
 * ParticlePool
 *****************************************************************************/
class ParticlePool {
public:
    ParticlePool();

    void create(double x, double y, double xVel, double yVel, int lifetime);

    void animate();

private:
    static const int POOL_SIZE = 100;
    Particle particles_[POOL_SIZE];

    Particle *firstAvailable_;
};

ParticlePool::ParticlePool() {
    // The first one is available.
    firstAvailable_ = &particles_[0];

    // Each particle points to the next.
    for (int i = 0; i < POOL_SIZE - 1; i++) {
        particles_[i].setNext(&particles_[i + 1]);
    }

    // The last one terminates the list.
    particles_[POOL_SIZE - 1].setNext(NULL);
}

void ParticlePool::animate() {
    for (int i = 0; i < POOL_SIZE; i++) {
        if (particles_[i].animate()) {
            // Add this particle to the front of the list.
            particles_[i].setNext(firstAvailable_);
            firstAvailable_ = &particles_[i];
        }
    }
}

void ParticlePool::create(double x, double y, double xVel, double yVel, int lifetime) {

    // Make sure the pool isn't full.
    assert(firstAvailable_ != NULL);

    // Remove it from the available list.
    Particle *newParticle = firstAvailable_;
    firstAvailable_ = newParticle->getNext();

    newParticle->init(x, y, xVel, yVel, lifetime);

//    for (int i = 0; i < POOL_SIZE; i++) {
//        if (!particles_[i].inUse()) {
//            particles_[i].init(x, y, xVel, yVel, lifetime);
//            return;
//        }
//    }
}

/******************************************************************************
 * ObjectPoolPatternTests
 *****************************************************************************/
TEST(ObjectPoolPatternTests, Assertion) {
    Particle p;
    ParticlePool pp;
    SUCCEED();
}

/******************************************************************************
 * GenericPool
 *****************************************************************************/
template<class TObject>
class GenericPool {
private:
    static const int POOL_SIZE = 100;

    TObject pool_[POOL_SIZE];
    bool inUse_[POOL_SIZE];
};
