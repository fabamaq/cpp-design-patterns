/******************************************************************************
 *
 * @file EventQueuePatternTest.cpp
 *
 * @brief Event Queue design pattern
 * - "Decouple when a message or event is sent from when it is processed."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Audio.hpp"

#include <gtest/gtest.h>

void startSound(ResourceId /*id*/, int /*channel*/, int volume) {
    if (volume == 1)
        SUCCEED();
    else
        FAIL();
}

TEST(EventQueuePatternTest, PlaySoundCallsStartSound) {
    Audio::init();

    SoundId id{1};
    int volume = 1;
    Audio::playSound(id, volume);

    Audio::update();
}