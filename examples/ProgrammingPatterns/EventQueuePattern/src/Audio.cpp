#include "Audio.hpp"

#include <cassert>

int max(int a, int b) {
    return b > a ? b : a;
}

int Audio::tail_ = 0;
int Audio::head_ = 0;
PlayMessage Audio::pending_[16]{0};

void Audio::playSound(SoundId id, int volume) {
    // Walk the pending requests.
    for (int i = head_; i != tail_; i = (i + 1) % MAX_PENDING) {
        if (pending_[i].id == id) {
            // Use the larger of the two volumes.
            pending_[i].volume = max(volume, pending_[i].volume);

            // Don't need to enqueue.
            return;
        }
    }

//    assert(numPending_ < MAX_PENDING);
//    assert(tail_ < MAX_PENDING);
    assert((tail_ + 1) % MAX_PENDING != head_);

    // Add to the end of the list
    pending_[tail_].id = id;
    pending_[tail_].volume = volume;
    //    numPending_++;
    //    tail_++;
    tail_ = (tail_ + 1) % MAX_PENDING;

    // MOVED to update()
//    ResourceId resource = loadSound(id);
//    int channel = findOpenChannel();
//    if (channel == -1) return;
//    startSound(resource, channel, volume);
}
