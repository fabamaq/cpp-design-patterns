#ifndef GAMEPROGRAMMINGPATTERNS_AUDIO_HPP
#define GAMEPROGRAMMINGPATTERNS_AUDIO_HPP

struct SoundId {
    int id;
};

inline bool operator== (const SoundId& lhs, const SoundId& rhs) {
    return lhs.id == rhs.id;
}

struct PlayMessage {
    SoundId id;
    int volume;
};


struct ResourceId {};

inline ResourceId loadSound(SoundId id) {
    return ResourceId{};
}

inline int findOpenChannel() {
    return 42;
}

void startSound(ResourceId id, int channel, int volume);

class Audio {
public:
    static void update() {
        // If there are no pending requests, do nothing.
        if (head_ == tail_) return;

        ResourceId resource = loadSound(pending_[head_].id);

        int channel = findOpenChannel();
        if (channel == -1) return;
        startSound(resource, channel, pending_[head_].volume);
//        head_++;
        head_ = (head_ + 1) % MAX_PENDING;

//        for(int i = 0; i < numPending_; i++) {
//            ResourceId resource = loadSound(pending_[i].id);
//            int channel = findOpenChannel();
//            if (channel == -1) return;
//            startSound(resource, channel, pending_[i].volume);
//        }
//
//        numPending_ = 0;
    }

    static void init() {
        head_ = 0;
        tail_ = 0;
//        numPending_ = 0;
    }

    static void playSound(SoundId id, int volume);

private:
    static int head_;
    static int tail_;

    // Array
    static const int MAX_PENDING = 16;
    static PlayMessage pending_[MAX_PENDING];
//    static int numPending_;
};

#endif //GAMEPROGRAMMINGPATTERNS_AUDIO_HPP
