# =============================================================================
# General Settings
# -----------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.0)
project(EventQueuePattern)

# =============================================================================
# Compiler settings
# -----------------------------------------------------------------------------
set(CMAKE_CXX_STANDARD 11)

# =============================================================================
# Workspace settings
# -----------------------------------------------------------------------------
set(EventQueuePatternSourcesFolder ${EventQueuePatternProjectFolder}/src)
set(EventQueuePatternIncludeFolder ${EventQueuePatternProjectFolder}/include)
set(EventQueuePatternTestsFolder ${EventQueuePatternProjectFolder}/tests)

# =============================================================================
# Targets settings
# -----------------------------------------------------------------------------
# Target Executable
add_executable(EventQueuePatternTest ${EventQueuePatternTestsFolder}/EventQueuePatternTest.cpp include/Audio.hpp src/Audio.cpp)
target_include_directories(EventQueuePatternTest PUBLIC ${EventQueuePatternIncludeFolder})
target_sources(EventQueuePatternTest PUBLIC ${EventQueuePatternSourcesFolder}/Audio.cpp)
target_link_libraries(EventQueuePatternTest PUBLIC gtest gtest_main)