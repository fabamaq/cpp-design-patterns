/******************************************************************************
 * @file InterpreterPatternTest.cpp
 *
 * @brief Interpreter design pattern
 * - "Given a language, define a representation for its grammar along with an
 * ... interpreter that uses the representation to interpret sentences in the
 * ... language."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes

#include "BooleanExp.hpp"
#include "Context.hpp"
#include "VariableExp.hpp"
#include "OrExp.hpp"
#include "AndExp.hpp"
#include "Constant.hpp"
#include "NotExp.hpp"

/**
 * BooleanEx ::=  VariableExp | Constant | OrExp | AndExp | NotExp |
 *                  '(' BooleanExp ')'
 * AndExp ::= BooleanExp 'and' BooleanExp
 * OrExp ::= BooleanExp 'or' BooleanExp
 * NotExp :== 'not' BooleanExp
 * Constant ::= 'true' | 'false'
 * VariableExp ::= 'A' | 'B' | ... | 'X' | 'Y' | 'Z'
 */

// Google Test Framework
#include <gtest/gtest.h>

// ---------------------------------------------------------------------- Tests
TEST(InterpreterPatternTest, DefaultFailingTest) {
    BooleanExp *expression;
    Context context;

    auto *x = new VariableExp("X");
    auto *y = new VariableExp("Y");

    expression = new OrExp(                    // false OR true => true
            new AndExp(new Constant(true), x), // true AND false => false
            new AndExp(y, new NotExp(x))       // true AND not false => true
    );

    context.Assign(x, false);
    context.Assign(y, true);

    bool result = expression->Evaluate(context);
    EXPECT_TRUE(result);
}
