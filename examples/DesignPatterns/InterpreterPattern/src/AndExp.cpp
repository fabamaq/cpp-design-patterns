/******************************************************************************
 * @file AndExp.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "AndExp.hpp"

AndExp::AndExp(BooleanExp *op1, BooleanExp *op2) {
    _operand1 = op1;
    _operand2 = op2;
}

AndExp::~AndExp() = default;

bool AndExp::Evaluate(Context &aContext) {
    return _operand1->Evaluate(aContext) &&
           _operand2->Evaluate(aContext);
}

BooleanExp *AndExp::Replace(const char *name, BooleanExp &exp) {
    return new AndExp(_operand1->Replace(name, exp),
                      _operand2->Replace(name, exp));
}

BooleanExp *AndExp::Copy() const {
    return new AndExp(_operand1->Copy(), _operand2->Copy());
}
