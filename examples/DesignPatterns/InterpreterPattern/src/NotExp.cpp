/******************************************************************************
 * @file NotExp.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "NotExp.hpp"

NotExp::NotExp(BooleanExp *exp) {
    _expression = exp;
}

NotExp::~NotExp() = default;

bool NotExp::Evaluate(Context &aContext) {
    return !_expression->Evaluate(aContext);
}

BooleanExp *NotExp::Replace(const char *name, BooleanExp &exp) {
    return new NotExp(_expression->Replace(name, exp));
}

BooleanExp *NotExp::Copy() const {
    return new NotExp(_expression->Copy());
}
