/******************************************************************************
 * @file VariableExp.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "VariableExp.hpp"

#include "Context.hpp"

#include <cstring>

VariableExp::VariableExp(const char *name) {
    #ifdef WIN32
    _name = _strdup(name);
    #else
    _name = strdup(name);
    #endif
}

VariableExp::~VariableExp() = default;

bool VariableExp::Evaluate(Context &aContext) {
    return aContext.Lookup(_name);
}

BooleanExp *VariableExp::Replace(const char *name, BooleanExp &exp) {
    if (strcmp(name, _name) == 0) {
        return exp.Copy();
    } else {
        return new VariableExp(_name);
    }
}

BooleanExp *VariableExp::Copy() const {
    return new VariableExp(_name);
}
