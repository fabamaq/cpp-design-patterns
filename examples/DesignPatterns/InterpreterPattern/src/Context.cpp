/******************************************************************************
 * @file Context.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Context.hpp"
#include <iostream>

bool Context::Lookup(const char *name) const {
    std::cout << "Context::Lookup()\n";
    return true;
}

void Context::Assign(VariableExp *exp, bool value) {
    std::cout << "Context::Assign()\n";
}
