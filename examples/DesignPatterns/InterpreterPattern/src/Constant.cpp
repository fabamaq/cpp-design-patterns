/******************************************************************************
 * @file Constant.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Constant.hpp"

Constant::Constant(bool constant) : _constant(constant) {}

Constant::~Constant() = default;

bool Constant::Evaluate(Context &aContext) {
    return _constant;
}

BooleanExp *Constant::Replace(const char *name, BooleanExp &exp) {
    return new Constant(_constant);
}

BooleanExp *Constant::Copy() const {
    return new Constant(_constant);
}
