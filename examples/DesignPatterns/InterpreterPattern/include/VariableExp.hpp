/******************************************************************************
 * @file VariableExp.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__INTERPRETER_PATTERN__VARIABLE_EXP__HPP
#define DESIGN_PATTERNS__INTERPRETER_PATTERN__VARIABLE_EXP__HPP

#include "BooleanExp.hpp"

// forward declaration
class Context;

class VariableExp : public BooleanExp {
public:
    explicit VariableExp(const char *);

    ~VariableExp() override;

    bool Evaluate(Context &) override;

    BooleanExp *Replace(const char *, BooleanExp &) override;

    BooleanExp *Copy() const override;

private:
    char *_name;
};

#endif // DESIGN_PATTERNS__INTERPRETER_PATTERN__VARIABLE_EXP__HPP
