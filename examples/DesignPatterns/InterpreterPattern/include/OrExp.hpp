/******************************************************************************
 * @file OrExp.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__INTERPRETER_PATTERN__OR_EXP__HPP
#define DESIGN_PATTERNS__INTERPRETER_PATTERN__OR_EXP__HPP

#include "BooleanExp.hpp"

class OrExp : public BooleanExp {
public:
    OrExp(BooleanExp *, BooleanExp *);

    ~OrExp() override;

    bool Evaluate(Context &) override;

    BooleanExp *Replace(const char *, BooleanExp &) override;

    BooleanExp *Copy() const override;

private:
    BooleanExp *_operand1;
    BooleanExp *_operand2;
};

#endif // DESIGN_PATTERNS__INTERPRETER_PATTERN__OR_EXP__HPP
