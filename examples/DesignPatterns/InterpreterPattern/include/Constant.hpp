/******************************************************************************
 * @file Constant.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__INTERPRETER_PATTERN__CONSTANT__HPP
#define DESIGN_PATTERNS__INTERPRETER_PATTERN__CONSTANT__HPP

#include "BooleanExp.hpp"

class Constant : public BooleanExp {
public:
    explicit Constant(bool);

    ~Constant() override;

    bool Evaluate(Context &) override;

    BooleanExp *Replace(const char *, BooleanExp &) override;

    BooleanExp *Copy() const override;

private:
    bool _constant;
};

#endif // DESIGN_PATTERNS__INTERPRETER_PATTERN__CONSTANT__HPP
