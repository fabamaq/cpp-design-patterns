/******************************************************************************
 * @file BooleanExp.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__INTERPRETER_PATTERN__BOOLEAN_EXP__HPP
#define DESIGN_PATTERNS__INTERPRETER_PATTERN__BOOLEAN_EXP__HPP

class Context;

class BooleanExp {
public:
    BooleanExp() = default;

    virtual ~BooleanExp() = default;

    virtual bool Evaluate(Context &) = 0;

    virtual BooleanExp *Replace(const char *, BooleanExp &) = 0;

    virtual BooleanExp *Copy() const = 0;
};

#endif // DESIGN_PATTERNS__INTERPRETER_PATTERN__BOOLEAN_EXP__HPP
