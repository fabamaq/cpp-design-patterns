/******************************************************************************
 * @file NotExp.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__INTERPRETER_PATTERN__NOT_EXP__HPP
#define DESIGN_PATTERNS__INTERPRETER_PATTERN__NOT_EXP__HPP

#include "BooleanExp.hpp"

class NotExp : public BooleanExp {
public:
    explicit NotExp(BooleanExp *);

    ~NotExp() override;

    bool Evaluate(Context &) override;

    BooleanExp *Replace(const char *, BooleanExp &) override;

    BooleanExp *Copy() const override;

private:
    BooleanExp *_expression;
};

#endif // DESIGN_PATTERNS__INTERPRETER_PATTERN__NOT_EXP__HPP