/******************************************************************************
 * @file Context.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__INTERPRETER_PATTERN__CONTEXT__HPP
#define DESIGN_PATTERNS__INTERPRETER_PATTERN__CONTEXT__HPP

// forward declaration
class VariableExp;

class Context {
public:
    bool Lookup(const char *) const;

    void Assign(VariableExp *, bool);
};

#endif // DESIGN_PATTERNS__INTERPRETER_PATTERN__CONTEXT__HPP
