/******************************************************************************
 * @file FlyweightPatternTest.cpp
 *
 * @brief Flyweight design pattern
 * - "Use sharing to support large numbers of fine-grained objects efficiently"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "GlyphContext.hpp"
#include "Font.hpp"

// Google Test Framework
#include <gtest/gtest.h>

// ---------------------------------------------------------------------- Tests
TEST(FlyweightPatternTest, DefaultFailingTest) {
    GlyphContext gc;
    Font* times12 = new Font("Times-Roman-12");
    Font* timesItalic12 = new Font("Times-Italic-12");
    // ...

    gc.SetFont(times12, 6);

    FAIL() << "Unimplemented Test";
}
