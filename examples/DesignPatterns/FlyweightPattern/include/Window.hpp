/******************************************************************************
 * @file Window.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__FLYWEIGHT_PATTERN__WINDOW__HPP
#define DESIGN_PATTERNS__FLYWEIGHT_PATTERN__WINDOW__HPP

class Window {
};

#endif // DESIGN_PATTERNS__FLYWEIGHT_PATTERN__WINDOW__HPP
