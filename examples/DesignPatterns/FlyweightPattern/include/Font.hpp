/******************************************************************************
 * @file Font.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__FLYWEIGHT_PATTERN__FONT__HPP
#define DESIGN_PATTERNS__FLYWEIGHT_PATTERN__FONT__HPP

class Font {
public:
    explicit Font(const char *);

private:
    const char *_font;
};

#endif // DESIGN_PATTERNS__FLYWEIGHT_PATTERN__FONT__HPP
