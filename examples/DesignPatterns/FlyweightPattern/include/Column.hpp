/******************************************************************************
 * @file Column.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__FLYWEIGHT_PATTERN__COLUMN__HPP
#define DESIGN_PATTERNS__FLYWEIGHT_PATTERN__COLUMN__HPP

class Column {
};

#endif // DESIGN_PATTERNS__FLYWEIGHT_PATTERN__COLUMN__HPP
