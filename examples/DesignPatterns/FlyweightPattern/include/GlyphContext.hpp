/******************************************************************************
 * @file GlyphContext.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__FLYWEIGHT_PATTERN__GLYPH_CONTEXT__HPP
#define DESIGN_PATTERNS__FLYWEIGHT_PATTERN__GLYPH_CONTEXT__HPP

#include "BTree.hpp"
#include "Font.hpp"

class GlyphContext {
public:
    GlyphContext();

    virtual ~GlyphContext();

    virtual void Next(int step = 1);

    virtual void Insert(int quantity = 1);

    virtual Font *GetFont();

    virtual void SetFont(Font *, int span = 1);

private:
    int _index;
    BTree *_fonts;
};

#endif // DESIGN_PATTERNS__FLYWEIGHT_PATTERN__GLYPH_CONTEXT__HPP
