/******************************************************************************
 * @file Character.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__FLYWEIGHT_PATTERN__CHARACTER__HPP
#define DESIGN_PATTERNS__FLYWEIGHT_PATTERN__CHARACTER__HPP

#include "Glyph.hpp"

class Character : public Glyph {
public:
    explicit Character(char);

    void Draw(Window *, GlyphContext &) override;

private:
    char _code;
};

#endif // DESIGN_PATTERNS__FLYWEIGHT_PATTERN__CHARACTER__HPP
