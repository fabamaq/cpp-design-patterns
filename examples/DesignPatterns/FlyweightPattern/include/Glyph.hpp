/******************************************************************************
 * @file Glyph.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__FLYWEIGHT_PATTERN__GLYPH__HPP
#define DESIGN_PATTERNS__FLYWEIGHT_PATTERN__GLYPH__HPP

#include "GlyphContext.hpp"
#include "Font.hpp"
#include "Window.hpp"

class Glyph {
public:
    virtual ~Glyph();

    virtual void Draw(Window *, GlyphContext &);

    virtual void SetFont(Font *, GlyphContext &);

    virtual Font *GetFont(GlyphContext &);

    virtual void First(GlyphContext &);

    virtual void Next(GlyphContext &);

    virtual bool IsDone(GlyphContext &);

    virtual Glyph *Current(GlyphContext &);

    virtual void Insert(Glyph *, GlyphContext &);

    virtual void Remove(GlyphContext &);

protected:
    Glyph();
};

#endif // DESIGN_PATTERNS__FLYWEIGHT_PATTERN__GLYPH__HPP
