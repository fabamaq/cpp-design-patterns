/******************************************************************************
 * @file GlyphFactory.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__FLYWEIGHT_PATTERN__GLYPH_FACTORY__HPP
#define DESIGN_PATTERNS__FLYWEIGHT_PATTERN__GLYPH_FACTORY__HPP

#include "Character.hpp"
#include "Row.hpp"
#include "Column.hpp"

const int N_CHAR_CODES = 128;

class GlyphFactory {
public:
    GlyphFactory();

    virtual ~GlyphFactory();

    virtual Character *CreateCharacter(char);

    virtual Row *CreateRow();

    virtual Column *CreateColumn();

private:
    Character *_character[N_CHAR_CODES];
};

#endif // DESIGN_PATTERNS__FLYWEIGHT_PATTERN__GLYPH_FACTORY__HPP
