/******************************************************************************
 * @file Character.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Character.hpp"

Character::Character(char c) : _code(c) {}

void Character::Draw(Window *w, GlyphContext &gc) {
    Glyph::Draw(w, gc);
}
