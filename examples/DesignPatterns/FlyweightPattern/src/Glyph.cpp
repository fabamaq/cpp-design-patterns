/******************************************************************************
 * @file Glyph.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Glyph.hpp"

Glyph::~Glyph() = default;

void Glyph::Draw(Window *, GlyphContext &) {

}

void Glyph::SetFont(Font *, GlyphContext &) {

}

Font *Glyph::GetFont(GlyphContext &) {
    return nullptr;
}

void Glyph::First(GlyphContext &) {

}

void Glyph::Next(GlyphContext &) {

}

bool Glyph::IsDone(GlyphContext &) {
    return false;
}

Glyph *Glyph::Current(GlyphContext &) {
    return nullptr;
}

void Glyph::Insert(Glyph *, GlyphContext &) {

}

void Glyph::Remove(GlyphContext &) {

}

Glyph::Glyph() = default;
