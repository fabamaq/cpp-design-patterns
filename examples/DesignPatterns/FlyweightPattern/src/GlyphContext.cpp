/******************************************************************************
 * @file GlyphContext.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "GlyphContext.hpp"

GlyphContext::GlyphContext() : _index(0), _fonts(nullptr) {}

GlyphContext::~GlyphContext() = default;

void GlyphContext::Next(int step) {

}

void GlyphContext::Insert(int quantity) {

}

Font *GlyphContext::GetFont() {
    return nullptr;
}

void GlyphContext::SetFont(Font *, int span) {

}
