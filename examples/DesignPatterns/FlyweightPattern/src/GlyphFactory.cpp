/******************************************************************************
 * @file GlyphFactory.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "GlyphFactory.hpp"

GlyphFactory::GlyphFactory() {
    for (int i = 0; i < N_CHAR_CODES; ++i) {
        _character[i] = nullptr;
    }
}

GlyphFactory::~GlyphFactory() = default;

Character *GlyphFactory::CreateCharacter(char c) {
    if (!_character[c]) {
        _character[c] = new Character(c);
    }

    return _character[c];
}

Row *GlyphFactory::CreateRow() {
    return new Row;
}

Column *GlyphFactory::CreateColumn() {
    return new Column;
}