/******************************************************************************
 * @file Composition.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Composition.hpp"

Composition::Composition(Compositor *c)
        : _compositor(c), _components(nullptr), _componentCount(0), _lineWidth(0),
          _lineBreaks(nullptr), _lineCount(0) {}

void Composition::Repair() {
    Coord *natural = nullptr;
    Coord *stretchability = nullptr;
    Coord *shrinkability = nullptr;
    int componentCount = 0;
    int *breaks = nullptr;

    // prepare the arrays with the desired component sizes
    // ...

    // determine where the breaks are:
    int breakCount;
    breakCount = _compositor->Compose(natural, stretchability, shrinkability, componentCount,
                                      _lineWidth, breaks);

    // lay out components according to breaks
    // ...
}
