/******************************************************************************
 * @file SimpleCompositor.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "SimpleCompositor.hpp"
#include <iostream>

SimpleCompositor::SimpleCompositor() = default;

int SimpleCompositor::Compose(Coord *natural, Coord *stretch, Coord *shrink, int componentCount,
                              int lineWidth, int *breaks) {
    std::cout << "SimpleCompositor::Compose()\n";
    return 0;
}
