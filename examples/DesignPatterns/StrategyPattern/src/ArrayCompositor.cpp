/******************************************************************************
 * @file ArrayCompositor.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "ArrayCompositor.hpp"
#include <iostream>

ArrayCompositor::ArrayCompositor(int interval) : _interval(interval) {}

int ArrayCompositor::Compose(Coord *natural, Coord *stretch, Coord *shrink, int componentCount,
                             int lineWidth, int *breaks) {
    std::cout << "ArrayCompositor::Compose()\n";
    return 0;
}
