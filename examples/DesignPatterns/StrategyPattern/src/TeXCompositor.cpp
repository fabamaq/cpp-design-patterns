/******************************************************************************
 * @file TeXCompositor.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "TeXCompositor.hpp"
#include <iostream>

TeXCompositor::TeXCompositor() = default;

int TeXCompositor::Compose(Coord *natural, Coord *stretch, Coord *shrink, int componentCount,
                           int lineWidth, int *breaks) {
    std::cout << "TeXCompositor::Compose()\n";
    return 0;
}
