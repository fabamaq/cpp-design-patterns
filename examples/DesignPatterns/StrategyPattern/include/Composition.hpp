/******************************************************************************
 * @file Composition.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__STRATEGY_PATTERN__COMPOSITION__HPP
#define DESIGN_PATTERNS__STRATEGY_PATTERN__COMPOSITION__HPP

#include "Compositor.hpp"
#include "Component.hpp"

class Composition {
public:
    Composition(Compositor *);

    void Repair();

private:
    Compositor *_compositor;
    Component *_components;     // list of components
    int _componentCount;        // the number of components
    int _lineWidth;             // the Composition's line width
    int *_lineBreaks;           // the position of linebreaks in components
    int _lineCount;             // the number of lines
};

#endif // DESIGN_PATTERNS__STRATEGY_PATTERN__COMPOSITION__HPP
