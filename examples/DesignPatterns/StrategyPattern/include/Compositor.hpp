/******************************************************************************
 * @file Compositor.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__STRATEGY_PATTERN__COMPOSITOR__HPP
#define DESIGN_PATTERNS__STRATEGY_PATTERN__COMPOSITOR__HPP


#include <Point.hpp>

class Compositor {
public:
    virtual int
    Compose(Coord natural[], Coord stretch[], Coord shrink[], int componentCount, int lineWidth,
            int breaks[]) = 0;

protected:
    Compositor() = default;
};


#endif // DESIGN_PATTERNS__STRATEGY_PATTERN__COMPOSITOR__HPP
