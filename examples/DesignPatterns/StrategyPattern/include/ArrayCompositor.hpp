/******************************************************************************
 * @file ArrayCompositor.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__STRATEGY_PATTERN__ARRAY_COMPOSITOR__HPP
#define DESIGN_PATTERNS__STRATEGY_PATTERN__ARRAY_COMPOSITOR__HPP

#include "Compositor.hpp"

/**
 * ArrayCompositor breaks the components into lines at regular intervals
 */
class ArrayCompositor : public Compositor {
public:
    ArrayCompositor(int interval);

    virtual int
    Compose(Coord natural[], Coord stretch[], Coord shrink[], int componentCount, int lineWidth,
            int breaks[]
    );

private:
    int _interval;
};


#endif // DESIGN_PATTERNS__STRATEGY_PATTERN__ARRAY_COMPOSITOR__HPP
