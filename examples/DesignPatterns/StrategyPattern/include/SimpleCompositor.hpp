/******************************************************************************
 * @file SimpleCompositor.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__STRATEGY_PATTERN__SIMPLE_COMPOSITOR__HPP
#define DESIGN_PATTERNS__STRATEGY_PATTERN__SIMPLE_COMPOSITOR__HPP

#include "Compositor.hpp"

class SimpleCompositor : public Compositor {
public:
    SimpleCompositor();

    virtual int
    Compose(Coord natural[], Coord stretch[], Coord shrink[], int componentCount, int lineWidth,
            int breaks[]
    );
    // ...
};

#endif // DESIGN_PATTERNS__STRATEGY_PATTERN__SIMPLE_COMPOSITOR__HPP
