/******************************************************************************
 * @file TeXCompositor.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__STRATEGY_PATTERN__TEX_COMPOSITOR__HPP
#define DESIGN_PATTERNS__STRATEGY_PATTERN__TEX_COMPOSITOR__HPP

#include "Compositor.hpp"

/**
 * TeXCompositor uses a more global strategy. It examines a paragraph at a time,
 * taking into account the component's size and stretchability. It also tries to
 * give an even "color" to the paragraph by minimizing the whitespaces between
 * components
 */
class TeXCompositor : public Compositor {
public:
    TeXCompositor();

    virtual int
    Compose(Coord natural[], Coord stretch[], Coord shrink[], int componentCount, int lineWidth,
            int breaks[]
    );
    // ...
};


#endif // DESIGN_PATTERNS__STRATEGY_PATTERN__TEX_COMPOSITOR__HPP
