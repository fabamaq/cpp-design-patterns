/******************************************************************************
 * @file Component.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__STRATEGY_PATTERN__COMPONENT__HPP
#define DESIGN_PATTERNS__STRATEGY_PATTERN__COMPONENT__HPP

class Component {
public:
	Component() = default;
	~Component() = default;
};

#endif // DESIGN_PATTERNS__STRATEGY_PATTERN__COMPONENT__HPP
