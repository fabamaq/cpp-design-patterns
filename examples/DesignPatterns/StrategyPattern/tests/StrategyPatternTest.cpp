/**************************************************************************************************
 * @file StrategyPatternTest.cpp
 *
 * @brief Strategy design pattern
 * - "Define a family of algorithms, encapsulate each one, and make them interchangeable.
 * - Strategy lets the algorithm vary independently from clients that use it."
 *************************************************************************************************/
// --------------------------------------------------------------------------------------- Includes
// Google Test Framework
#include <gtest/gtest.h>

// Project Header Files
#include "Composition.hpp"
#include "Compositor.hpp"
#include "SimpleCompositor.hpp"
#include "TeXCompositor.hpp"
#include "ArrayCompositor.hpp"

// ------------------------------------------------------------------------------------------ Tests
TEST(StrategyPatternTest, DefaultFailingTest) {
    auto *quick = new Composition(new SimpleCompositor);
    auto *slick = new Composition(new TeXCompositor);
    auto *iconic = new Composition(new ArrayCompositor(100));

    quick->Repair();
    slick->Repair();
    iconic->Repair();

    delete quick;
    delete slick;
    delete iconic;
}
