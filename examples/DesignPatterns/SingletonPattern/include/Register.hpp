/******************************************************************************
 * @file Register.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__SINGLETON_PATTERN__REGISTER__HPP
#define DESIGN_PATTERNS__SINGLETON_PATTERN__REGISTER__HPP


class Register {
public:
	Register();
	~Register();
};


#endif // DESIGN_PATTERNS__SINGLETON_PATTERN__REGISTER__HPP
