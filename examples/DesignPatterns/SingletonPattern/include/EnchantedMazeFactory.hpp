/******************************************************************************
 * @file EnchantedMazeFactory.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__SINGLETON_PATTERN__ENCHANTED_MAZE_FACTORY__HPP
#define DESIGN_PATTERNS__SINGLETON_PATTERN__ENCHANTED_MAZE_FACTORY__HPP

#include "MazeFactory.hpp"

namespace Singleton {

    class EnchantedMazeFactory  : public MazeFactory {
    public:
        EnchantedMazeFactory();

        ~EnchantedMazeFactory();
    };

} // namespace Singleton

#endif // DESIGN_PATTERNS__SINGLETON_PATTERN__ENCHANTED_MAZE_FACTORY__HPP
