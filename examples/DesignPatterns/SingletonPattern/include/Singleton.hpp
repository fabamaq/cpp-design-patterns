#ifndef DESIGN_PATTERNS__SINGLETON_PATTERN__SINGLETON__HPP
#define DESIGN_PATTERNS__SINGLETON_PATTERN__SINGLETON__HPP

#include <utility>
#include "List.hpp"

class Singleton; // Forward Declaration
typedef std::pair<const char*, Singleton*> NameSingletonPair;

#include "Register.hpp"

class Singleton {
public:
    static void Register(const char* name, Singleton*);
    static Singleton *Instance();

protected:
    static Singleton* Lookup(const char* name);
    //Singleton();

private:
    static Singleton* _instance;
    static std::list<NameSingletonPair>* _registry;
};

#endif // DESIGN_PATTERNS__SINGLETON_PATTERN__SINGLETON__HPP
