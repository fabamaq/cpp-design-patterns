/******************************************************************************
 * @file BombedMazeFactory.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__SINGLETON_PATTERN__BOMBED_MAZE_FACTORY__HPP
#define DESIGN_PATTERNS__SINGLETON_PATTERN__BOMBED_MAZE_FACTORY__HPP

#include "MazeFactory.hpp"

namespace Singleton {

    class BombedMazeFactory : public MazeFactory {
    public:
        BombedMazeFactory();

        ~BombedMazeFactory();
    };

} // namespace Singleton

#endif // DESIGN_PATTERNS__SINGLETON_PATTERN__BOMBED_MAZE_FACTORY__HPP
