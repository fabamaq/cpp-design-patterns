#ifndef DESIGN_PATTERNS__SINGLETON_PATTERN__MAZE_FACTORY__HPP
#define DESIGN_PATTERNS__SINGLETON_PATTERN__MAZE_FACTORY__HPP


namespace Singleton {

    class MazeFactory {
    public:
        static MazeFactory *Instance();

        // existing interface goes here
    protected:
        MazeFactory();

    private:
        static MazeFactory *_instance;
    };

} // namespace Singleton

#endif //DESIGN_PATTERNS__SINGLETON_PATTERN__MAZE_FACTORY__HPP
