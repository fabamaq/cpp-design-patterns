#ifndef DESIGN_PATTERNS__SINGLETON_PATTERN__MY_SINGLETON__HPP
#define DESIGN_PATTERNS__SINGLETON_PATTERN__MY_SINGLETON__HPP

#include "Singleton.hpp"

class MySingleton : public Singleton {
public:
    MySingleton();
};

#endif //DESIGN_PATTERNS__SINGLETON_PATTERN__MY_SINGLETON__HPP
