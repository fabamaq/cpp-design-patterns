# =============================================================================
# General Settings
# -----------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.0)
project(SingletonPattern)

# =============================================================================
# Compiler settings
# -----------------------------------------------------------------------------
set(CMAKE_CXX_STANDARD 17)

# =============================================================================
# Workspace settings
# -----------------------------------------------------------------------------
set(SingletonPatternSourcesFolder ${SingletonPatternProjectFolder}/src)
set(SingletonPatternIncludeFolder ${SingletonPatternProjectFolder}/include)
set(SingletonPatternTestsFolder ${SingletonPatternProjectFolder}/tests)

# =============================================================================
# Scaffolding System
# -----------------------------------------------------------------------------
include(${ScaffoldingFolder}/GenerateClass.cmake)
# ! uncomment AFTER changing the ClassPath and ClassName
#GenerateClass("EnchantedMazeFactory" SingletonPattern)

# =============================================================================
# ReconfigureCMakeListsFile
# - Comments call to "GenerateClass"
# -----------------------------------------------------------------------------
set(CMakeFile ${SingletonPatternProjectFolder}/CMakeLists.txt)
file(READ ${CMakeFile} CMakeFileContents)
string(REGEX REPLACE "(.*)[^#]GenerateClass(\\(\".*)" "\\1\n#GenerateClass\\2" result ${CMakeFileContents})
file(WRITE ${CMakeFile} ${result})

# =============================================================================
# Targets settings
# -----------------------------------------------------------------------------
# Target Executable
add_executable(SingletonPatternTest ${SingletonPatternTestsFolder}/SingletonPatternTest.cpp)
target_include_directories(SingletonPatternTest PUBLIC
        ${SingletonPatternIncludeFolder}
        ${MazeGameProjectFolder}
        ${FoundationClassesProjectFolder}
)
# since we need to reload cmake to generate a new class, this works fine
file(GLOB files "${SingletonPatternSourcesFolder}/*.cpp")
set(SOURCES "")
foreach(file ${files})
    set(SOURCES ${SOURCES} ${file})
endforeach()
target_sources(SingletonPatternTest PUBLIC ${SOURCES})
target_link_libraries(SingletonPatternTest PUBLIC gtest gtest_main)
