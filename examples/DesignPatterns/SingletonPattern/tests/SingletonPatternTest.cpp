/******************************************************************************
 * @file SingletonPatternTest.cpp
 *
 * @brief Singleton design pattern
 * - "Ensure a class only has one instance,
 * ... and provide a global point of access to it"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Singleton.hpp"

// Google Test Framework
#include <gtest/gtest.h>

// ---------------------------------------------------------------------- Tests
TEST(SingletonPatternTest, DefaultFailingTest) {
	FAIL() << "Unimplemented Test";
}
