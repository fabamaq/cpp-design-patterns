/******************************************************************************
 * @file BombedMazeFactory.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "BombedMazeFactory.hpp"

namespace Singleton {

    BombedMazeFactory::BombedMazeFactory() = default;

    BombedMazeFactory::~BombedMazeFactory() = default;

} // namespace Singleton


