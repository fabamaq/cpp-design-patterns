#include <cstdlib>
#include <cstring>
#include "MazeFactory.hpp"
#include "BombedMazeFactory.hpp"
#include "EnchantedMazeFactory.hpp"

namespace Singleton {

    MazeFactory *MazeFactory::_instance = nullptr;

    MazeFactory *MazeFactory::Instance() {
        if (_instance == nullptr) {
            const char *mazeStyle = getenv("MAZESTYLE");

            if (strcmp(mazeStyle, "bombed") == 0) {
                _instance = new BombedMazeFactory;
            }  // ... other possible subclasses
            else if (strcmp(mazeStyle, "enchanted") == 0) {
                _instance = new EnchantedMazeFactory;
            } // ... default
            else {
                _instance = new MazeFactory;
            }
            _instance = new MazeFactory;
        }
        return _instance;
    }

    MazeFactory::MazeFactory() = default;

} // namespace Singleton