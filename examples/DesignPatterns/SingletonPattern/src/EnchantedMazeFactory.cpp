/******************************************************************************
 * @file EnchantedMazeFactory.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "EnchantedMazeFactory.hpp"

namespace Singleton {

    Singleton::EnchantedMazeFactory::EnchantedMazeFactory() = default;

    EnchantedMazeFactory::~EnchantedMazeFactory() = default;

} // namespace Singleton