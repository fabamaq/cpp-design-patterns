#include "Singleton.hpp"
#include <cstdlib>

Singleton *Singleton::_instance = nullptr;

Singleton *Singleton::Instance() {
    if (_instance == nullptr) {
        const char *singletonName = getenv("SINGLETON");
        // user or environment supplies this at startup

//        _instance = new Singleton;
        _instance = Lookup(singletonName);
        // Lookup returns 0 if there's no such singleton
    }
    return _instance;
}

void Singleton::Register(const char *name, Singleton *) {

}

Singleton *Singleton::Lookup(const char *name) {
    return nullptr;
}
