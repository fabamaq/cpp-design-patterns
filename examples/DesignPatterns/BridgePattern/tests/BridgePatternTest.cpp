/******************************************************************************
 * @file BridgePatternTest.cpp
 *
 * @brief Bridge design pattern
 * - "Decouple an abstraction from its implementation so that the two can vary
 * ... independently"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "ApplicationWindow.hpp"
#include "IconWindow.hpp"

// Google Test Framework
#include <gtest/gtest.h>

// ---------------------------------------------------------------------- Tests
TEST(BridgePatternTest, DefaultFailingTest) {
    FAIL() << "Unimplemented Test";
}
