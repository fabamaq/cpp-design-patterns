#include "PMWindowImp.hpp"

#include <cmath>
#include <algorithm>

PMWindowImp::PMWindowImp() = default;

void PMWindowImp::DeviceRect(Coord x0, Coord y0, Coord x1, Coord y1) {
    Coord left = std::min(x0, x1);
    Coord right = std::max(x0, x1);
    Coord bottom = std::min(y0, y1);
    Coord top = std::max(y0, y1);

    PPOINTL point[4];

    // clang-format off
    // @formatter:off
    point[0].x = left;      point[0].y = top;
    point[1].x = right;     point[1].y = top;
    point[2].x = right;     point[2].y = bottom;
    point[3].x = left;      point[3].y = bottom;

    if (
            ! GpiBeginPath(_hps, 1L) ||
            ! GpiSetuSetCurrentPosition(_hps, 4L, &point[3]) ||
            (GpiPolyLine(_hps, 4L, point) == GPI_ERROR) ||
            ! GpiEndPath(_hps)
    ) {
        reportError();
    } else {
        GpiStrokePath(_hps, 1L, 0L);
    }
    // @formatter:on
    // clang-format on
}


void PMWindowImp::DeviceText(const char *, Coord, Coord) {

}

void PMWindowImp::DeviceBitmap(const char *, Coord, Coord) {

}

void PMWindowImp::ImpTop() {

}

void PMWindowImp::ImpBottom() {

}

void PMWindowImp::ImpSetExtent(const Point &point) {

}

void PMWindowImp::ImpSetOrigin(const Point &point) {

}


bool PMWindowImp::GpiBeginPath(HPS hps, long i) {
    return false;
}


bool PMWindowImp::GpiSetuSetCurrentPosition(HPS hps, long i, PPOINTL *point) {
    return false;
}


int PMWindowImp::GpiPolyLine(HPS hps, long i, PPOINTL *point) {
    return GPI_OK;
}

bool PMWindowImp::GpiEndPath(HPS hps) {
    return false;
}

void PMWindowImp::GpiStrokePath(HPS hps, long i, long i1) {

}

void PMWindowImp::reportError() {

}