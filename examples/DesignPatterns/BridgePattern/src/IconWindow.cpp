#include "IconWindow.hpp"

void IconWindow::DrawContents() {
    WindowImp *imp = GetWindowImp();
    if (imp != nullptr) {
        imp->DeviceBitmap(_bitmapName, 0.0, 0.0);
    }
}