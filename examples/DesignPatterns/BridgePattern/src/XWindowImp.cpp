#include "XWindowImp.hpp"
#include <cmath>
#include <algorithm>

XWindowImp::XWindowImp() : _display(nullptr) {};

void XWindowImp::DeviceRect(Coord x0, Coord y0, Coord x1, Coord y1) {
    int x = std::round(std::min(x0, x1));
    int y = std::round(std::min(x0, x1));
    int w = std::round(std::abs(x0 - x1));
    int h = std::round(std::abs(y0 - y1));
    XDrawRectangle(_display, _window_id, _graphic_context, x, y, w, h);
}

void XWindowImp::DeviceText(const char *c, Coord c1, Coord c2) {

}

void XWindowImp::DeviceBitmap(const char *c, Coord c1, Coord c2) {

}

void XWindowImp::ImpTop() {

}

void XWindowImp::ImpBottom() {

}

void XWindowImp::ImpSetExtent(const Point &p) {

}

void XWindowImp::ImpSetOrigin(const Point &p) {

}

void XWindowImp::XDrawRectangle(Display *pDisplay, Drawable drawable, GC gc, int x, int y, int w,
                                int h) {

}
