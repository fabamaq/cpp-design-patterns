#include "WindowSystemFactory.hpp"
#include "XWindowImp.hpp"
#include "PMWindowImp.hpp"


WindowSystemFactory* WindowSystemFactory::_instance = nullptr;
WindowSystemFactory* WindowSystemFactory::Instance() {
    if (_instance == nullptr) {
        _instance = new WindowSystemFactory;
    }
    return _instance;
}

WindowImp* WindowSystemFactory::MakeWindowImp() {
#if defined(_WIN32)
    return new PMWindowImp();
#else
    return new XWindowImp();
#endif
}

WindowSystemFactory::WindowSystemFactory() = default;