#ifndef DESIGN_PATTERNS__WINDOW_SYSTEM_FACTORY__HPP
#define DESIGN_PATTERNS__WINDOW_SYSTEM_FACTORY__HPP

#include "WindowImp.hpp"

/**
 * @implements Abstract Factory design pattern
 * @implements Singleton design pattern
 */
class WindowSystemFactory {
public:
    static WindowSystemFactory* Instance();
    static WindowImp* MakeWindowImp();

protected:
    WindowSystemFactory();

private:
    static WindowSystemFactory* _instance;
};

#endif //DESIGN_PATTERNS__WINDOW_SYSTEM_FACTORY__HPP
