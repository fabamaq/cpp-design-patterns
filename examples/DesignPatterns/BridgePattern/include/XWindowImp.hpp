#ifndef DESIGN_PATTERNS__X_WINDOW_IMP__HPP
#define DESIGN_PATTERNS__X_WINDOW_IMP__HPP

#include "WindowImp.hpp"

class Display {
};

class Drawable {
};

class GC {
};

class XWindowImp : public WindowImp {
public:
    XWindowImp();

    void DeviceRect(Coord, Coord, Coord, Coord) override;

    void DeviceText(const char *, Coord, Coord) override;

    void DeviceBitmap(const char *, Coord, Coord) override;

    void ImpTop() override;

    void ImpBottom() override;

    void ImpSetExtent(const Point &) override;

    void ImpSetOrigin(const Point &) override;

    void XDrawRectangle(Display *pDisplay, Drawable drawable, GC gc, int x, int y, int w, int h);

private:
    // lots of X window system-specific state, including:
    Display *_display;
    Drawable _window_id;
    GC _graphic_context; // window graphic context
};

#endif //DESIGN_PATTERNS__X_WINDOW_IMP__HPP
