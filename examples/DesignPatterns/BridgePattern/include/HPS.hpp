/******************************************************************************
 * @file HPS.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__BRIDGE_PATTERN__HPS__HPP
#define DESIGN_PATTERNS__BRIDGE_PATTERN__HPS__HPP

class HPS {
public:
    HPS();

    ~HPS();
};


#endif // DESIGN_PATTERNS__BRIDGE_PATTERN__HPS__HPP
