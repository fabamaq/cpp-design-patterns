#ifndef DESIGN_PATTERNS__APPLICATION_WINDOW__HPP
#define DESIGN_PATTERNS__APPLICATION_WINDOW__HPP

#include "Window.hpp"

class ApplicationWindow : public Window {
public:
    // ...
    void DrawContents() override;
};

#endif //DESIGN_PATTERNS__APPLICATION_WINDOW__HPP
