#ifndef DESIGN_PATTERNS__PM_WINDOW_IMP__HPP
#define DESIGN_PATTERNS__PM_WINDOW_IMP__HPP

#include "WindowImp.hpp"
#include "HPS.hpp"

struct PPOINTL {
    Coord x;
    Coord y;
};


enum {
    GPI_ERROR = -1,
    GPI_OK = 0
};

class PMWindowImp : public WindowImp {
public:
    PMWindowImp();

    void DeviceRect(Coord, Coord, Coord, Coord) override;

    void DeviceText(const char *, Coord, Coord) override;

    void DeviceBitmap(const char *, Coord, Coord) override;

    void ImpTop() override;

    void ImpBottom() override;

    void ImpSetExtent(const Point &point) override;

    void ImpSetOrigin(const Point &point) override;

private:
    // lots of PM window system-specific state, including:
    HPS _hps;

    bool GpiBeginPath(HPS hps, long i);

    bool GpiSetuSetCurrentPosition(HPS hps, long i, PPOINTL *point);

    int GpiPolyLine(HPS hps, long i, PPOINTL* point);

    bool GpiEndPath(HPS hps);

    void reportError();

    void GpiStrokePath(HPS hps, long i, long i1);
};

#endif //DESIGN_PATTERNS__PM_WINDOW_IMP__HPP
