#ifndef DESIGN_PATTERNS__BRIDGE_PATTERN__VIEW__HPP
#define DESIGN_PATTERNS__BRIDGE_PATTERN__VIEW__HPP

class Window;

class View {
public:
    void DrawOn(Window *w) {}
};

#endif //DESIGN_PATTERNS__BRIDGE_PATTERN__VIEW__HPP
