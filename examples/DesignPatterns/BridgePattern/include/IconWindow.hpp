#ifndef DESIGN_PATTERNS__ICON_WINDOW__HPP
#define DESIGN_PATTERNS__ICON_WINDOW__HPP

#include "Window.hpp"

class IconWindow : public Window {
public:
    // ...
    void DrawContents() override;

private:
    const char *_bitmapName{};
};

#endif //DESIGN_PATTERNS__ICON_WINDOW__HPP
