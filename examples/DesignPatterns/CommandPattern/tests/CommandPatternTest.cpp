/******************************************************************************
 * @file CommandPatternTest.cpp
 *
 * @brief Command design pattern
 * - "Encapsulate a request as an object, thereby letting you parameterize
 * ... clients with different requests, queue or log requests, and
 * ... support undoable operations."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes

// Google Test Framework
#include <gtest/gtest.h>

// ---------------------------------------------------------------------- Tests
TEST(CommandPatternTest, DefaultFailingTest) {
	FAIL() << "Unimplemented Test";
}
