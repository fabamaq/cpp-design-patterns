/******************************************************************************
 * @file SimpleCommand.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "SimpleCommand.hpp"

template<class Receiver>
void SimpleCommand<Receiver>::Execute() {
    (_receiver->*_action)();
}