/******************************************************************************
 * @file MacroCommand.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "MacroCommand.hpp"
#include "ListIterator.hpp"

MacroCommand::MacroCommand() : _commands(new List<Command *>) {
}

MacroCommand::~MacroCommand() {
    delete _commands;
}

void MacroCommand::Add(Command *c) {
    _commands->Append(c);
}

void MacroCommand::Remove(Command *c) {
    _commands->Remove(c);
}

void MacroCommand::Execute() {
    ListIterator<Command *> i(_commands);

    for (i.First(); !i.IsDone(); i.Next()) {
        Command *c = i.CurrentItem();
        c->Execute();
    }
}
