/******************************************************************************
 * @file Document.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Document.hpp"
#include <iostream>

Document::Document(const char *name) : _name(name) {}

void Document::Open() {
    std::cout << "Opening document " << _name << '\n';
}

void Document::Paste() {
    std::cout << "Pasting into " << _name << " document\n";
}
