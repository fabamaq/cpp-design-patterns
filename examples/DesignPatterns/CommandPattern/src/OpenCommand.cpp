/******************************************************************************
 * @file OpenCommand.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "OpenCommand.hpp"
#include "Document.hpp"

#include <cstdio>

OpenCommand::OpenCommand(Application *a) : _application(a), _response(nullptr) {}

void OpenCommand::Execute() {
    const char *name = AskUser();

    if (name != nullptr) {
        auto *document = new Document(name);
        _application->Add(document);
        document->Open();
    }
}

const char *OpenCommand::AskUser() {
    puts("OpenCommand::AskUser()");
    return "yes\n";
}
