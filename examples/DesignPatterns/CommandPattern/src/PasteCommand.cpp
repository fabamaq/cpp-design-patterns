/******************************************************************************
 * @file PasteCommand.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "PasteCommand.hpp"

PasteCommand::PasteCommand(Document *doc) : _document(doc) {}

void PasteCommand::Execute() {
    _document->Paste();
}
