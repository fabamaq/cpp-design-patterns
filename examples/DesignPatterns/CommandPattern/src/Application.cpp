/******************************************************************************
 * @file Application.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Application.hpp"

void Application::Add(Document *pDocument) {
    _documents.push_back(pDocument);
}
