/******************************************************************************
 * @file OpenCommand.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__COMMAND_PATTERN__OPEN_COMMAND__HPP
#define DESIGN_PATTERNS__COMMAND_PATTERN__OPEN_COMMAND__HPP

#include "Command.hpp"
#include "Application.hpp"

class OpenCommand : public Command {
public:
    explicit OpenCommand(Application*);

    void Execute() override;

protected:
    virtual const char* AskUser();

private:
    Application* _application;
    char* _response;
};

#endif // DESIGN_PATTERNS__COMMAND_PATTERN__OPEN_COMMAND__HPP
