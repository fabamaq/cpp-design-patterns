/******************************************************************************
 * @file SimpleCommand.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__COMMAND_PATTERN__SIMPLE_COMMAND__HPP
#define DESIGN_PATTERNS__COMMAND_PATTERN__SIMPLE_COMMAND__HPP

#include "Command.hpp"

template<class Receiver>
class SimpleCommand : public Command {
public:
    typedef void (Receiver::* Action)();

    SimpleCommand(Receiver *r, Action a) : _receiver(r), _action(a) {}

    void Execute() override;

private:
    Action _action;
    Receiver *_receiver;
};

#endif // DESIGN_PATTERNS__COMMAND_PATTERN__SIMPLE_COMMAND__HPP
