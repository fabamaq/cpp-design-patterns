/******************************************************************************
 * @file Document.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__COMMAND_PATTERN__DOCUMENT__HPP
#define DESIGN_PATTERNS__COMMAND_PATTERN__DOCUMENT__HPP

class Document {

public:
    explicit Document(const char *name);

    void Open();

    void Paste();

private:
    const char *_name;
};

#endif // DESIGN_PATTERNS__COMMAND_PATTERN__DOCUMENT__HPP
