/******************************************************************************
 * @file MacroCommand.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__COMMAND_PATTERN__MACRO_COMMAND__HPP
#define DESIGN_PATTERNS__COMMAND_PATTERN__MACRO_COMMAND__HPP

#include "Command.hpp"
#include "List.hpp"

class MacroCommand : public Command {
public:
    MacroCommand();

    ~MacroCommand() override;

    virtual void Add(Command *);

    virtual void Remove(Command *);

    void Execute() override;

private:
    List<Command *> *_commands;
};

#endif // DESIGN_PATTERNS__COMMAND_PATTERN__MACRO_COMMAND__HPP
