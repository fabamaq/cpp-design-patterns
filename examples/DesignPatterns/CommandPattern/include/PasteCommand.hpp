/******************************************************************************
 * @file PasteCommand.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__COMMAND_PATTERN__PASTE_COMMAND__HPP
#define DESIGN_PATTERNS__COMMAND_PATTERN__PASTE_COMMAND__HPP

#include "Command.hpp"
#include "Document.hpp"

class PasteCommand : public Command {
    explicit PasteCommand(Document*);

    void Execute() override;

private:
    Document* _document;
};

#endif // DESIGN_PATTERNS__COMMAND_PATTERN__PASTE_COMMAND__HPP
