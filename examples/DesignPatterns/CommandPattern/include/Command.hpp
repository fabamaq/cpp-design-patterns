/******************************************************************************
 * @file Command.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__COMMAND_PATTERN__COMMAND__HPP
#define DESIGN_PATTERNS__COMMAND_PATTERN__COMMAND__HPP

class Command {
public:
    virtual ~Command() = default;

    virtual void Execute() = 0;

protected:
    Command() = default;
};

#endif // DESIGN_PATTERNS__COMMAND_PATTERN__COMMAND__HPP
