/******************************************************************************
 * @file Application.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__COMMAND_PATTERN__APPLICATION__HPP
#define DESIGN_PATTERNS__COMMAND_PATTERN__APPLICATION__HPP

#include "Document.hpp"
#include <vector>

class Application {
public:
    void Add(Document *pDocument);

private:
    std::vector<Document*> _documents;
};

#endif // DESIGN_PATTERNS__COMMAND_PATTERN__APPLICATION__HPP
