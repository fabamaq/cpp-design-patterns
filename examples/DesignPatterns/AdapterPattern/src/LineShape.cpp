/******************************************************************************
 * @file LineShape.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "LineShape.hpp"

LineShape::LineShape() {

}

LineShape::~LineShape() {

}

void LineShape::BoundingBox(Point &bottomLeft, Point &topRight) const {
    Shape::BoundingBox(bottomLeft, topRight);
}

Manipulator *LineShape::CreateManipulator() const {
    return Shape::CreateManipulator();
}
