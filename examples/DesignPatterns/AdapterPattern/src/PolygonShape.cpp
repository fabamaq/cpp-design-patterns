/******************************************************************************
 * @file PolygonShape.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "PolygonShape.hpp"


PolygonShape::PolygonShape() {

}

PolygonShape::~PolygonShape() {

}

void PolygonShape::BoundingBox(Point &bottomLeft, Point &topRight) const {
    Shape::BoundingBox(bottomLeft, topRight);
}

Manipulator *PolygonShape::CreateManipulator() const {
    return Shape::CreateManipulator();
}