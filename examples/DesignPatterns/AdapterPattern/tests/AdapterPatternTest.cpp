/******************************************************************************
 * @file AdapterPatternTest.cpp
 *
 * @brief Adapter design pattern
 * - "Convert the interface of a class into another interface clients expect.
 * ... Adapter lets classes work together that couldn't otherwise because of
 * ... incompatible interfaces"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "AdapterPatternTest.hpp"

// Google Test Framework
#include <gtest/gtest.h>

// ---------------------------------------------------------------------- Tests
TEST(AdapterPatternTest, MotivationExample) {
    LineShape lShape;
    PolygonShape pShape;

    Point bottomLeft(0,0);
    Point topRight(10,10);

    lShape.BoundingBox(bottomLeft, topRight);
    pShape.BoundingBox(bottomLeft, topRight);

    TextShapeClassAdapter classAdapter;
    classAdapter.BoundingBox(bottomLeft, topRight);

    TextView textView;
    TextShapeObjectAdapter objectAdapter(&textView);
    objectAdapter.BoundingBox(bottomLeft, topRight);
}
