#ifndef DESIGN_PATTERNS__MANIPULATOR__HPP
#define DESIGN_PATTERNS__MANIPULATOR__HPP

#include "Shape.hpp"

class Manipulator {

public:
    explicit Manipulator(Shape *);

private:
    Shape *shape_;
};

#endif //DESIGN_PATTERNS__MANIPULATOR__HPP
