/******************************************************************************
 * @file PolygonShape.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ADAPTER_PATTERN__POLYGON_SHAPE__HPP
#define DESIGN_PATTERNS__ADAPTER_PATTERN__POLYGON_SHAPE__HPP

#include "Shape.hpp"

class PolygonShape : public Shape {
public:
	PolygonShape();
	~PolygonShape();

    void BoundingBox(Point &bottomLeft, Point &topRight) const override;

    Manipulator *CreateManipulator() const override;
};


#endif // DESIGN_PATTERNS__ADAPTER_PATTERN__POLYGON_SHAPE__HPP
