/******************************************************************************
 * @file LineShape.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ADAPTER_PATTERN__LINE_SHAPE__HPP
#define DESIGN_PATTERNS__ADAPTER_PATTERN__LINE_SHAPE__HPP

#include "Shape.hpp"

class LineShape : public Shape {
public:
	LineShape();
	~LineShape();

    void BoundingBox(Point &bottomLeft, Point &topRight) const override;

    Manipulator *CreateManipulator() const override;
};


#endif // DESIGN_PATTERNS__ADAPTER_PATTERN__LINE_SHAPE__HPP
