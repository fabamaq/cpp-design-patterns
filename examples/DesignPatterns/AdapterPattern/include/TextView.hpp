#ifndef DESIGN_PATTERNS__TEXT_VIEW__HPP
#define DESIGN_PATTERNS__TEXT_VIEW__HPP

#include "Point.hpp"

class TextView {
public:
    TextView();

    virtual void GetOrigin(Coord &x, Coord &y) const;

    virtual void GetExtent(Coord &width, Coord &height) const;

    virtual bool IsEmpty() const;
};

#endif //DESIGN_PATTERNS__TEXT_VIEW__HPP
