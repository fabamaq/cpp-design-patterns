#ifndef DESIGN_PATTERNS__SHAPE__HPP
#define DESIGN_PATTERNS__SHAPE__HPP

#include "Point.hpp"

class Manipulator;

class Shape {
public:
    Shape();

    virtual void BoundingBox(Point &bottomLeft, Point &topRight) const;

    virtual Manipulator *CreateManipulator() const;
};

#endif //DESIGN_PATTERNS__SHAPE__HPP
