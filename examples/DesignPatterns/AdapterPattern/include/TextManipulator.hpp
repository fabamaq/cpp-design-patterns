#ifndef DESIGN_PATTERNS__ADAPTER_PATTERN__TEXT_MANIPULATOR__HPP
#define DESIGN_PATTERNS__ADAPTER_PATTERN__TEXT_MANIPULATOR__HPP

#include "Manipulator.hpp"

class TextManipulator : public Manipulator {
public:
    explicit TextManipulator(Shape *s);
};

#endif //DESIGN_PATTERNS__ADAPTER_PATTERN__TEXT_MANIPULATOR__HPP
