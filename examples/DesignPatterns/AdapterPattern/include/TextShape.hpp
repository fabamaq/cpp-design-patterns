#ifndef DESIGN_PATTERNS__TEXT_SHAPE__HPP
#define DESIGN_PATTERNS__TEXT_SHAPE__HPP

#include "Shape.hpp"
#include "TextView.hpp"
#include "Point.hpp"
#include "Manipulator.hpp"

// TextShape based on Class Adapter
class TextShapeClassAdapter : public Shape, private TextView {
public:
    TextShapeClassAdapter();

    virtual void BoundingBox(Point &bottomLeft, Point &topRight) const;

    bool IsEmpty() const override;

    virtual Manipulator *CreateManipulator() const;
};

// TextShape based on Object Adapter
class TextShapeObjectAdapter : public Shape {
public:
    explicit TextShapeObjectAdapter(TextView*);

    virtual void BoundingBox(Point &bottomLeft, Point &topRight) const;

    virtual bool IsEmpty() const ;

    virtual Manipulator *CreateManipulator() const;

private:
    TextView* _text;
};

#endif //DESIGN_PATTERNS__TEXT_SHAPE__HPP
