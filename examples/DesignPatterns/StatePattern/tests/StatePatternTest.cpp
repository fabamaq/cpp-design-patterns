/******************************************************************************
 * @file StatePatternTest.cpp
 *
 * @brief State design pattern
 * - "Allow an object to alter its behavior when its internal state changes.
 * ... The object will appear to change its class"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Google Test Framework
#include <gtest/gtest.h>

// Project Header Files
#include "TCPConnection.hpp"
#include "TCPState.hpp"
#include "TCPEstablished.hpp"
#include "TCPListen.hpp"
#include "TCPClosed.hpp"

// ---------------------------------------------------------------------- Tests
TEST(StatePatternTest, DefaultFailingTest) {
    TCPConnection t = TCPConnection();
    t.ActiveOpen();
}
