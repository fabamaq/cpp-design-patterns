/******************************************************************************
 * @file TCPState.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "TCPState.hpp"

void TCPState::Transmit(TCPConnection *, TCPOctetStream *) {

}

void TCPState::ActiveOpen(TCPConnection *) {

}

void TCPState::PassiveOpen(TCPConnection *) {

}

void TCPState::Close(TCPConnection *) {

}

void TCPState::Acknowledge(TCPConnection *) {

}

void TCPState::Synchronize(TCPConnection *) {

}

void TCPState::Send(TCPConnection *) {

}

void TCPState::ChangeState(TCPConnection *t, TCPState *s) {
    t->ChangeState(s);
}
