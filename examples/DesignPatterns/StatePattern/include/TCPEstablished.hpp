/******************************************************************************
 * @file TCPEstablished.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__STATE_PATTERN__TCP_ESTABLISHED__HPP
#define DESIGN_PATTERNS__STATE_PATTERN__TCP_ESTABLISHED__HPP

#include "TCPState.hpp"

class TCPEstablished : public TCPState {
public:
    static TCPState* Instance();

    virtual void Transmit(TCPConnection*, TCPOctetStream*);
    virtual void Close(TCPConnection*);

private:
    static TCPState* _instance;
};


#endif // DESIGN_PATTERNS__STATE_PATTERN__TCP_ESTABLISHED__HPP
