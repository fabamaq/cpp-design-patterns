/******************************************************************************
 * @file TCPListen.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__STATE_PATTERN__TCP_LISTEN__HPP
#define DESIGN_PATTERNS__STATE_PATTERN__TCP_LISTEN__HPP

#include "TCPState.hpp"

class TCPListen : public TCPState {
public:
    static TCPState *Instance();

    virtual void Send(TCPConnection *);
    // ...

private:
    static TCPState *_instance;
};


#endif // DESIGN_PATTERNS__STATE_PATTERN__TCP_LISTEN__HPP
