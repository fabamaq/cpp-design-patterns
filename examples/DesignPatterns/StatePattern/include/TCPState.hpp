/******************************************************************************
 * @file TCPState.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__STATE_PATTERN__TCP_STATE__HPP
#define DESIGN_PATTERNS__STATE_PATTERN__TCP_STATE__HPP

#include "TCPConnection.hpp"

class TCPState {
public:
    virtual void Transmit(TCPConnection *, TCPOctetStream *);

    virtual void ActiveOpen(TCPConnection *);

    virtual void PassiveOpen(TCPConnection *);

    virtual void Close(TCPConnection *);

    virtual void Acknowledge(TCPConnection *);

    virtual void Synchronize(TCPConnection *);

    virtual void Send(TCPConnection *);

protected:
    void ChangeState(TCPConnection *, TCPState *);
};


#endif // DESIGN_PATTERNS__STATE_PATTERN__TCP_STATE__HPP
