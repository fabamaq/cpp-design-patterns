/******************************************************************************
 * @file TCPClosed.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__STATE_PATTERN__TCP_CLOSED__HPP
#define DESIGN_PATTERNS__STATE_PATTERN__TCP_CLOSED__HPP

#include "TCPState.hpp"

class TCPClosed : public TCPState {
public:
    static TCPState *Instance();

    virtual void ActiveOpen(TCPConnection *);

    virtual void PassiveOpen(TCPConnection *);
    // ...

private:
    static TCPState *_instance;
};


#endif // DESIGN_PATTERNS__STATE_PATTERN__TCP_CLOSED__HPP
