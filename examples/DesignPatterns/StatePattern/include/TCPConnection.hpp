/******************************************************************************
 * @file TCPConnection.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__STATE_PATTERN__TCP_CONNECTION__HPP
#define DESIGN_PATTERNS__STATE_PATTERN__TCP_CONNECTION__HPP

class TCPOctetStream;

class TCPState;

class TCPConnection {
public:
    TCPConnection();

    void ActiveOpen();

    void PassiveOpen();

    void Close();

    void Send();

    void Acknowledge();

    void Synchronize();

    void ProcessOctet(TCPOctetStream *);

private:
    friend class TCPState;

    void ChangeState(TCPState *);

private:
    TCPState *_state;
};


#endif // DESIGN_PATTERNS__STATE_PATTERN__TCP_CONNECTION__HPP
