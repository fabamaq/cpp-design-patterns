/******************************************************************************
 * @file FacadePatternTest.cpp
 *
 * @brief Facade design pattern
 * - "Provide a unified interface to a set of interfaces in a subsystem. Facade
 * ... defines a higher-level interface that makes the subsystem easier to use"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Google Test Framework
#include <gtest/gtest.h>

#include "Compiler.hpp"
#include "StackMachineCodeGenerator.hpp"

// ---------------------------------------------------------------------- Tests
TEST(FacadePatternTest, DefaultFailingTest) {
    Compiler compiler;
    istream &input = std::cin;
    BytecodeStream output = BytecodeStream();
    compiler.Compile(input, output);
}
