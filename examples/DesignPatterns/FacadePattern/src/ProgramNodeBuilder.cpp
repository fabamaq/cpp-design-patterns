#include <ProgramNodeBuilder.hpp>

ProgramNodeBuilder::ProgramNodeBuilder() = default;

ProgramNode *ProgramNodeBuilder::NewVariable(const char *variableName) const {
    return nullptr;
}

ProgramNode *ProgramNodeBuilder::NewAssignment(ProgramNode *variable, ProgramNode *expression) const {
    return nullptr;
}

ProgramNode *ProgramNodeBuilder::NewReturnStatement(ProgramNode *value) const {
    return nullptr;
}

ProgramNode *
ProgramNodeBuilder::NewCondition(ProgramNode *condition, ProgramNode *truePart, ProgramNode *falsePart) const {
    return nullptr;
}

ProgramNode *ProgramNodeBuilder::GetRootNode() {
    return _node;
}
