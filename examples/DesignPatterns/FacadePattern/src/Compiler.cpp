#include "ProgramNodeBuilder.hpp"
#include "Parser.hpp"
#include "Compiler.hpp"
#include "Scanner.hpp"
#include "RISCCodeGenerator.hpp"

Compiler::Compiler() = default;

void Compiler::Compile(istream &input, BytecodeStream &output) {
    Scanner scanner(input);
    ProgramNodeBuilder builder;
    Parser parser;

    parser.Parse(scanner, builder);

    // TODO we might want to change the Compiler constructor to receive a CodeGenerator
    RISCCodeGenerator generator(output);
    ProgramNode *parseTree = builder.GetRootNode();
    parseTree->Traverse(generator);
}
