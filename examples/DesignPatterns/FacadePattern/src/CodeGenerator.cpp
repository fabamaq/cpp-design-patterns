
#include "CodeGenerator.hpp"
#include "StatementNode.hpp"
#include "ExpressionNode.hpp"

void CodeGenerator::Visit(StatementNode *node) {
    // ...
}

void CodeGenerator::Visit(ExpressionNode *node) {
    // ...
}

CodeGenerator::CodeGenerator(BytecodeStream &stream) : _output(stream) {
    // ...
}
