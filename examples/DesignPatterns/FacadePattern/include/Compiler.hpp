#ifndef DESIGN_PATTERNS__COMPILER__HPP
#define DESIGN_PATTERNS__COMPILER__HPP

#include "BytecodeStream.hpp"
#include <istream>

using std::istream;

class Compiler {
public:
    Compiler();

    virtual void Compile(istream &, BytecodeStream &);
};

#endif //DESIGN_PATTERNS__COMPILER__HPP
