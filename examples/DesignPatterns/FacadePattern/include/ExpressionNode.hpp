#ifndef DESIGN_PATTERNS__EXPRESSION_NODE__HPP
#define DESIGN_PATTERNS__EXPRESSION_NODE__HPP

#include "List.hpp"
#include "ProgramNode.hpp"

class ExpressionNode : public ProgramNode {
public:
    void Traverse(CodeGenerator &) override;

private:
    List<ProgramNode *> _children;
};

#endif //DESIGN_PATTERNS__EXPRESSION_NODE__HPP
