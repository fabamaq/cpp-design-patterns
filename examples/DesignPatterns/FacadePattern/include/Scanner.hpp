#ifndef DESIGN_PATTERNS__SCANNER__HPP
#define DESIGN_PATTERNS__SCANNER__HPP

#include <istream>

using std::istream;

class Token {
};

class Scanner {
public:
    explicit Scanner(istream &is) : _inputStream(is) {}

    virtual ~Scanner() = default;

    virtual Token &Scan() {
        // TODO: !!! MEMORY LEAK !!!
        auto *t = new Token;
        return *t;
    }

private:
    istream &_inputStream;
};

#endif //DESIGN_PATTERNS__SCANNER__HPP
