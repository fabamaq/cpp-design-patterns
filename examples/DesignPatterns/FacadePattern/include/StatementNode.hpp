#ifndef DESIGN_PATTERNS__STATEMENT_NODE__HPP
#define DESIGN_PATTERNS__STATEMENT_NODE__HPP

#include "ProgramNode.hpp"

class CodeGenerator;

class StatementNode : public ProgramNode {
public:
    void Traverse(CodeGenerator &) override;
};

#endif //DESIGN_PATTERNS__STATEMENT_NODE__HPP
