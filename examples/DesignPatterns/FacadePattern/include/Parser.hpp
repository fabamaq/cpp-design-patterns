#ifndef DESIGN_PATTERNS__PARSER__HPP
#define DESIGN_PATTERNS__PARSER__HPP

#include "Scanner.hpp"
#include "ProgramNodeBuilder.hpp"

class Parser {
public:
    Parser();

    virtual ~Parser();

    virtual void Parse(Scanner &, ProgramNodeBuilder &);
};

#endif //DESIGN_PATTERNS__PARSER__HPP
