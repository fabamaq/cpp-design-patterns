#ifndef DESIGN_PATTERNS__PROGRAM_NODE__HPP
#define DESIGN_PATTERNS__PROGRAM_NODE__HPP

#include "CodeGenerator.hpp"

class ProgramNode {
public:
    // program node manipulation
    virtual void GetSourcePosition(int &line, int &index);
    // ...

    // child manipulation
    virtual void Add(ProgramNode *);

    virtual void Remove(ProgramNode *);
    // ...

    virtual void Traverse(CodeGenerator &);

protected:
    ProgramNode();
};

#endif //DESIGN_PATTERNS__PROGRAM_NODE__HPP
