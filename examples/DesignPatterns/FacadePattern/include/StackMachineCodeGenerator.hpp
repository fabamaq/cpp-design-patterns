#ifndef DESIGN_PATTERNS__STACK_MACHINE_CODE_GENERATOR__HPP
#define DESIGN_PATTERNS__STACK_MACHINE_CODE_GENERATOR__HPP

#include "CodeGenerator.hpp"

class StackMachineCodeGenerator : public CodeGenerator {
public:
    explicit StackMachineCodeGenerator(BytecodeStream &stream) : CodeGenerator(stream) {}
};

#endif //DESIGN_PATTERNS__STACK_MACHINE_CODE_GENERATOR__HPP
