#ifndef DESIGN_PATTERNS__CODE_GENERATOR__HPP
#define DESIGN_PATTERNS__CODE_GENERATOR__HPP

#include "BytecodeStream.hpp"

class StatementNode;

class ExpressionNode;

class CodeGenerator {
public:
    virtual void Visit(StatementNode *);

    virtual void Visit(ExpressionNode *);
    // ...
protected:
    explicit CodeGenerator(BytecodeStream &);

protected:
    BytecodeStream &_output;
};

#endif //DESIGN_PATTERNS__CODE_GENERATOR__HPP
