#ifndef DESIGN_PATTERNS__RISC_CODE_GENERATOR__HPP
#define DESIGN_PATTERNS__RISC_CODE_GENERATOR__HPP

#include "CodeGenerator.hpp"

class RISCCodeGenerator : public CodeGenerator {
public:
    explicit RISCCodeGenerator(BytecodeStream &stream) : CodeGenerator(stream) {}
};

#endif //DESIGN_PATTERNS__RISC_CODE_GENERATOR__HPP
