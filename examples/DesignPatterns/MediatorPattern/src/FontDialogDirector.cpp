/******************************************************************************
 * @file FontDialogDirector.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "FontDialogDirector.hpp"

namespace Mediator {
    FontDialogDirector::FontDialogDirector()
            : _ok(nullptr), _cancel(nullptr), _fontList(nullptr), _fontName(nullptr) {
    }

    FontDialogDirector::~FontDialogDirector() = default;

    void FontDialogDirector::WidgetChanged(Widget *theChangedWidget) {
        if (theChangedWidget == _fontList) {
            _fontName->SetText(_fontList->GetSelection());
        } else if (theChangedWidget == _ok) {
            // apply font change and dismiss dialog
            // ...
        } else if (theChangedWidget == _cancel) {
            // dismiss dialog
        }
    }

    void FontDialogDirector::CreateWidgets() {
        _ok = new Button(this);
        _cancel = new Button(this);
        _fontList = new ListBox(this);
        _fontName = new EntryField(this);

        // fill the listBox with the available font names

        // assemble the widgets in the dialog
    }

    void FontDialogDirector::ShowDialog() {
        DialogDirector::ShowDialog();
    }

} // namespace Mediator