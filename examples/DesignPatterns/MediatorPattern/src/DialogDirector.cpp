/******************************************************************************
 * @file DialogDirector.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "DialogDirector.hpp"

namespace Mediator {

    DialogDirector::~DialogDirector() = default;

    void DialogDirector::ShowDialog() {
        // TODO
    }

    DialogDirector::DialogDirector() = default;

} // namespace Mediator
