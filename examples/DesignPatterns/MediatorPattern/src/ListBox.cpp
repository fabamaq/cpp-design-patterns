/******************************************************************************
 * @file ListBox.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "ListBox.hpp"
#include "DialogDirector.hpp"
#include "MouseEvent.hpp"

namespace Mediator {

    ListBox::ListBox(DialogDirector *d) : Widget(d) {}

    const char *ListBox::GetSelection() {
        return nullptr;
    }

    void ListBox::SetList(List<char *> *listItems) {

    }

    void ListBox::HandleMouse(MouseEvent &event) {
        Widget::HandleMouse(event);
    }

} // namespace Mediator