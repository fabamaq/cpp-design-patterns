/******************************************************************************
 * @file EntryField.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "EntryField.hpp"
#include "DialogDirector.hpp"
#include "MouseEvent.hpp"

namespace Mediator {

    EntryField::EntryField(DialogDirector *d) : Widget(d) {}

    void EntryField::SetText(const char *text) {

    }

    const char *EntryField::GetText() {
        return nullptr;
    }

    void EntryField::HandleMouse(MouseEvent &event) {
        Widget::HandleMouse(event);
    }

} // namespace Mediator
