/******************************************************************************
 * @file Widget.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Widget.hpp"
#include "DialogDirector.hpp"
#include "MouseEvent.hpp"

namespace Mediator {
    Widget::Widget(DialogDirector *d) : _director(d) {}

    void Widget::Changed() {
        _director->WidgetChanged(this);
    }

    void Widget::HandleMouse(MouseEvent &event) {
        // ...
    }

} // namespace Mediator