/******************************************************************************
 * @file Button.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include <string.h>
#include "Button.hpp"
#include "DialogDirector.hpp"
#include "MouseEvent.hpp"

namespace Mediator {

    Button::Button(DialogDirector *d) : Widget(d), _text("BUTTON") {}

    void Button::SetText(const char *text) {
        #ifdef WIN32
        _text = _strdup(text);
        #else
        _text = strdup(text);
        #endif
    }

    void Button::HandleMouse(MouseEvent &event) {
        Widget::HandleMouse(event);
        // ...
        Changed();
    }

} // namespace Mediator