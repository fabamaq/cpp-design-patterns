/******************************************************************************
 * @file MediatorPatternTest.cpp
 *
 * @brief Mediator design pattern
 * - "Define an object that encapsulates how a set of objects interact.
 * - Mediator promotes loose coupling by keeping objects from referring to each
 * ... other explicitly, and it lets you vary their interaction independently."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Google Test Framework
#include <gtest/gtest.h>

// Project Headers
#include "DialogDirector.hpp"
#include "Widget.hpp"
#include "ListBox.hpp"
#include "Button.hpp"
#include "EntryField.hpp"
#include "FontDialogDirector.hpp"

using namespace Mediator;

// ---------------------------------------------------------------------- Tests
TEST(MediatorPatternTest, DefaultFailingTest) {
    FontDialogDirector dialog = FontDialogDirector();
    dialog.ShowDialog();
}
