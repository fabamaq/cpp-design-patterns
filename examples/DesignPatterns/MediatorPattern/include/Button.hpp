/******************************************************************************
 * @file Button.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__MEDIATOR_PATTERN__BUTTON__HPP
#define DESIGN_PATTERNS__MEDIATOR_PATTERN__BUTTON__HPP

#include "Widget.hpp"

namespace Mediator {

// forward declaration
    class DialogDirector;

    class MouseEvent;

    class Button : public Widget {
    public:
        Button(DialogDirector *);

        virtual void SetText(const char *text);

        virtual void HandleMouse(MouseEvent &event);
        // ...

    private:
        const char* _text;
    };

} // namespace Mediator

#endif // DESIGN_PATTERNS__MEDIATOR_PATTERN__BUTTON__HPP
