/******************************************************************************
 * @file DialogDirector.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__MEDIATOR_PATTERN__DIALOG_DIRECTOR__HPP
#define DESIGN_PATTERNS__MEDIATOR_PATTERN__DIALOG_DIRECTOR__HPP

namespace Mediator {

    // forward declaration
    class Widget;

    class DialogDirector {
    public:
        virtual ~DialogDirector();

        virtual void ShowDialog();

        virtual void WidgetChanged(Widget *) = 0;

    protected:
        DialogDirector();

        virtual void CreateWidgets() = 0;
    };

} // namespace Mediator

#endif // DESIGN_PATTERNS__MEDIATOR_PATTERN__DIALOG_DIRECTOR__HPP
