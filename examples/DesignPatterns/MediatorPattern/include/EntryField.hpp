/******************************************************************************
 * @file EntryField.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__MEDIATOR_PATTERN__ENTRY_FIELD__HPP
#define DESIGN_PATTERNS__MEDIATOR_PATTERN__ENTRY_FIELD__HPP

#include "Widget.hpp"

namespace Mediator {

// forward declaration
    class DialogDirector;

    class MouseEvent;

    class EntryField : public Widget {
    public:
        EntryField(DialogDirector *);

        virtual void SetText(const char *text);

        virtual const char *GetText();

        virtual void HandleMouse(MouseEvent &event);
        // ...
    };

} // namespace Mediator


#endif // DESIGN_PATTERNS__MEDIATOR_PATTERN__ENTRY_FIELD__HPP
