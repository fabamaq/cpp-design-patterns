/******************************************************************************
 * @file MouseEvent.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__MEDIATOR_PATTERN__MOUSE_EVENT__HPP
#define DESIGN_PATTERNS__MEDIATOR_PATTERN__MOUSE_EVENT__HPP

class MouseEvent {};

#endif // DESIGN_PATTERNS__MEDIATOR_PATTERN__MOUSE_EVENT__HPP
