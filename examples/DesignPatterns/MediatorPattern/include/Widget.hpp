/******************************************************************************
 * @file Widget.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__MEDIATOR_PATTERN__WIDGET__HPP
#define DESIGN_PATTERNS__MEDIATOR_PATTERN__WIDGET__HPP

namespace Mediator {

    // forward declaration
    class DialogDirector;

    class MouseEvent;

    class Widget {
    public:
        Widget(DialogDirector *);

        virtual void Changed();

        virtual void HandleMouse(MouseEvent &event);
        // ...
    private:
        DialogDirector *_director;
    };

} // namespace Mediator

#endif // DESIGN_PATTERNS__MEDIATOR_PATTERN__WIDGET__HPP
