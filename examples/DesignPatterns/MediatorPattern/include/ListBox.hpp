/******************************************************************************
 * @file ListBox.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__MEDIATOR_PATTERN__LIST_BOX__HPP
#define DESIGN_PATTERNS__MEDIATOR_PATTERN__LIST_BOX__HPP

#include "Widget.hpp"
#include "List.hpp"

namespace Mediator {

    // forward declaration
    class DialogDirector;

    class MouseEvent;

    class ListBox : public Widget {
    public:
        ListBox(DialogDirector *);

        virtual const char *GetSelection();

        virtual void SetList(List<char *> *listItems);

        virtual void HandleMouse(MouseEvent &event);
        // ...
    };

} // namespace Mediator

#endif // DESIGN_PATTERNS__MEDIATOR_PATTERN__LIST_BOX__HPP
