/******************************************************************************
 * @file FontDialogDirector.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__MEDIATOR_PATTERN__FONT_DIALOG_DIRECTOR__HPP
#define DESIGN_PATTERNS__MEDIATOR_PATTERN__FONT_DIALOG_DIRECTOR__HPP

#include "DialogDirector.hpp"
#include "Button.hpp"
#include "ListBox.hpp"
#include "EntryField.hpp"

namespace Mediator {
    class FontDialogDirector : public DialogDirector {
    public:
        FontDialogDirector();

        ~FontDialogDirector() override;

        void WidgetChanged(Widget *) override;

        void ShowDialog() override;

    protected:
        void CreateWidgets() override;

    private:
        Button *_ok;
        Button *_cancel;
        ListBox *_fontList;
        EntryField *_fontName;
    };

} // namespace Mediator

#endif // DESIGN_PATTERNS__MEDIATOR_PATTERN__FONT_DIALOG_DIRECTOR__HPP
