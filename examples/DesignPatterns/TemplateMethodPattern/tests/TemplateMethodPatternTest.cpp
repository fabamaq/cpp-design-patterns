/******************************************************************************
 * @file TemplateMethodPatternTest.cpp
 *
 * @brief Template Method design pattern
 * - "Define the skeleton of an algorithm in an operation,
 * ... deferring some steps to subclasses.
 * - Template Method lets subclasses redefine certain steps of an algorithm
 * ... without changing the algorithm's structure"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Google Test Framework
#include <gtest/gtest.h>

class View {
public:
    void Display() {
        SetFocus();
        DoDisplay();
        ResetFocus();
    }

protected:
    virtual void DoDisplay() {
        // does nothing
    }

private:
    void SetFocus() {
        puts("View::SetFocus()");
    }

    void ResetFocus() {
        puts("View::SetFocus()");
    }
};

class MyView : public View {
public:
protected:
    void DoDisplay() override {
        puts("MyView::DoDisplay()");
    }
};

// ---------------------------------------------------------------------- Tests
TEST(TemplateMethodPatternTest, DefaultFailingTest) {
    auto v = MyView();
    v.Display();
}
