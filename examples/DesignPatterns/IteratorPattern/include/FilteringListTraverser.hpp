/******************************************************************************
 * @file FilteringListTraverser.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ITERATOR_PATTERN__FILTERING_LIST_TRAVERSER__HPP
#define DESIGN_PATTERNS__ITERATOR_PATTERN__FILTERING_LIST_TRAVERSER__HPP

#include "List.hpp"

template <class Item>
class FilteringListTraverser {
public:
    FilteringListTraverser(List<Item>* aList);
    bool Traverse();

};

#endif // DESIGN_PATTERNS__ITERATOR_PATTERN__FILTERING_LIST_TRAVERSER__HPP
