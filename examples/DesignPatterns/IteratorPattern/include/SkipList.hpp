/******************************************************************************
 * @file SkipList.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ITERATOR_PATTERN__SKIP_LIST__HPP
#define DESIGN_PATTERNS__ITERATOR_PATTERN__SKIP_LIST__HPP

#include "List.hpp"

template<class Item>
class SkipList : public List<Item> {

};

#endif // DESIGN_PATTERNS__ITERATOR_PATTERN__SKIP_LIST__HPP
