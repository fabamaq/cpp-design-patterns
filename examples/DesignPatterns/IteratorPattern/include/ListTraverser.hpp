/******************************************************************************
 * @file ListTraverser.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ITERATOR_PATTERN__LIST_TRAVERSER__HPP
#define DESIGN_PATTERNS__ITERATOR_PATTERN__LIST_TRAVERSER__HPP

#include "List.hpp"
#include "ListIterator.hpp"

template<class Item>
class ListTraverser {
public:
    ListTraverser(List<Item> *aList);

    bool Traverse();

protected:
    virtual bool ProcessItem(const Item &) = 0;

private:
    ListIterator<Item> _iterator;
};

template<class Item>
ListTraverser<Item>::ListTraverser(List<Item> *aList) : _iterator(aList) {}

template<class Item>
bool ListTraverser<Item>::Traverse() {
    bool result = false;

    for (_iterator.First(); !_iterator.IsDone(); _iterator.Next()) {
        result = ProcessItem(_iterator.CurrentItem());

        if (result == false) {
            break;
        }
    }
    return result;
}

#endif // DESIGN_PATTERNS__ITERATOR_PATTERN__LIST_TRAVERSER__HPP
