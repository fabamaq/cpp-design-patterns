/******************************************************************************
 * @file SkipListIterator.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ITERATOR_PATTERN__SKIP_LIST_ITERATOR__HPP
#define DESIGN_PATTERNS__ITERATOR_PATTERN__SKIP_LIST_ITERATOR__HPP

#include "ListIterator.hpp"

template<class Item>
class SkipListIterator : public ListIterator<Item> {
public:
    SkipListIterator(const List<Item> *aList) : ListIterator<Item>(aList) {}
};

#endif // DESIGN_PATTERNS__ITERATOR_PATTERN__SKIP_LIST_ITERATOR__HPP
