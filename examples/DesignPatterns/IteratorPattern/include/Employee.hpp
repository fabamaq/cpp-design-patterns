/******************************************************************************
 * @file Employee.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ITERATOR_PATTERN__EMPLOYEE__HPP
#define DESIGN_PATTERNS__ITERATOR_PATTERN__EMPLOYEE__HPP

#include <string>

class Employee {
    std::string _name;
public:
    Employee(std::string &&name = "Anonymous") : _name(name) {}

    void Print();
};

#endif // DESIGN_PATTERNS__ITERATOR_PATTERN__EMPLOYEE__HPP
