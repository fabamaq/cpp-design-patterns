/******************************************************************************
 * @file PrintNEmployees.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ITERATOR_PATTERN__PRINT_N_EMPLOYEES__HPP
#define DESIGN_PATTERNS__ITERATOR_PATTERN__PRINT_N_EMPLOYEES__HPP

#include "ListTraverser.hpp"
#include "Employee.hpp"

class PrintNEmployees : public ListTraverser<Employee *> {
public:
    PrintNEmployees(List<Employee *> *aList, int n) : ListTraverser<Employee *>(aList), _total(n), _count(0) {}

protected:
    bool ProcessItem(Employee *const &);

private:
    int _total;
    int _count;
};

inline bool PrintNEmployees::ProcessItem(Employee *const &e) {
    _count++;
    e->Print();
    return _count < _total;
}

#endif // DESIGN_PATTERNS__ITERATOR_PATTERN__PRINT_N_EMPLOYEES__HPP
