/******************************************************************************
 * @file IteratorPtr.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ITERATOR_PATTERN__ITERATOR_PTR__HPP
#define DESIGN_PATTERNS__ITERATOR_PATTERN__ITERATOR_PTR__HPP

#include "Iterator.hpp"

template<class Item>
class IteratorPtr {
public:
    IteratorPtr(Iterator<Item> *i) : _i(i) {}

    ~IteratorPtr() { delete _i; }

    Iterator<Item> *operator->() { return _i; }

    Iterator<Item> &operator*() { return *_i; }

private:
    // disallow copy and assignment to avoid multiple deletions of _i:
    IteratorPtr(const IteratorPtr &);

    IteratorPtr &operator=(const IteratorPtr &);

private:
    Iterator<Item> *_i;
};

#endif // DESIGN_PATTERNS__ITERATOR_PATTERN__ITERATOR_PTR__HPP
