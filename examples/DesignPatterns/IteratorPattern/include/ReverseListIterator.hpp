/******************************************************************************
 * @file ReverseReverseListIterator.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ITERATOR_PATTERN__REVERSE_LIST_ITERATOR__HPP
#define DESIGN_PATTERNS__ITERATOR_PATTERN__REVERSE_LIST_ITERATOR__HPP

#include "Iterator.hpp"
#include "List.hpp"
#include <list>

template<class Item>
class ReverseListIterator : public Iterator<Item> {
public:
    explicit ReverseListIterator(const List<Item> *aList) : _list(aList), _current(aList->Count()) {}

    virtual void First() {
        _current = _list->Count()-1;
    }

    virtual void Next() {
        _current--;
    }

    virtual bool IsDone() const {
        return _current < 0;
    }

    virtual Item CurrentItem() const {
        if (IsDone()) {
            throw IteratorOutOfBounds;
        }
        return _list->Get(_current);
    }

private:
    const List<Item> *_list;
    long _current;
};

#endif // DESIGN_PATTERNS__ITERATOR_PATTERN__REVERSE_LIST_ITERATOR__HPP
