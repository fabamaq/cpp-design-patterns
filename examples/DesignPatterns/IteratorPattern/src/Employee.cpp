/******************************************************************************
 * @file Employee.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Employee.hpp"
#include <iostream>

void Employee::Print() {
    std::cout << "Hello " << _name << '\n';
}