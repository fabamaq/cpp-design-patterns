/******************************************************************************
 * @file IteratorPatternTest.cpp
 *
 * @brief Iterator design pattern
 * - "Provide a way to access the elements of an aggregate object sequentially
 * ... without exposing its underlying representation."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Google Test Framework
#include <gtest/gtest.h>

// Project Headers
#include "ReverseListIterator.hpp"
#include "ListIterator.hpp"
#include "Iterator.hpp"
#include "List.hpp"
#include "Employee.hpp"

void PrintEmployees(Iterator<Employee*> &iter) {
    for (iter.First(); !iter.IsDone(); iter.Next()) {
        iter.CurrentItem()->Print();
    }
}

// ---------------------------------------------------------------------- Tests
TEST(IteratorPatternTest, DefaultFailingTest) {
    List<Employee*>* employees = new List<Employee*>();
    employees->Append(new Employee("pedro"));
    employees->Append(new Employee("andre"));
    // ...

    ListIterator<Employee*> forward(employees);
    ReverseListIterator<Employee*> backward(employees);

    PrintEmployees(forward);
    PrintEmployees(backward);

    delete employees;
}

#include "SkipList.hpp"
#include "SkipListIterator.hpp"

TEST(IteratorPatternTest, SkipListTest) {
    SkipList<Employee*>* employees = new SkipList<Employee*>();
    employees->Append(new Employee("pedro"));
    employees->Append(new Employee("andre"));
    // ...

    SkipListIterator<Employee*> iterator(employees);
    PrintEmployees(iterator);

    delete employees;
}

#include "IteratorPtr.hpp"

TEST(IteratorPatternTest, IteratorPtrTest) {
    AbstractList<Employee*>* employees = new List<Employee*>();
    // ...

    IteratorPtr<Employee*> iterator(employees->CreateIterator());
    PrintEmployees(*iterator);

    delete employees;
}

#include "ListTraverser.hpp"
#include "PrintNEmployees.hpp"

TEST(IteratorPatternTest, PrintNEmployeesTest) {
    List<Employee*>* employees = new List<Employee*>();
    employees->Append(new Employee("pedro"));
    employees->Append(new Employee("andre"));
    // ...

    PrintNEmployees pa(employees, 10);
    pa.Traverse();

    delete employees;
}