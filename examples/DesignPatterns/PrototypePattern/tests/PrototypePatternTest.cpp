/******************************************************************************
 * @file PrototypePatternTest.cpp
 *
 * @brief Prototype design pattern
 * - "Specify the kinds of objects to create using a prototypical instance,
 * ... and create new objects by copying this prototype.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Google Test Framework
#include <gtest/gtest.h>

// Project Header Files
#include "MazePrototypeFactory.hpp"
#include "MazeGame.hpp"
#include "BombedWall.hpp"
#include "RoomWithABomb.hpp"
#include "Door.hpp"

// ---------------------------------------------------------------------- Tests
TEST(PrototypePatternTest, DefaultFailingTest) {
    MazeGame game;
    MazePrototypeFactory simpleMazeFactory(
            new Maze, new BombedWall,
            new RoomWithABomb, new Door
    );
}
