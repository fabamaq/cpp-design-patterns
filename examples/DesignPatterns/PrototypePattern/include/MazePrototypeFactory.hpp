#ifndef DESIGN_PATTERNS__MAZE_PROTOTYPE_FACTORY__HPP
#define DESIGN_PATTERNS__MAZE_PROTOTYPE_FACTORY__HPP

#include "MazeFactory.hpp"
#include "Maze.hpp"
#include "Wall.hpp"
#include "Door.hpp"

class MazePrototypeFactory : public MazeFactory {
public:
    MazePrototypeFactory(Maze*, Wall*, Room*, Door*);

    Maze *MakeMaze() const override;
    Wall *MakeWall() const override;
    Room *MakeRoom(int) const override;
    Door *MakeDoor(Room *, Room *) const override;

private:
    Maze* _prototypeMaze;
    Room* _prototypeRoom;
    Wall* _prototypeWall;
    Door* _prototypeDoor;
};

#endif //DESIGN_PATTERNS__MAZE_PROTOTYPE_FACTORY__HPP
