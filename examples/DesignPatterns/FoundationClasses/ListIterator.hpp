#ifndef DESIGN_PATTERNS__LIST_ITERATOR__HPP
#define DESIGN_PATTERNS__LIST_ITERATOR__HPP

#include "Iterator.hpp"
//#include "List.hpp"
#include <list>

template <class Item>
class List;

template<class Item>
class ListIterator : public Iterator<Item> {
public:
    explicit ListIterator(const List<Item> *aList) : _list(aList), _current(0) {}

    virtual void First() {
        _current = 0;
    }

    virtual void Next() {
        _current++;
    }

    virtual bool IsDone() const {
        return _current >= _list->Count();
    }

    virtual Item CurrentItem() const {
        if (IsDone()) {
            throw IteratorOutOfBounds;
        }
        return _list->Get(_current);
    }

private:
    const List<Item> *_list;
    long _current;
};

#endif //DESIGN_PATTERNS__LIST_ITERATOR__HPP
