// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ITERATOR_HPP
#define DESIGN_PATTERNS__ITERATOR_HPP

// TODO
const auto IteratorOutOfBounds = nullptr;

/**
 * An abstract class that defines a traversal interface for aggregates
 */
template<class Item>
class Iterator {
public:
    /// Positions the iterator to the first object in the aggregate
    virtual void First() = 0;

    /// Positions the iterator to the next object in the aggregate
    virtual void Next() = 0;

    /// Returns 'true' when there are no more objects in the sequence
    virtual bool IsDone() const = 0;

    /// Returns the object at the current position in the sequence
    virtual Item CurrentItem() const = 0;

    virtual ~Iterator() = default;

protected:
    Iterator() = default;
};


#endif // DESIGN_PATTERNS__ITERATOR_HPP
