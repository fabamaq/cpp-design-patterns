/******************************************************************************
 * @file AbstractList.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__ITERATOR_PATTERN__ABSTRACT_LIST__HPP
#define DESIGN_PATTERNS__ITERATOR_PATTERN__ABSTRACT_LIST__HPP

#include "Iterator.hpp"

template <class Item>
class AbstractList {
public:
    virtual Iterator<Item>* CreateIterator() const = 0;
    // ...
};

#endif // DESIGN_PATTERNS__ITERATOR_PATTERN__ABSTRACT_LIST__HPP
