#ifndef DESIGN_PATTERNS__FOUNDATION_CLASSES__POINT__HPP
#define DESIGN_PATTERNS__FOUNDATION_CLASSES__POINT__HPP

#include <ostream>
#include <istream>

typedef float Coord;

class Point {
public:
    static const Point Zero;

    explicit Point(Coord x = 0.0, Coord y = 0.0) noexcept: _x(x), _y(y) {}

    Coord X() const { return _x; }

    void X(Coord x) { _x = x; }

    Coord Y() const { return _y; }

    void Y(Coord y) { _y = y; }

    friend Point operator+(const Point &lhs, const Point &rhs) {
        return Point(lhs._x + rhs._x, lhs._y + rhs._y);
    }

    friend Point operator-(const Point &lhs, const Point &rhs) {
        return Point(lhs._x - rhs._x, lhs._y - rhs._y);
    }

    friend Point operator*(const Point &lhs, const Point &rhs) {
        return Point(lhs._x * rhs._x, lhs._y * rhs._y);
    }

    friend Point operator/(const Point &lhs, const Point &rhs) {
        return Point(lhs._x / rhs._x, lhs._y / rhs._y);
    }

    Point &operator+=(const Point &other) {
        this->_x += other._x;
        this->_y += other._y;
        return *this;
    }

    Point &operator-=(const Point &other) {
        this->_x -= other._x;
        this->_y -= other._y;
        return *this;
    }

    Point &operator*=(const Point &other) {
        this->_x *= other._x;
        this->_y *= other._y;
        return *this;
    }

    Point &operator/=(const Point &other) {
        this->_x /= other._x;
        this->_y /= other._y;
        return *this;
    }

    Point operator-() {
        return Point(-this->_x, -this->_y);
    }

    friend bool operator==(const Point &lhs, const Point &rhs) {
        return (lhs._x == rhs._x) && (lhs._y == rhs._y);
    }

    friend bool operator!=(const Point &lhs, const Point &rhs) {
        return (lhs._x != rhs._x) || (lhs._y != rhs._y);
    }

    friend std::ostream &operator<<(std::ostream &os, const Point &point) {
        os << point._x << "," << point._y;
        return os;
    }

    friend std::istream &operator>>(std::istream &is, Point &point) {
        is >> point._x;
        is >> point._y;
        return is;
    }

private:
    Coord _x;
    Coord _y;
};

#endif //DESIGN_PATTERNS__FOUNDATION_CLASSES__POINT__HPP
