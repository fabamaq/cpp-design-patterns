#ifndef DESIGN_PATTERNS__RECT__HPP
#define DESIGN_PATTERNS__RECT__HPP

#include "Coord.hpp"

// clang-format off
// @formatter:off

class Rect {
public:
    static const Rect Zero;

    Rect(Coord x, Coord y, Coord w, Coord h);
    Rect(const Point &origin, const Point &extent);

    // clang-format off
    // @formatter:off
    Coord Width() const;    void Width(Coord);
    Coord Height() const;   void Height(Coord);
    Coord Left() const;     void Left(Coord);
    Coord Right() const;    void Right(Coord);

    Point &Origin() const;  void Origin(const Point &);
    Point &Extent() const;  void Extent(const Point &);

    void MoveTo(const Point &);
    void MoveBy(const Point &);

    bool IsEmpty() const;
    bool Contains(const Point &) const;
};

// @formatter:on
// clang-format on

#endif //DESIGN_PATTERNS__RECT__HPP
