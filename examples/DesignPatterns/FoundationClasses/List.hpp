#ifndef DESIGN_PATTERNS__LIST__HPP
#define DESIGN_PATTERNS__LIST__HPP

#include "AbstractList.hpp"
#include "Iterator.hpp"
#include "ListIterator.hpp"

#include <algorithm>
#include <cassert>
#include <deque>

const long DEFAULT_LIST_CAPACITY = 1;

template<class Item>
class List : public AbstractList<Item> {
public:
    // Implements AbstractList
    Iterator<Item> *CreateIterator() const {
        return new ListIterator<Item>(this);
    }

    // Construction, Destruction, Initialization and Assignment
    /// initializes the list. The `size` parameter is a hint for the initial number of elements
    explicit List(long size = DEFAULT_LIST_CAPACITY) : _list(size) {
        _list.clear();
    }

    /// overrides the default copy constructor so that member data are initialized properly
    List(List &other) {
        _list = other._list;
    }

    /** free the list's internal data structures but not the elements in the list.
     The class is not designed for subclassing; therefore the destructor isn't virtual */
    ~List() = default;

    /// implements the assignment operation to assign member data properly
    List &operator=(const List &other) {
        _list = other._list;
        return &this;
    }

    // Accessing

    /// returns the number of objects in the list
    long Count() const {
        return _list.size();
    }

    /// returns the object at the given index
    const Item &Get(long index) const {
        return _list.at(index);
    }

    /// returns the first object in the list
    Item &First() const {
        return _list.front();
    }

    /// returns the last object in the list
    Item &Last() const {
        return _list.back();
    }

    /// returns `true` if the list contains the given element. Otherwise, returns `false`
    bool Includes(const Item &item) const {
        return std::find(_list.cbegin(), _list.cend(), item) != _list.end();
    }

    // Adding

    /// adds the argument to the list, making it the last element
    void Append(const Item &item) {
        _list.push_back(item);
    }

    /// adds the argument to the list, making it the first element
    void Prepend(const Item &item) {
        _list.push_front(item);
    }

    // Removing

    /**
     * removes the given element from the list. This operation requires that the type of
     * elements in the list supports the `==` operator for comparison
     */
    void Remove(const Item &item) {
        // Erase–remove idiom -  proper way to remove elements from a container
        _list.erase(std::remove(_list.begin(), _list.end(), item), _list.end());
    }

    /// removes the last element from the list
    void RemoveLast() {
        _list.pop_back();
    }

    /// removes the first element from the list
    void RemoveFirst() {
        _list.pop_front();
    }

    /// removes all elements from the list
    void RemoveAll() {
        _list.clear();
    }

    // Stack Interface

    /// returns the top element (when the List is viewed as a stack)
    Item &Top() const {
        return _list.back();
    }

    /// pushes the element onto the stack
    void Push(const Item &item) {
        _list.push_back(item);
    }

    /// pops the top element from the stack
    Item &Pop() {
        assert(!_list.empty());
        // assign copy of last element
        auto copy = _list.back();
        _list.pop_back();
        return copy;
    }

private:
    std::deque<Item> _list{};
};

#endif //DESIGN_PATTERNS__LIST__HPP
