/******************************************************************************
 * @file Chassis.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Chassis.hpp"
#include "Currency.hpp"
#include "EquipmentVisitor.hpp"
#include "ListIterator.hpp"


Chassis::Chassis() : Equipment("Chassis") {}

void Chassis::Accept(EquipmentVisitor &visitor) {
    for (
            ListIterator<Equipment *> i(_parts);
            !i.IsDone();
            i.Next()
            ) {
        i.CurrentItem()->Accept(visitor);
    }
    visitor.VisitChassis(this);
}

Currency Chassis::NetPrice() {
    return Currency(0);
}