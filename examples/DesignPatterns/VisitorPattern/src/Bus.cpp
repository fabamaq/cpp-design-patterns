/******************************************************************************
 * @file Bus.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Bus.hpp"
#include "Currency.hpp"

Bus::Bus() : Equipment("Bus") {}

Currency Bus::NetPrice() {
    return Currency(0);
}
