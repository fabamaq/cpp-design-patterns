/******************************************************************************
 * @file Card.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Card.hpp"
#include "Currency.hpp"

Card::Card() : Equipment("Card") {}

Currency Card::NetPrice() {
    return Currency(0);
}
