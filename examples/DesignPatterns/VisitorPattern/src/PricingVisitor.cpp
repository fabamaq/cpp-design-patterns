/******************************************************************************
 * @file PricingVisitor.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "PricingVisitor.hpp"

#include "FloppyDisk.hpp"
#include "Card.hpp"
#include "Chassis.hpp"
#include "Bus.hpp"

PricingVisitor::PricingVisitor() : _total(0) {}

Currency &PricingVisitor::GetTotalPrice() {
    return _total;
}

void PricingVisitor::VisitFloppyDisk(FloppyDisk *e) {
    _total += e->NetPrice();
}

void PricingVisitor::VisitCard(Card *e) {
    _total += e->NetPrice();
}

void PricingVisitor::VisitChassis(Chassis *e) {
    _total += e->NetPrice();
}

void PricingVisitor::VisitBus(Bus *e) {
    _total += e->NetPrice();
}
