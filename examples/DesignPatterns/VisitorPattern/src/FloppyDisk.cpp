/******************************************************************************
 * @file FloppyDisk.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "FloppyDisk.hpp"

#include "Currency.hpp"
#include "EquipmentVisitor.hpp"

FloppyDisk::FloppyDisk() : Equipment("FloppyDisk") {};

void FloppyDisk::Accept(EquipmentVisitor &visitor) {
    visitor.VisitFloppyDisk(this);
}

Currency FloppyDisk::NetPrice() {
    return Currency(0);
}
