/******************************************************************************
 * @file Currency.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Currency.hpp"

std::ostream &operator<<(std::ostream &os, const Currency &c) {
    os << c._value;
    return os;
}