/******************************************************************************
 * @file Equipment.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Equipment.hpp"
#include "Currency.hpp"
#include "Watt.hpp"

Equipment::Equipment(const char *name) : _name(name) {}

Equipment::~Equipment() = default;

const char *Equipment::Name() {
    return _name;
}

Watt Equipment::Power() {
    return {};
}

Currency Equipment::NetPrice() {
    return Currency(0);
}

Currency Equipment::DiscountPrice() {
    return Currency(0);
}

void Equipment::Accept(EquipmentVisitor &) {

}
