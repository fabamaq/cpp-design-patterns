/******************************************************************************
 * @file EquipmentVisitor.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "EquipmentVisitor.hpp"

EquipmentVisitor::~EquipmentVisitor() = default;

EquipmentVisitor::EquipmentVisitor() = default;

void EquipmentVisitor::VisitFloppyDisk(FloppyDisk *) {

}

void EquipmentVisitor::VisitCard(Card *) {

}

void EquipmentVisitor::VisitChassis(Chassis *) {

}

void EquipmentVisitor::VisitBus(Bus *) {

}
