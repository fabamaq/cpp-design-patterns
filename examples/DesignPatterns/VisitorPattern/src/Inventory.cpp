/******************************************************************************
 * @file Inventory.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Inventory.hpp"

#include "Equipment.hpp"

void Inventory::Accumulate(Equipment *e) {
    // TODO:
}

std::ostream &operator<<(std::ostream &os, const Inventory &i) {
    os << i.get_value();
    return os;
}