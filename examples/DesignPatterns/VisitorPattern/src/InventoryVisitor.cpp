/******************************************************************************
 * @file InventoryVisitor.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "InventoryVisitor.hpp"

#include "FloppyDisk.hpp"
#include "Card.hpp"
#include "Chassis.hpp"
#include "Bus.hpp"

InventoryVisitor::InventoryVisitor() = default;

Inventory &InventoryVisitor::GetInventory() {
    return _inventory;
}

void InventoryVisitor::VisitFloppyDisk(FloppyDisk *e) {
    _inventory.Accumulate(e);
}

void InventoryVisitor::VisitCard(Card *e) {
    _inventory.Accumulate(e);
}

void InventoryVisitor::VisitChassis(Chassis *e) {
    _inventory.Accumulate(e);
}

void InventoryVisitor::VisitBus(Bus *e) {
    _inventory.Accumulate(e);
}
