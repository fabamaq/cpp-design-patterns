/******************************************************************************
 * @file FloppyDisk.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__VISITOR_PATTERN__FLOPPY_DISK__HPP
#define DESIGN_PATTERNS__VISITOR_PATTERN__FLOPPY_DISK__HPP

#include "Equipment.hpp"

class EquipmentVisitor;

class Currency;

class FloppyDisk : public Equipment {
public:
    FloppyDisk();

    void Accept(EquipmentVisitor &visitor);

    Currency NetPrice();
};

#endif // DESIGN_PATTERNS__VISITOR_PATTERN__FLOPPY_DISK__HPP
