/******************************************************************************
 * @file Chassis.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__VISITOR_PATTERN__CHASSIS__HPP
#define DESIGN_PATTERNS__VISITOR_PATTERN__CHASSIS__HPP

#include "List.hpp"
#include "Equipment.hpp"

class Currency;

class EquipmentVisitor;

class Chassis : public Equipment {

private:
    List<Equipment *> *_parts;
public:
    Chassis();

    void Accept(EquipmentVisitor &visitor);

    Currency NetPrice();
};


#endif // DESIGN_PATTERNS__VISITOR_PATTERN__CHASSIS__HPP
