/******************************************************************************
 * @file PricingVisitor.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__VISITOR_PATTERN__PRICING_VISITOR__HPP
#define DESIGN_PATTERNS__VISITOR_PATTERN__PRICING_VISITOR__HPP

#include "EquipmentVisitor.hpp"

#include "Currency.hpp"

class FloppyDisk;

class Card;

class Chassis;

class Bus;

class PricingVisitor : public EquipmentVisitor {
public:
    PricingVisitor();

    Currency &GetTotalPrice();

    virtual void VisitFloppyDisk(FloppyDisk *);

    virtual void VisitCard(Card *);

    virtual void VisitChassis(Chassis *);

    virtual void VisitBus(Bus *);
    // ...

private:
    Currency _total;
};


#endif // DESIGN_PATTERNS__VISITOR_PATTERN__PRICING_VISITOR__HPP
