/******************************************************************************
 * @file Watt.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__VISITOR_PATTERN__WATT__HPP
#define DESIGN_PATTERNS__VISITOR_PATTERN__WATT__HPP

class Watt {
public:
    Watt() = default;
};

#endif // DESIGN_PATTERNS__VISITOR_PATTERN__WATT__HPP
