/******************************************************************************
 * @file Bus.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__VISITOR_PATTERN__BUS__HPP
#define DESIGN_PATTERNS__VISITOR_PATTERN__BUS__HPP

#include "Equipment.hpp"

class Currency;

class Bus : public Equipment {
public:
    Bus();

    Currency NetPrice();
};


#endif // DESIGN_PATTERNS__VISITOR_PATTERN__BUS__HPP
