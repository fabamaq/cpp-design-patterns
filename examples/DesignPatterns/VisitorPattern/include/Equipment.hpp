/******************************************************************************
 * @file Equipment.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__VISITOR_PATTERN__EQUIPMENT__HPP
#define DESIGN_PATTERNS__VISITOR_PATTERN__EQUIPMENT__HPP

class Watt;

class Currency;

class EquipmentVisitor;

class Equipment {
public:
    virtual ~Equipment();

    const char *Name();

    virtual Watt Power();

    virtual Currency NetPrice();

    virtual Currency DiscountPrice();

    virtual void Accept(EquipmentVisitor &);

protected:
    explicit Equipment(const char *);

private:
    const char *_name;
};


#endif // DESIGN_PATTERNS__VISITOR_PATTERN__EQUIPMENT__HPP
