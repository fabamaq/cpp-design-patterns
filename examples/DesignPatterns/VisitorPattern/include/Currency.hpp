/******************************************************************************
 * @file Currency.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__VISITOR_PATTERN__CURRENCY__HPP
#define DESIGN_PATTERNS__VISITOR_PATTERN__CURRENCY__HPP

#include <ostream>

class Currency {
public:
    explicit Currency(int value) : _value(value) {}

    Currency &operator+=(const Currency &other) {
        _value += other._value;
        return *this;
    }

    friend std::ostream &operator<<(std::ostream &os, const Currency &c);

private:
    int _value;
};



#endif // DESIGN_PATTERNS__VISITOR_PATTERN__CURRENCY__HPP
