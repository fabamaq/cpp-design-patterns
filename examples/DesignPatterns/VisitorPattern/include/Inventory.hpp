/******************************************************************************
 * @file Inventory.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__VISITOR_PATTERN__INVENTORY__HPP
#define DESIGN_PATTERNS__VISITOR_PATTERN__INVENTORY__HPP

#include <ostream>

class Equipment;

class Inventory {
    int _value{0};
public:
    void Accumulate(Equipment *);

    int get_value() const { return _value; }


    friend std::ostream &operator<<(std::ostream &os, const Inventory &c);
};

#endif // DESIGN_PATTERNS__VISITOR_PATTERN__INVENTORY__HPP
