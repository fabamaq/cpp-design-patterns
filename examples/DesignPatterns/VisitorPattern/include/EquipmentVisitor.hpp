/******************************************************************************
 * @file EquipmentVisitor.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__VISITOR_PATTERN__EQUIPMENT_VISITOR__HPP
#define DESIGN_PATTERNS__VISITOR_PATTERN__EQUIPMENT_VISITOR__HPP

class FloppyDisk;

class Card;

class Chassis;

class Bus;

class EquipmentVisitor {
public:
    ~EquipmentVisitor();

    virtual void VisitFloppyDisk(FloppyDisk *);

    virtual void VisitCard(Card *);

    virtual void VisitChassis(Chassis *);

    virtual void VisitBus(Bus *);

    // and so on for the other concrete subclasses of Equipment

protected:
    EquipmentVisitor();
};


#endif // DESIGN_PATTERNS__VISITOR_PATTERN__EQUIPMENT_VISITOR__HPP
