/******************************************************************************
 * @file InventoryVisitor.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__VISITOR_PATTERN__INVENTORY_VISITOR__HPP
#define DESIGN_PATTERNS__VISITOR_PATTERN__INVENTORY_VISITOR__HPP

#include "EquipmentVisitor.hpp"
#include "Equipment.hpp"
#include "Inventory.hpp"

class InventoryVisitor : public EquipmentVisitor {
public:
    InventoryVisitor();

    Inventory &GetInventory();

    void VisitFloppyDisk(FloppyDisk *) override;

    void VisitCard(Card *) override;

    void VisitChassis(Chassis *) override;

    void VisitBus(Bus *) override;

private:
    Inventory _inventory;
};


#endif // DESIGN_PATTERNS__VISITOR_PATTERN__INVENTORY_VISITOR__HPP
