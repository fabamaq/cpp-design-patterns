/******************************************************************************
 * @file Card.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__VISITOR_PATTERN__CARD__HPP
#define DESIGN_PATTERNS__VISITOR_PATTERN__CARD__HPP

#include "Equipment.hpp"

class Currency;

class Card : public Equipment {
public:
    Card();

    Currency NetPrice();
};


#endif // DESIGN_PATTERNS__VISITOR_PATTERN__CARD__HPP
