/**************************************************************************************************
 * @file VisitorPatternTest.cpp
 *
 * @brief Visitor design pattern
 * - "Represent an operation to be performed on the elements of an object structure.
 * Visitor lets you defined a new operation without changing the
 * ... classes of the elements on which it operates"
 *************************************************************************************************/
// ------------------------------------------------------------------- Includes
// Google Test Framework
#include <gtest/gtest.h>

// C++ Standard Library
#include <iostream>

// Project Header Files
#include "Equipment.hpp"
#include "InventoryVisitor.hpp"
#include "Bus.hpp"

// ---------------------------------------------------------------------- Tests
TEST(VisitorPatternTest, DefaultFailingTest) {
    Equipment *component = new Bus();
    InventoryVisitor visitor;

    component->Accept(visitor);
    std::cout << "Inventory "
              << component->Name()
              << visitor.GetInventory();
}
