/******************************************************************************
 * @file ConstraintSolverMemento.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__MEMENTO_PATTERN__CONSTRAINT_SOLVER_MEMENTO__HPP
#define DESIGN_PATTERNS__MEMENTO_PATTERN__CONSTRAINT_SOLVER_MEMENTO__HPP

class ConstraintSolverMemento {
public:
    virtual ~ConstraintSolverMemento();

private:
    friend class ConstraintSolver;

    ConstraintSolverMemento();

    // private constraint solver state
};

#endif // DESIGN_PATTERNS__MEMENTO_PATTERN__CONSTRAINT_SOLVER_MEMENTO__HPP
