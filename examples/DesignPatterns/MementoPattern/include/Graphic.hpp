/******************************************************************************
 * @file Graphic.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__MEMENTO_PATTERN__GRAPHIC__HPP
#define DESIGN_PATTERNS__MEMENTO_PATTERN__GRAPHIC__HPP

#include "Point.hpp"

class Graphic {
public:
    void Move(Point);
};

#endif // DESIGN_PATTERNS__MEMENTO_PATTERN__GRAPHIC__HPP
