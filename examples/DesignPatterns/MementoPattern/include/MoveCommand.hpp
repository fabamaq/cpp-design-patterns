/******************************************************************************
 * @file MoveCommand.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__MEMENTO_PATTERN__MOVE_COMMAND__HPP
#define DESIGN_PATTERNS__MEMENTO_PATTERN__MOVE_COMMAND__HPP

#include "Graphic.hpp"
#include "Point.hpp"
#include "ConstraintSolverMemento.hpp"

class MoveCommand {
public:
    MoveCommand(Graphic *target, const Point &delta);

    void Execute();

    void Unexecute();

private:
    ConstraintSolverMemento *_state;
    Point _delta;
    Graphic *_target;
};


#endif // DESIGN_PATTERNS__MEMENTO_PATTERN__MOVE_COMMAND__HPP
