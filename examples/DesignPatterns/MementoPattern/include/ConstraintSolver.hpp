/******************************************************************************
 * @file ConstraintSolver.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__MEMENTO_PATTERN__CONSTRAINT_SOLVER__HPP
#define DESIGN_PATTERNS__MEMENTO_PATTERN__CONSTRAINT_SOLVER__HPP

#include "Graphic.hpp"
#include "ConstraintSolverMemento.hpp"

class ConstraintSolver {
public:
    static ConstraintSolver *Instance();

    void Solve();

    void AddConstraint(Graphic *startConnection, Graphic *endConnection);

    void RemoveConstraint(Graphic *startConnection, Graphic *endConnection);

    ConstraintSolverMemento *CreateMemento();

    void SetMemento(ConstraintSolverMemento *);

private:
    // nontrivial state and operations for enforcing
    // connectivity semantics

    static ConstraintSolver* _instance;
};



#endif // DESIGN_PATTERNS__MEMENTO_PATTERN__CONSTRAINT_SOLVER__HPP
