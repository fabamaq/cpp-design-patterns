/******************************************************************************
 * @file MementoPatternTest.cpp
 *
 * @brief Memento design pattern
 * - "Without violating encapsulation, capture and externalize an object's
 * ... internal state so that the object can be restored to this state later."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Google Test Framework
#include <gtest/gtest.h>

// ---------------------------------------------------------------------- Tests
TEST(MementoPatternTest, DefaultFailingTest) {
	FAIL() << "Unimplemented Test";
}
