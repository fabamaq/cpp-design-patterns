/******************************************************************************
 * @file MoveCommand.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "MoveCommand.hpp"
#include "ConstraintSolver.hpp"

MoveCommand::MoveCommand(Graphic *target, const Point &delta)
        : _target(target), _delta(delta) {}

void MoveCommand::Execute() {
    ConstraintSolver *solver = ConstraintSolver::Instance();
    _state = solver->CreateMemento(); // create a memento
    _target->Move(_delta);
    solver->Solve();
}

void MoveCommand::Unexecute() {
    ConstraintSolver *solver = ConstraintSolver::Instance();
    _target->Move(-_delta);
    solver->SetMemento(_state); // restore solver state
    solver->Solve();
}
