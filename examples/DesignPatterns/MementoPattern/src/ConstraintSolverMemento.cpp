/******************************************************************************
 * @file ConstraintSolverMemento.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "ConstraintSolverMemento.hpp"

ConstraintSolverMemento::ConstraintSolverMemento() = default;

ConstraintSolverMemento::~ConstraintSolverMemento() = default;
