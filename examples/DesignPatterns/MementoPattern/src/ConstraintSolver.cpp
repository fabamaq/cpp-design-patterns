/******************************************************************************
 * @file ConstraintSolver.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "ConstraintSolver.hpp"

ConstraintSolver *ConstraintSolver::_instance = nullptr;

ConstraintSolver *ConstraintSolver::Instance() {
    if (_instance == nullptr) {
        _instance = new ConstraintSolver();
    }
    return _instance;
}

void ConstraintSolver::Solve() {

}

void ConstraintSolver::AddConstraint(Graphic *startConnection, Graphic *endConnection) {

}

void ConstraintSolver::RemoveConstraint(Graphic *startConnection, Graphic *endConnection) {

}

ConstraintSolverMemento *ConstraintSolver::CreateMemento() {
    return nullptr;
}

void ConstraintSolver::SetMemento(ConstraintSolverMemento *) {

}
