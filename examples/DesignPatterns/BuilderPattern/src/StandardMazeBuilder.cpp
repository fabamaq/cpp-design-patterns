#include <StandardMazeBuilder.hpp>

#include <Wall.hpp>
#include <Door.hpp>

StandardMazeBuilder::StandardMazeBuilder() {
    _currentMaze = nullptr;
}

void StandardMazeBuilder::BuildMaze() {
    _currentMaze = new Maze();
}

Maze *StandardMazeBuilder::GetMaze() {
    return _currentMaze;
}

void StandardMazeBuilder::BuildRoom(int n) {
    if (!_currentMaze->RoomNo(n)) {
        Room *room = new Room(n);
        _currentMaze->AddRoom(room);

        room->SetSide(Direction::North, new Wall);
        room->SetSide(Direction::South, new Wall);
        room->SetSide(Direction::East, new Wall);
        room->SetSide(Direction::West, new Wall);
    }
}

void StandardMazeBuilder::BuildDoor(int n1, int n2) {
    Room *r1 = _currentMaze->RoomNo(n1);
    Room *r2 = _currentMaze->RoomNo(n2);
    Door *d = new Door(r1, r2);

    r1->SetSide(CommonWall(r1, r2), d);
    r2->SetSide(CommonWall(r2, r1), d);
}

Direction StandardMazeBuilder::CommonWall(Room *r1, Room *r2) {
    return Direction::West;
}
