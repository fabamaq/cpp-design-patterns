# =============================================================================
# General Settings
# -----------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.0)
project(BuilderPattern)

# =============================================================================
# Compiler settings
# -----------------------------------------------------------------------------
set(CMAKE_CXX_STANDARD 17)

# =============================================================================
# Workspace settings
# -----------------------------------------------------------------------------
set(BuilderPatternSourcesFolder ${BuilderPatternProjectFolder}/src)
set(BuilderPatternIncludeFolder ${BuilderPatternProjectFolder}/include)
set(BuilderPatternTestsFolder ${BuilderPatternProjectFolder}/tests)

# =============================================================================
# Scaffolding System
# -----------------------------------------------------------------------------
include(${ScaffoldingFolder}/GenerateClass.cmake)
# ! uncomment AFTER changing the ClassPath and ClassName
#GenerateClass("Inventory" BuilderPattern)

# =============================================================================
# ReconfigureCMakeListsFile
# - Comments call to "GenerateClass"
# -----------------------------------------------------------------------------
set(CMakeFile ${BuilderPatternProjectFolder}/CMakeLists.txt)
file(READ ${CMakeFile} CMakeFileContents)
string(REGEX REPLACE "(.*)[^#]GenerateClass(\\(\".*)" "\\1\n#GenerateClass\\2" result ${CMakeFileContents})
file(WRITE ${CMakeFile} ${result})

# =============================================================================
# Targets settings
# -----------------------------------------------------------------------------
# Target Executable
add_executable(BuilderPatternTest ${BuilderPatternTestsFolder}/BuilderPatternTest.cpp)
target_include_directories(BuilderPatternTest PUBLIC
        ${BuilderPatternIncludeFolder}
        ${MazeGameProjectFolder}
        )
# since we need to reload cmake to generate a new class, this works fine
file(GLOB files "${BuilderPatternSourcesFolder}/*.cpp")
set(SOURCES "")
foreach (file ${files})
    set(SOURCES ${SOURCES} ${file})
endforeach ()
file(GLOB files "${MazeGameProjectFolder}/*.cpp")
foreach (file ${files})
    set(SOURCES ${SOURCES} ${file})
endforeach ()
target_sources(BuilderPatternTest PUBLIC ${SOURCES})
target_link_libraries(BuilderPatternTest PUBLIC gtest gtest_main)
target_compile_definitions(BuilderPatternTest PUBLIC BUILDER_PATTERN)