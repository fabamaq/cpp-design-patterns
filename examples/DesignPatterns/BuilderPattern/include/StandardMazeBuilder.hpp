#ifndef DESIGN_PATTERNS__STANDARD_MAZE_BUILDER__HPP
#define DESIGN_PATTERNS__STANDARD_MAZE_BUILDER__HPP

#include <MazeBuilder.hpp>
#include <Maze.hpp>
#include <Room.hpp>
#include <Direction.hpp>

class StandardMazeBuilder : public MazeBuilder {
public:
    StandardMazeBuilder();

    virtual void BuildMaze();
    virtual void BuildRoom(int);
    virtual void BuildDoor(int, int);

    virtual Maze* GetMaze();

private:
    Direction CommonWall(Room*, Room*);
    Maze* _currentMaze;
};


#endif //DESIGN_PATTERNS__STANDARD_MAZE_BUILDER__HPP
