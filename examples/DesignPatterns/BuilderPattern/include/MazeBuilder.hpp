#ifndef DESIGN_PATTERNS__MAZE_BUILDER__HPP
#define DESIGN_PATTERNS__MAZE_BUILDER__HPP

#include <Maze.hpp>

class MazeBuilder {
public:
    virtual void BuildMaze() {}
    virtual void BuildRoom(int room) {}
    virtual void BuildDoor(int roomFrom, int roomTo) {}

    virtual Maze* GetMaze() { return nullptr; }

protected:
    MazeBuilder() = default;
};

#endif //DESIGN_PATTERNS__MAZE_BUILDER__HPP
