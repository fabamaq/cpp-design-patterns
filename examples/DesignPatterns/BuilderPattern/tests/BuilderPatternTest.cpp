/******************************************************************************
 * @file BuilderPatternTest.cpp
 *
 * @brief Builder design pattern
 * - "Separate the construction of a complex object from its representation
 * ... so the same construction process can create different representations"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes

// Google Test Framework
#include <gtest/gtest.h>

#include <Maze.hpp>
#include <MazeGame.hpp>
#include <StandardMazeBuilder.hpp>

// ---------------------------------------------------------------------- Tests
TEST(BuilderPatternTest, StandardMazeBuilder) {
    Maze *maze;
    MazeGame game;
    StandardMazeBuilder builder;

    game.CreateMaze(builder);
    maze = builder.GetMaze();

    ASSERT_NE(maze, nullptr);
}
