/******************************************************************************
 * @file CompositePatternTest.cpp
 *
 * @brief Composite design pattern
 * - "Compose objects into tree structures to represent part-whole hierarchies.
 * ... Composite lets clients treat individual objects and compositions of
 * ... objects uniformly"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Equipment.hpp"
#include "CompositeEquipment.hpp"
#include "FloppyDisk.hpp"

#include "Chassis.hpp"
#include "Cabinet.hpp"
#include "Card.hpp"
#include "Bus.hpp"

#include <iostream>

// Google Test Framework
#include <gtest/gtest.h>

using namespace Composite;

// ---------------------------------------------------------------------- Tests
TEST(CompositePatternTest, DefaultFailingTest) {
    auto *cabinet = new Cabinet("PC Cabinet");
    auto *chassis = new Chassis("PC Chassis");

    cabinet->Add(chassis);

    Bus *bus = new Bus("MCA Bus");
    bus->Add(new Card("16Mbs Token Ring"));

    chassis->Add(bus);
    chassis->Add(new FloppyDisk("3.5in Floppy"));

    std::cout << "The net price is " << chassis->NetPrice() << std::endl;
}
