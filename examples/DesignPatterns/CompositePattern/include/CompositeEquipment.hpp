#ifndef DESIGN_PATTERNS__COMPOSITE_PATTERN__COMPOSITE_EQUIPMENT__HPP
#define DESIGN_PATTERNS__COMPOSITE_PATTERN__COMPOSITE_EQUIPMENT__HPP

#include "Equipment.hpp"
#include "List.hpp"

namespace Composite {

    class CompositeEquipment : public Equipment {
    public:
        ~CompositeEquipment() override;

        Watt Power() override;

        Currency NetPrice() override;

        Currency DiscountPrice() override;

        void Add(Equipment *) override;

        void Remove(Equipment *) override;

        Iterator<Equipment *> *CreateIterator() override;

    protected:
        explicit CompositeEquipment(const char *);

    private:
        List<Equipment *> _equipment;
    };

} // namespace Composite

#endif //DESIGN_PATTERNS__COMPOSITE_PATTERN__COMPOSITE_EQUIPMENT__HPP
