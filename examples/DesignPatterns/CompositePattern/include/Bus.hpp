#ifndef DESIGN_PATTERNS__COMPOSITE_PATTERN__BUS__HPP
#define DESIGN_PATTERNS__COMPOSITE_PATTERN__BUS__HPP

namespace Composite {

    class Bus : public CompositeEquipment {
    public:
        explicit Bus(const char *name) : CompositeEquipment(name) {}

        ~Bus() override = default;

        Watt Power() override {
            return CompositeEquipment::Power();
        }

        Currency NetPrice() override {
            return CompositeEquipment::NetPrice();
        }

        Currency DiscountPrice() override {
            return CompositeEquipment::DiscountPrice();
        }
    };

} // namespace Composite

#endif //DESIGN_PATTERNS__COMPOSITE_PATTERN__BUS__HPP
