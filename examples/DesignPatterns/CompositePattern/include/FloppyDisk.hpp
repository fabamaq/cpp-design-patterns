#ifndef DESIGN_PATTERNS__COMPOSITE_PATTERN__FLOPPY_DISK__HPP
#define DESIGN_PATTERNS__COMPOSITE_PATTERN__FLOPPY_DISK__HPP

#include "Equipment.hpp"
#include "Watt.hpp"
#include "Currency.hpp"

namespace Composite {

    class FloppyDisk : public Equipment {
    public:
        explicit FloppyDisk(const char *name) : Equipment(name) {}

        ~FloppyDisk() override = default;

        Watt Power() override {
            return Equipment::Power();
        }

        Currency NetPrice() override {
            return Equipment::NetPrice();
        }

        Currency DiscountPrice() override {
            return Equipment::DiscountPrice();
        }
    };

} // namespace Composite

#endif //DESIGN_PATTERNS__COMPOSITE_PATTERN__FLOPPY_DISK__HPP
