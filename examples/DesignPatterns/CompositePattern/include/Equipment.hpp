#ifndef DESIGN_PATTERNS__COMPOSITE_PATTERN__EQUIPMENT__HPP
#define DESIGN_PATTERNS__COMPOSITE_PATTERN__EQUIPMENT__HPP

#include "Watt.hpp"
#include "Currency.hpp"
#include "Iterator.hpp"

namespace Composite {

    class Equipment {
    public:
        virtual ~Equipment();

        const char *Name();

        virtual Watt Power();

        virtual Currency NetPrice();

        virtual Currency DiscountPrice();

        virtual void Add(Equipment *);

        virtual void Remove(Equipment *);

        virtual Iterator<Equipment *> *CreateIterator();

    protected:
        explicit Equipment(const char *);

    private:
        const char *_name;
    };

} // namespace Composite

#endif //DESIGN_PATTERNS__COMPOSITE_PATTERN__EQUIPMENT__HPP
