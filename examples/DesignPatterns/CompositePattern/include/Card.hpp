#ifndef DESIGN_PATTERNS__COMPOSITE_PATTERN__CARD__HPP
#define DESIGN_PATTERNS__COMPOSITE_PATTERN__CARD__HPP

#include "CompositeEquipment.hpp"

namespace Composite {

    class Card : public CompositeEquipment {
    public:
        explicit Card(const char *name) : CompositeEquipment(name) {}

        ~Card() override = default;

        Watt Power() override {
            return CompositeEquipment::Power();
        }

        Currency NetPrice() override {
            return CompositeEquipment::NetPrice();
        }

        Currency DiscountPrice() override {
            return CompositeEquipment::DiscountPrice();
        }
    };

} // namespace Composite

#endif //DESIGN_PATTERNS__COMPOSITE_PATTERN__CARD__HPP
