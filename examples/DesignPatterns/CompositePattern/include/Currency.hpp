#ifndef DESIGN_PATTERNS__COMPOSITE_PATTERN__CURRENCY__HPP
#define DESIGN_PATTERNS__COMPOSITE_PATTERN__CURRENCY__HPP

#include <ostream>

namespace Composite {

    class Currency {
    public:
        Currency() : _value(0) {}

        explicit Currency(int value) : _value(value) {}

        Currency &operator+=(const Currency &other) {
            _value += other._value;
            return *this;
        }

        friend std::ostream &operator<<(std::ostream &os, const Currency &c);


    private:
        int _value;
    };

    inline std::ostream &operator<<(std::ostream &os, const Currency &c) {
        os << c._value;
        return os;
    }

} // namespace Composite

#endif //DESIGN_PATTERNS__COMPOSITE_PATTERN__CURRENCY__HPP
