#ifndef DESIGN_PATTERNS__COMPOSITE_PATTERN__WATT__HPP
#define DESIGN_PATTERNS__COMPOSITE_PATTERN__WATT__HPP

namespace Composite {

    class Watt {
    public:
        Watt() = default;

        ~Watt() = default;
    };

} // namespace Composite

#endif //DESIGN_PATTERNS__COMPOSITE_PATTERN__WATT__HPP
