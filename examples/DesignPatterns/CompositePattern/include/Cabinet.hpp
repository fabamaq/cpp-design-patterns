#ifndef DESIGN_PATTERNS__COMPOSITE_PATTERN__CABINET__HPP
#define DESIGN_PATTERNS__COMPOSITE_PATTERN__CABINET__HPP

namespace Composite {

    class Cabinet : public CompositeEquipment {
    public:
        explicit Cabinet(const char *name) : CompositeEquipment(name) {}

        ~Cabinet() override = default;

        Watt Power() override {
            return CompositeEquipment::Power();
        }

        Currency NetPrice() override {
            return CompositeEquipment::NetPrice();
        }

        Currency DiscountPrice() override {
            return CompositeEquipment::DiscountPrice();
        }
    };

} // namespace Composite

#endif //DESIGN_PATTERNS__COMPOSITE_PATTERN__CABINET__HPP
