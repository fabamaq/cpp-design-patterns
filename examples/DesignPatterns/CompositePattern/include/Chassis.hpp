#ifndef DESIGN_PATTERNS__COMPOSITE_PATTERN__CHASSIS__HPP
#define DESIGN_PATTERNS__COMPOSITE_PATTERN__CHASSIS__HPP

#include "CompositeEquipment.hpp"

namespace Composite {

    class Chassis : public CompositeEquipment {
    public:
        explicit Chassis(const char *name) : CompositeEquipment(name) {}

        ~Chassis() override = default;

        Watt Power() override {
            return CompositeEquipment::Power();
        }

        Currency NetPrice() override {
            return CompositeEquipment::NetPrice();
        }

        Currency DiscountPrice() override {
            return CompositeEquipment::DiscountPrice();
        }
    };

} // namespace Composite

#endif //DESIGN_PATTERNS__COMPOSITE_PATTERN__CHASSIS__HPP
