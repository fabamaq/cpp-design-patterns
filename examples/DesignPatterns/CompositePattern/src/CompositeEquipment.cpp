#include "CompositeEquipment.hpp"
#include "ListIterator.hpp"

namespace Composite {

    CompositeEquipment::~CompositeEquipment() = default;

    Watt CompositeEquipment::Power() {
        return Equipment::Power();
    }

    Currency CompositeEquipment::NetPrice() {
        Iterator<Equipment *> *i = CreateIterator();
        Currency total{0};

        for (i->First(); !i->IsDone(); i->Next()) {
            total += i->CurrentItem()->NetPrice();
        }

        delete i;
        return total;
    }

    Currency CompositeEquipment::DiscountPrice() {
        return Equipment::DiscountPrice();
    }

    void CompositeEquipment::Add(Equipment *e) {
        _equipment.Append(e);
    }

    void CompositeEquipment::Remove(Equipment *e) {
        _equipment.Remove(e);
    }

    Iterator<Equipment *> *CompositeEquipment::CreateIterator() {
        return new ListIterator<Equipment *>(&_equipment);
    }

    CompositeEquipment::CompositeEquipment(const char *name) : Equipment(name) {}

} // namespace Composite
