#include "Equipment.hpp"

namespace Composite {

    Equipment::Equipment(const char *name) : _name(name) {
        // ...
    }

    Equipment::~Equipment() = default;

    const char *Equipment::Name() {
        return _name;
    }

    Watt Equipment::Power() {
        return {};
    }

    Currency Equipment::NetPrice() {
        return {};
    }

    Currency Equipment::DiscountPrice() {
        return {};
    }

    void Equipment::Add(Equipment *e) {
        // ...
    }

    void Equipment::Remove(Equipment *e) {
        // ...
    }

    Iterator<Equipment *> *Equipment::CreateIterator() {
        return nullptr;
    }

} // namespace Composite
