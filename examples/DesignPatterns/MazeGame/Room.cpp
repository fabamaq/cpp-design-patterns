#include "Room.hpp"

Room::Room(int roomNo) : _roomNumber(roomNo) {
    // ...
}

MapSite *Room::GetSide(Direction) const {
    return nullptr;
}

void Room::SetSide(Direction, MapSite *) {

}

void Room::Enter() {

}

// Prototype design pattern API
Room::Room(const Room &copy) {
    _sides[0] = copy._sides[0];
    _sides[1] = copy._sides[1];
    _sides[2] = copy._sides[2];
    _sides[3] = copy._sides[3];
    _roomNumber = copy._roomNumber;
}

Room *Room::Clone() const {
    return new Room(*this);
}
