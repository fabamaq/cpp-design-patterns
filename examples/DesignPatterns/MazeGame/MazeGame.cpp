#include "MazeGame.hpp"
#if defined(ABSTRACT_FACTORY_PATTERN)
#include "MazeFactory.hpp"
#elif defined(BUILDER_PATTERN)
#include "MazeBuilder.hpp"
#endif
#include "Direction.hpp"

Maze *MazeGame::CreateMaze() {
    Maze *aMaze = MakeMaze();

    Room *r1 = MakeRoom(1);
    Room *r2 = MakeRoom(2);
    Door *theDoor = MakeDoor(r1,r2);

    aMaze->AddRoom(r1);
    aMaze->AddRoom(r2);

    r1->SetSide(Direction::North, MakeWall());
    r1->SetSide(Direction::East, theDoor);
    r1->SetSide(Direction::South, MakeWall());
    r1->SetSide(Direction::West, MakeWall());

    r2->SetSide(Direction::North, MakeWall());
    r2->SetSide(Direction::East, MakeWall());
    r2->SetSide(Direction::South, MakeWall());
    r2->SetSide(Direction::West, theDoor);

    return aMaze;
}

#if defined(ABSTRACT_FACTORY_PATTERN)
Maze *MazeGame::CreateMaze(MazeFactory &factory) {
    Maze *aMaze = factory.MakeMaze();
    Room *r1 = factory.MakeRoom(1);
    Room *r2 = factory.MakeRoom(2);
    Door *aDoor = factory.MakeDoor(r1, r2);

    aMaze->AddRoom(r1);
    aMaze->AddRoom(r2);

    r1->SetSide(Direction::North, factory.MakeWall());
    r1->SetSide(Direction::East, aDoor);
    r1->SetSide(Direction::South, factory.MakeWall());
    r1->SetSide(Direction::West, factory.MakeWall());

    r2->SetSide(Direction::North, factory.MakeWall());
    r2->SetSide(Direction::East, factory.MakeWall());
    r2->SetSide(Direction::South, factory.MakeWall());
    r2->SetSide(Direction::West, aDoor);

    return aMaze;
}

#elif defined(BUILDER_PATTERN)
Maze *MazeGame::CreateMaze(MazeBuilder& builder) {
    builder.BuildMaze();

    builder.BuildRoom(1);
    builder.BuildRoom(2);
    builder.BuildDoor(1,2);

    return builder.GetMaze();
}

Maze *MazeGame::CreateComplexMaze(MazeBuilder &builder) {
    builder.BuildRoom(1);
    // ...
    builder.BuildRoom(1001);

    return builder.GetMaze();
}
#endif
