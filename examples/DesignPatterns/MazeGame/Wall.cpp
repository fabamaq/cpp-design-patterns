#include "Wall.hpp"

Wall::Wall() = default;

void Wall::Enter() {
    // ...
}

// Prototype design pattern API
Wall::Wall(const Wall &other) = default;

Wall *Wall::Clone() const {
    return new Wall(*this);
}
