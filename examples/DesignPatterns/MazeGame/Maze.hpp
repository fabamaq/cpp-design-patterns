#ifndef DESIGN_PATTERNS__MAZE__HPP
#define DESIGN_PATTERNS__MAZE__HPP

class Room;

class Maze {
public:
    Maze() = default;

    void AddRoom(Room *);

    Room *RoomNo(int) const;

private:
    // ...
};

#endif //DESIGN_PATTERNS__MAZE__HPP
