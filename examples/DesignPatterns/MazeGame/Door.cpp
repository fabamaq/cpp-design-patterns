#include "Door.hpp"

Door::Door(Room *r1, Room *r2) : _room1(r1), _room2(r2), _isOpen(false) {

}

Room *Door::OtherSideFrom(Room *) {
    return nullptr;
}

void Door::Enter() {

}

void Door::Initialize(Room *r1, Room *r2) {
    _room1 = r1;
    _room2 = r2;
}

// Prototype design pattern API
Door::Door(const Door &other) {
    _room1 = other._room1;
    _room2 = other._room2;
}

Door *Door::Clone() const {
    return new Door(*this);
}
