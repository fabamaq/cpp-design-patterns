#ifndef DESIGN_PATTERNS__DIRECTION__HPP
#define DESIGN_PATTERNS__DIRECTION__HPP

enum class Direction {
    North, South, East, West
};

#endif //DESIGN_PATTERNS__DIRECTION__HPP
