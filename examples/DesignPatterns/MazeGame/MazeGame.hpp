#ifndef DESIGN_PATTERNS__MAZE_GAME__HPP
#define DESIGN_PATTERNS__MAZE_GAME__HPP

#include "Maze.hpp"

#include "Room.hpp"
#include "Door.hpp"
#include "Wall.hpp"

#if defined(ABSTRACT_FACTORY_PATTERN)
#include "MazeFactory.hpp"
#elif defined(BUILDER_PATTERN)
#include "MazeBuilder.hpp"

#endif
class MazeGame {
public:
    Maze *CreateMaze();

#if defined(ABSTRACT_FACTORY_PATTERN)
    Maze *CreateMaze(MazeFactory &);

#elif defined(BUILDER_PATTERN)
    Maze *CreateMaze(MazeBuilder &);

    Maze *CreateComplexMaze(MazeBuilder &);

#endif
    // Factory Methods
    virtual Maze *MakeMaze() const { return new Maze; }

    virtual Room *MakeRoom(int n) const { return new Room(n); }

    virtual Wall *MakeWall() const { return new Wall; }

    virtual Door *MakeDoor(Room *r1, Room *r2) const { return new Door(r1, r2); }

};

#endif // DESIGN_PATTERNS__MAZE_GAME__HPP