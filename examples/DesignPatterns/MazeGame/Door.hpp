#ifndef DESIGN_PATTERNS__MAZE_GAME__DOOR__HPP
#define DESIGN_PATTERNS__MAZE_GAME__DOOR__HPP

#include "MapSite.hpp"
#include "Room.hpp"

class Door : public MapSite {
public:
    Door(Room * = nullptr, Room * = nullptr);

    virtual void Initialize(Room *, Room *);

    void Enter() override;

    Room *OtherSideFrom(Room *);

public:
    // Prototype design pattern API
    Door(const Door &); // copy constructor
    virtual Door *Clone() const;

private:
    Room *_room1{};
    Room *_room2{};
    bool _isOpen{};
};

#endif //DESIGN_PATTERNS__MAZE_GAME__DOOR__HPP
