# =============================================================================
# General Settings
# -----------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.0)
project(MazeGame)

# =============================================================================
# Compiler settings
# -----------------------------------------------------------------------------
set(CMAKE_CXX_STANDARD 17)

# =============================================================================
# Workspace settings
# -----------------------------------------------------------------------------
set(MazeGameSourcesFolder ${MazeGameProjectFolder}/src)
set(MazeGameIncludeFolder ${MazeGameProjectFolder}/include)
set(MazeGameTestsFolder ${MazeGameProjectFolder}/tests)

# =============================================================================
# Scaffolding System
# -----------------------------------------------------------------------------
include(${ScaffoldingFolder}/GenerateClass.cmake)
# ! uncomment AFTER changing the ClassPath and ClassName
#GenerateClass("FilteringListTraverser" MazeGame)

# =============================================================================
# ReconfigureCMakeListsFile
# - Comments call to "GenerateClass"
# -----------------------------------------------------------------------------
set(CMakeFile ${MazeGameProjectFolder}/CMakeLists.txt)
file(READ ${CMakeFile} CMakeFileContents)
string(REGEX REPLACE "(.*)[^#]GenerateClass(\\(\".*)" "\\1\n#GenerateClass\\2" result ${CMakeFileContents})
file(WRITE ${CMakeFile} ${result})

# =============================================================================
# Targets settings
# -----------------------------------------------------------------------------
add_executable(MazeGame MazeGame.cpp)
target_include_directories(MazeGame PUBLIC ${MazeGameProjectFolder})
# since we need to reload cmake to generate a new class, this works fine
file(GLOB files "${MazeGameProjectFolder}/*.cpp")
set(SOURCES "")
foreach(file ${files})
    set(SOURCES ${SOURCES} ${file})
endforeach()
target_sources(MazeGame PUBLIC ${SOURCES})
target_link_libraries(MazeGame PUBLIC gtest gtest_main)