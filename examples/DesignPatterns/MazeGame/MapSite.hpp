#ifndef DESIGN_PATTERNS__MAP_SITE__HPP
#define DESIGN_PATTERNS__MAP_SITE__HPP

class MapSite {
public:
    virtual void Enter() = 0;
};

#endif //DESIGN_PATTERNS__MAP_SITE__HPP
