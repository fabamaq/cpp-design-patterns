#ifndef DESIGN_PATTERNS__MAZE_GAME__WALL__HPP
#define DESIGN_PATTERNS__MAZE_GAME__WALL__HPP

#include "MapSite.hpp"

class Wall : public MapSite {
public:
    Wall();

    void Enter() override;

public:
    // Prototype design pattern API
    Wall(const Wall &); // copy constructor
    virtual Wall *Clone() const;
};

#endif //DESIGN_PATTERNS__MAZE_GAME__WALL__HPP
