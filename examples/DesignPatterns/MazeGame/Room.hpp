#ifndef DESIGN_PATTERNS__MAZE_GAME__ROOM__HPP
#define DESIGN_PATTERNS__MAZE_GAME__ROOM__HPP

#include <MapSite.hpp>
#include <Direction.hpp>

class Room : public MapSite {
public:
    explicit Room(int roomNo);

    MapSite *GetSide(Direction) const;

    void SetSide(Direction, MapSite *);

    void Enter() override;

public:
    // Prototype design pattern API
    Room(const Room &);
    virtual Room *Clone() const;

private:
    MapSite *_sides[4]{};
    int _roomNumber{};
};


#endif //DESIGN_PATTERNS__MAZE_GAME__ROOM__HPP
