/******************************************************************************
 * @file Subject.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Subject.hpp"
#include "Observer.hpp"
#include "ListIterator.hpp"

Subject::Subject() {
    _observers = new List<Observer *>();
}

Subject::~Subject() {
    _observers->RemoveAll();
    delete _observers;
}

void Subject::Attach(Observer *o) {
    _observers->Append(o);
}

void Subject::Detach(Observer *o) {
    _observers->Remove(o);
}

void Subject::Notify() {
    ListIterator<Observer *> i(_observers);

    for (i.First(); !i.IsDone(); i.Next()) {
        i.CurrentItem()->Update(this);
    }
}
