/******************************************************************************
 * @file AnalogClock.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "AnalogClock.hpp"

#include <iostream>

AnalogClock::AnalogClock(ClockTimer *s) {
    _subject = s;
    _subject->Attach(this);
}

AnalogClock::~AnalogClock() {
    _subject->Detach(this);
//    delete _subject;
}

void AnalogClock::Update(Subject *theChangedSubject) {
    if (theChangedSubject == _subject) {
        Draw();
    }
}

void AnalogClock::Draw() {
    // get the new values from the subject

    int hour = _subject->GetHour();
    int minute = _subject->GetMinute();
    int second = _subject->GetSecond();

    // draw the digital clock
    std::cout << "AnalogClock: " << hour << ":" << minute << ":" << second << std::endl;
}

