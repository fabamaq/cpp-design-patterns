/******************************************************************************
 * @file ClockTimer.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "ClockTimer.hpp"

ClockTimer::ClockTimer() {

}

int ClockTimer::GetHour() {
    return 0;
}

int ClockTimer::GetMinute() {
    return 0;
}

int ClockTimer::GetSecond() {
    return 0;
}

void ClockTimer::Tick() {
    // update internal time-keeping state
    // ...
    Notify();
}
