/******************************************************************************
 * @file Widget.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Widget.hpp"
#include <iostream>

void Widget::Draw() {
    std::cout << "Widget::Draw()\n";
}
