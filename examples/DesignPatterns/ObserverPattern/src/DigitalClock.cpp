/******************************************************************************
 * @file DigitalClock.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "DigitalClock.hpp"

#include <iostream>

DigitalClock::DigitalClock(ClockTimer *s) {
    _subject = s;
    _subject->Attach(this);
}

DigitalClock::~DigitalClock() {
    _subject->Detach(this);
//    delete _subject;
}

void DigitalClock::Update(Subject *theChangedSubject) {
    if (theChangedSubject == _subject) {
        Draw();
    }
}

void DigitalClock::Draw() {
    // get the new values from the subject

    int hour = _subject->GetHour();
    int minute = _subject->GetMinute();
    int second = _subject->GetSecond();

    // draw the digital clock
    std::cout << "DigitalClock: " << hour << ":" << minute << ":" << second << std::endl;
}


