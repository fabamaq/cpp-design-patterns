/******************************************************************************
 * @file Observer.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__OBSERVER_PATTERN__OBSERVER__HPP
#define DESIGN_PATTERNS__OBSERVER_PATTERN__OBSERVER__HPP

class Subject;

class Observer {
public:
    virtual ~Observer() = default;

    virtual void Update(Subject *theChangedSubject) = 0;

protected:
    Observer() = default;
};


#endif // DESIGN_PATTERNS__OBSERVER_PATTERN__OBSERVER__HPP
