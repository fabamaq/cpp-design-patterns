/******************************************************************************
 * @file ClockTimer.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__OBSERVER_PATTERN__CLOCK_TIMER__HPP
#define DESIGN_PATTERNS__OBSERVER_PATTERN__CLOCK_TIMER__HPP

#include "Subject.hpp"

class ClockTimer : public Subject {
public:
    ClockTimer();

    virtual int GetHour();

    virtual int GetMinute();

    virtual int GetSecond();

    void Tick();
};


#endif // DESIGN_PATTERNS__OBSERVER_PATTERN__CLOCK_TIMER__HPP
