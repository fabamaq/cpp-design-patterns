/******************************************************************************
 * @file DigitalClock.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__OBSERVER_PATTERN__DIGITAL_CLOCK__HPP
#define DESIGN_PATTERNS__OBSERVER_PATTERN__DIGITAL_CLOCK__HPP

#include "Widget.hpp"
#include "Observer.hpp"
#include "ClockTimer.hpp"

class DigitalClock : public Widget, public Observer {
public:
    explicit DigitalClock(ClockTimer*);
    ~DigitalClock() override;

    void Update(Subject*) override;

    void Draw() override;

private:
    ClockTimer* _subject;
};


#endif // DESIGN_PATTERNS__OBSERVER_PATTERN__DIGITAL_CLOCK__HPP
