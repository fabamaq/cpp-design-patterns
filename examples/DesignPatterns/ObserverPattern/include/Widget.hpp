/******************************************************************************
 * @file Widget.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__OBSERVER_PATTERN__WIDGET__HPP
#define DESIGN_PATTERNS__OBSERVER_PATTERN__WIDGET__HPP

class Widget {
public:
    virtual void Draw();
};

#endif // DESIGN_PATTERNS__OBSERVER_PATTERN__WIDGET__HPP
