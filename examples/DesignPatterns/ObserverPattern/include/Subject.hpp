/******************************************************************************
 * @file Subject.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__OBSERVER_PATTERN__SUBJECT__HPP
#define DESIGN_PATTERNS__OBSERVER_PATTERN__SUBJECT__HPP

#include "List.hpp"

class Observer;

class Subject {
public:
	virtual ~Subject();

	virtual void Attach(Observer*);
	virtual void Detach(Observer*);
	virtual void Notify();

protected:
	Subject();

private:
    List<Observer*>* _observers;
};


#endif // DESIGN_PATTERNS__OBSERVER_PATTERN__SUBJECT__HPP
