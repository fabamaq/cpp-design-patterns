/******************************************************************************
 * @file AnalogClock.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__OBSERVER_PATTERN__ANALOG_CLOCK__HPP
#define DESIGN_PATTERNS__OBSERVER_PATTERN__ANALOG_CLOCK__HPP

#include "Widget.hpp"
#include "Observer.hpp"
#include "ClockTimer.hpp"

class AnalogClock : public Widget, public Observer {
public:
    explicit AnalogClock(ClockTimer*);
    ~AnalogClock() override;

    void Update(Subject*) override;

    void Draw() override;

private:
    ClockTimer* _subject;
};

#endif // DESIGN_PATTERNS__OBSERVER_PATTERN__ANALOG_CLOCK__HPP
