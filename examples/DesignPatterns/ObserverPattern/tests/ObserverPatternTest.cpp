/******************************************************************************
 * @file ObserverPatternTest.cpp
 *
 * @brief Observer design pattern
 * - "Define a one-to-many dependency between objects so that when one object
 * changes state, all its dependents are notified and updated automatically"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Google Test Framework
#include <gtest/gtest.h>

// Project Headers
#include "ClockTimer.hpp"
#include "AnalogClock.hpp"
#include "DigitalClock.hpp"

// ---------------------------------------------------------------------- Tests
TEST(ObserverPatternTest, DefaultFailingTest) {
    ClockTimer* timer = new ClockTimer;
    AnalogClock *analogClock = new AnalogClock(timer);
    DigitalClock *digitalClock = new DigitalClock(timer);

    timer->Tick();
}
