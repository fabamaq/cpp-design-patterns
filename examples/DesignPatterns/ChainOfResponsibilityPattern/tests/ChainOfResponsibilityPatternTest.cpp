/******************************************************************************
 * @file ChainOfResponsibilityPatternTest.cpp
 *
 * @brief Chain of Responsibility design pattern
 * - "Avoid coupling the sender of a request to its receiver by
 * ... giving more than one object a chance to handle the request.
 * - Chain the receiving objects and
 * ... pass the request along the chain until an object handles it."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Application.hpp"
#include "Dialog.hpp"
#include "Button.hpp"

using namespace ChainOfResponsibility;

// Google Test Framework
#include <gtest/gtest.h>

// ---------------------------------------------------------------------- Tests
TEST(ChainOfResponsibilityPatternTest, DefaultFailingTest) {
	const Topic PRINT_TOPIC = 1;
	const Topic PAPER_ORIENTATION_TOPIC = 2;
	const Topic APPLICATION_TOPIC = 3;

	Application* application = new Application(APPLICATION_TOPIC);
	Dialog* dialog = new Dialog(application, PRINT_TOPIC);
	Button* button = new Button(dialog, PAPER_ORIENTATION_TOPIC);

	button->HandleHelp();
	dialog->HandleHelp();
	application->HandleHelp();
}
