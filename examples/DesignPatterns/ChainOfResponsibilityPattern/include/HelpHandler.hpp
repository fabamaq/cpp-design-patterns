/******************************************************************************
 * @file HelpHandler.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__HELP_HANDLER__HPP
#define DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__HELP_HANDLER__HPP

namespace ChainOfResponsibility {

    typedef int Topic;
    const Topic NO_HELP_TOPIC = -1;

    class HelpHandler {
    public:
        explicit HelpHandler(HelpHandler * = nullptr, Topic = NO_HELP_TOPIC);

        virtual bool HasHelp();

        virtual void SetHandler(HelpHandler *, Topic);

        virtual void HandleHelp();

    private:
        HelpHandler *_successor;
        Topic _topic;
    };

} // ChainOfResponsibility

#endif // DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__HELP_HANDLER__HPP
