/******************************************************************************
 * @file Widget.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__WIDGET__HPP
#define DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__WIDGET__HPP

#include "HelpHandler.hpp"

namespace ChainOfResponsibility {

    class Widget : public HelpHandler {
    protected:
        explicit Widget(Widget *parent, Topic t = NO_HELP_TOPIC);

    private:
        Widget *_parent;
    };

} // ChainOfResponsibility

#endif // DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__WIDGET__HPP
