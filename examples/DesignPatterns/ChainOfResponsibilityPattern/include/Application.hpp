/******************************************************************************
 * @file Application.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__APPLICATION__HPP
#define DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__APPLICATION__HPP

#include "HelpHandler.hpp"

namespace ChainOfResponsibility {

    class Application : public HelpHandler {
    public:
        explicit Application(Topic t) : HelpHandler(nullptr, t) {}

        void HandleHelp() override;
        // application-specific operations...
    };

} // ChainOfResponsibility

#endif // DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__APPLICATION__HPP
