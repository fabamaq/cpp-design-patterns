/******************************************************************************
 * @file Button.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__BUTTON__HPP
#define DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__BUTTON__HPP

#include "Widget.hpp"

namespace ChainOfResponsibility {

    class Button : public Widget {
    public:
        explicit Button(Widget *d, Topic t = NO_HELP_TOPIC);

        void HandleHelp() override;
        // Widget operations that Button overrides...
    };

} // ChainOfResponsibility

#endif // DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__BUTTON__HPP
