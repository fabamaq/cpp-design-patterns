/******************************************************************************
 * @file Dialog.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__DIALOG__HPP
#define DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__DIALOG__HPP

#include "Widget.hpp"

namespace ChainOfResponsibility {

    class Dialog : public Widget {
    public:
        explicit Dialog(HelpHandler *h, Topic t = NO_HELP_TOPIC);

        void HandleHelp() override;

        // Widget operations that dialog overrides...
        // ...
    };

} // ChainOfResponsibility

#endif // DESIGN_PATTERNS__CHAIN_OF_RESPONSIBILITY_PATTERN__DIALOG__HPP
