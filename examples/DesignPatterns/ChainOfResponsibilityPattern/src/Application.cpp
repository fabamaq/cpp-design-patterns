/******************************************************************************
 * @file Application.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Application.hpp"

#include <iostream>

namespace ChainOfResponsibility {

    void Application::HandleHelp() {
        // show a list of help topics
        std::cout << "Application is helping...\n";
    }

} // ChainOfResponsibility