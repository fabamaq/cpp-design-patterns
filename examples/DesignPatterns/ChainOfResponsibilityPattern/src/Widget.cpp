/******************************************************************************
 * @file Widget.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Widget.hpp"

namespace ChainOfResponsibility {

    Widget::Widget(Widget *w, Topic t) : HelpHandler(w, t) {
        _parent = w;
    }

} // ChainOfResponsibility