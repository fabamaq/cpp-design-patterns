/******************************************************************************
 * @file HelpHandler.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "HelpHandler.hpp"

namespace ChainOfResponsibility {

    HelpHandler::HelpHandler(HelpHandler *h, Topic t) : _successor(h), _topic(t) {}

    bool HelpHandler::HasHelp() {
        return _topic != NO_HELP_TOPIC;
    }

    void HelpHandler::SetHandler(HelpHandler *h, Topic t) {
        _successor = h;
        _topic = t;
    }

    void HelpHandler::HandleHelp() {
        if (_successor) {
            _successor->HandleHelp();
        }
    }

} // ChainOfResponsibility
