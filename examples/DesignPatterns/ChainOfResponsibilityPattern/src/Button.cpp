/******************************************************************************
 * @file Button.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Button.hpp"

#include <iostream>

namespace ChainOfResponsibility {

    Button::Button(Widget *h, Topic t) : Widget(h, t) {}

    void Button::HandleHelp() {
        if (HasHelp()) {
            std::cout << "Button is helping...\n";
        } else {
            HelpHandler::HandleHelp();
        }
    }

} // ChainOfResponsibility