/******************************************************************************
 * @file Dialog.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include <iostream>
#include "Dialog.hpp"

namespace ChainOfResponsibility {
    Dialog::Dialog(HelpHandler *h, Topic t) : Widget(nullptr) {
        SetHandler(h, t);
    }

    void Dialog::HandleHelp() {
        if (HasHelp()) {
            std::cout << "Dialog is helping...\n";
        } else {
            HelpHandler::HandleHelp();
        }
    }

} // ChainOfResponsibility