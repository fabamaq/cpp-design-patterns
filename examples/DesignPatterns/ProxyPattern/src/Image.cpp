/******************************************************************************
 * @file Image.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "Image.hpp"

Image::Image(const char *file) {

}

void Image::Draw(const Point &at) {

}

void Image::HandleMouse(Event &event) {

}

const Point &Image::GetExtent() {
    return Point::Zero;
}

void Image::Load(std::istream &from) {

}

void Image::Save(std::ostream &to) {

}
