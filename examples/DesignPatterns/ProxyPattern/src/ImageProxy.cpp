/******************************************************************************
 * @file ImageProxy.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include <cstring>
#include "ImageProxy.hpp"

ImageProxy::ImageProxy(const char *imageFile) {
    #ifdef WIN32
    _fileName = _strdup(imageFile);
    #else
    _fileName = strdup(imageFile);
    #endif
    _extent = Point::Zero; // don't know extent yet
    _image = nullptr;
}

ImageProxy::~ImageProxy() = default;

void ImageProxy::Draw(const Point &at) {
    GetImage()->Draw(at);
}

void ImageProxy::HandleMouse(Event &event) {
    GetImage()->HandleMouse(event);
}

const Point &ImageProxy::GetExtent() {
    if (_extent == Point::Zero) {
        _extent = GetImage()->GetExtent();
    }
    return _extent;
}

void ImageProxy::Load(std::istream &from) {
    from >> _extent >> _fileName;
}

void ImageProxy::Save(std::ostream &to) {
    to << _extent << _fileName;
}

Image *ImageProxy::GetImage() {
    if (_image == nullptr) {
        _image = new Image(_fileName);
    }
    return _image;
}
