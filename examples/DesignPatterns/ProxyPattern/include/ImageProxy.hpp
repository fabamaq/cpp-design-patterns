/******************************************************************************
 * @file ImageProxy.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__PROXY_PATTERN__IMAGE_PROXY__HPP
#define DESIGN_PATTERNS__PROXY_PATTERN__IMAGE_PROXY__HPP

#include "Graphic.hpp"
#include "Image.hpp"
#include "Point.hpp"

class ImageProxy : public Graphic {
public:
    explicit ImageProxy(const char *imageFile);

    ~ImageProxy() override;

    void Draw(const Point &at) override;

    void HandleMouse(Event &event) override;

    const Point &GetExtent() override;

    void Load(std::istream &from) override;

    void Save(std::ostream &to) override;

protected:
    Image *GetImage();

private:
    Image *_image;
    Point _extent;
    char *_fileName;
};

#endif // DESIGN_PATTERNS__PROXY_PATTERN__IMAGE_PROXY__HPP