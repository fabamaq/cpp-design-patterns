/******************************************************************************
 * @file Graphic.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__PROXY_PATTERN__GRAPHIC__HPP
#define DESIGN_PATTERNS__PROXY_PATTERN__GRAPHIC__HPP

#include "Point.hpp"
#include <istream>
#include <ostream>

class Event;

class Graphic {
public:
    virtual ~Graphic() = default;

    virtual void Draw(const Point &at) = 0;

    virtual void HandleMouse(Event &event) = 0;

    virtual const Point &GetExtent() = 0;

    virtual void Load(std::istream &from) = 0;

    virtual void Save(std::ostream &to) = 0;

protected:
    Graphic() = default;
};

#endif // DESIGN_PATTERNS__PROXY_PATTERN__GRAPHIC__HPP
