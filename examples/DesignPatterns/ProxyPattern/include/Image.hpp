/******************************************************************************
 * @file Image.hpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// -------------------------------------------------------------- Include Guard
#ifndef DESIGN_PATTERNS__PROXY_PATTERN__IMAGE__HPP
#define DESIGN_PATTERNS__PROXY_PATTERN__IMAGE__HPP

#include "Graphic.hpp"

class Image : public Graphic {
public:
    explicit Image(const char *file);        // loads image from a file

    ~Image() override = default;

    void Draw(const Point &at) override;

    void HandleMouse(Event &event) override;

    const Point &GetExtent() override;

    void Load(std::istream &from) override;

    void Save(std::ostream &to) override;

private:
    // ...
};

#endif // DESIGN_PATTERNS__PROXY_PATTERN__IMAGE__HPP
