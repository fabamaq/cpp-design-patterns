/******************************************************************************
 * @file ProxyPatternTest.cpp
 *
 * @brief Proxy design pattern
 * - "Provide a surrogate or placeholder for another object to
 * ... control access to it"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes

#include "Graphic.hpp"
#include "Image.hpp"
#include "ImageProxy.hpp"

const Point Point::Zero(0.0, 0.0);

// Google Test Framework
#include <gtest/gtest.h>

class TextDocument {
public:
    TextDocument() = default;

    void Insert(Graphic *) {}
    // ...
};

// ---------------------------------------------------------------------- Tests
TEST(ProxyPatternTest, DefaultFailingTest) {
    auto *text = new TextDocument;
    // ...
    text->Insert(new ImageProxy("anImageFileName"));

    delete text;
}
