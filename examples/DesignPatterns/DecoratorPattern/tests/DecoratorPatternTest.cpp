/******************************************************************************
 * @file DecoratorPatternTest.cpp
 * @brief Decorator design pattern
 * - "Attach additional responsibilities to an object dynamically. Decorators
 * ... provide an alternative to subclassing for extending functionality"
 *****************************************************************************/
// ------------------------------------------------------------------- Includes

#include "VisualComponent.hpp"
#include "Decorator.hpp"
#include "BorderDecorator.hpp"
#include "ScrollDecorator.hpp"

// Google Test Framework
#include <gtest/gtest.h>

class Window {
public:
    void SetContents(VisualComponent *contents) {}
};

class TextView : public VisualComponent {
public:
    void Draw() override {

    }

    void Resize() override {

    }
};

// ---------------------------------------------------------------------- Tests
TEST(DecoratorPatternTest, DefaultFailingTest) {
    auto *window = new Window;
    auto *textView = new TextView;

    window->SetContents(new BorderDecorator(new ScrollDecorator(textView), 1));
}
