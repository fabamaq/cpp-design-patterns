#ifndef DESIGN_PATTERNS__DECORATOR_PATTERN__BORDER_DECORATOR__HPP
#define DESIGN_PATTERNS__DECORATOR_PATTERN__BORDER_DECORATOR__HPP

#include "Decorator.hpp"

class BorderDecorator : public Decorator {
public:
    BorderDecorator(VisualComponent *, int borderWidth);

    void Draw() override;

private:
    void DrawBorder(int);

private:
    int _width;
};

#endif //DESIGN_PATTERNS__DECORATOR_PATTERN__BORDER_DECORATOR__HPP
