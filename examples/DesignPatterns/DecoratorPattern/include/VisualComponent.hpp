#ifndef DESIGN_PATTERNS__DECORATOR_PATTERN__VISUAL_COMPONENT__HPP
#define DESIGN_PATTERNS__DECORATOR_PATTERN__VISUAL_COMPONENT__HPP

class VisualComponent {
public:
    VisualComponent() = default;

    virtual void Draw() = 0;

    virtual void Resize() = 0;
    // ...
};

#endif //DESIGN_PATTERNS__DECORATOR_PATTERN__VISUAL_COMPONENT__HPP
