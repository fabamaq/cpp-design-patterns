#ifndef DESIGN_PATTERNS__DECORATOR_PATTERN__SCROLL_DECORATOR__HPP
#define DESIGN_PATTERNS__DECORATOR_PATTERN__SCROLL_DECORATOR__HPP

#include "Decorator.hpp"

class ScrollDecorator : public Decorator {
public:
    explicit ScrollDecorator(VisualComponent *c) : Decorator(c) {}

    void Draw() override {
        Decorator::Draw();
    }
};

#endif //DESIGN_PATTERNS__DECORATOR_PATTERN__SCROLL_DECORATOR__HPP
