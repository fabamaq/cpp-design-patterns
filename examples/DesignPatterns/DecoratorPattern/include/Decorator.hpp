#ifndef DESIGN_PATTERNS__DECORATOR_PATTERN__DECORATOR__HPP
#define DESIGN_PATTERNS__DECORATOR_PATTERN__DECORATOR__HPP

#include "VisualComponent.hpp"

class Decorator : public VisualComponent {
public:
    explicit Decorator(VisualComponent *);

    void Draw() override;

    void Resize() override;
    // ...

private:
    VisualComponent *_component;
};

#endif //DESIGN_PATTERNS__DECORATOR_PATTERN__DECORATOR__HPP
