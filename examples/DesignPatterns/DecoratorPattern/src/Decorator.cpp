#include "Decorator.hpp"

Decorator::Decorator(VisualComponent *c) : _component(c) {}

void Decorator::Draw() {
    _component->Draw();
}

void Decorator::Resize() {
    _component->Resize();
}
