#include "BorderDecorator.hpp"
#include <iostream>

BorderDecorator::BorderDecorator(VisualComponent *c, int borderWidth) : Decorator(c), _width(borderWidth) {}

void BorderDecorator::Draw() {
    Decorator::Draw();
    DrawBorder(_width);
}

void BorderDecorator::DrawBorder(int val) {
    std::cout << "DrawBorder(" << val << ")\n";
    std::cout << "_width: " << _width << '\n';
}
