# =============================================================================
# General Settings
# -----------------------------------------------------------------------------
cmake_minimum_required(VERSION 3.0)
project(FactoryMethodPattern)

# =============================================================================
# Compiler settings
# -----------------------------------------------------------------------------
set(CMAKE_CXX_STANDARD 17)

# =============================================================================
# Workspace settings
# -----------------------------------------------------------------------------
set(FactoryMethodPatternSourcesFolder ${FactoryMethodPatternProjectFolder}/src)
set(FactoryMethodPatternIncludeFolder ${FactoryMethodPatternProjectFolder}/include)
set(FactoryMethodPatternTestsFolder ${FactoryMethodPatternProjectFolder}/tests)

# =============================================================================
# Targets settings
# -----------------------------------------------------------------------------
# Target Executable
add_executable(FactoryMethodPatternTest ${FactoryMethodPatternTestsFolder}/FactoryMethodPatternTest.cpp)
target_include_directories(FactoryMethodPatternTest PUBLIC ${FactoryMethodPatternIncludeFolder})
# target_sources(FactoryMethodPatternTest PUBLIC ${FactoryMethodPatternSourcesFolder}/Example.cpp)
target_link_libraries(FactoryMethodPatternTest PUBLIC gtest gtest_main)
