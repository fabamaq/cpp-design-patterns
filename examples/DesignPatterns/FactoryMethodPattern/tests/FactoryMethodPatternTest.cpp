/******************************************************************************
 * @file FactoryMethodPatternTest.cpp
 *
 * @brief Factory Method design pattern
 * - "Define an interface for creating an object, but let subclasses decide
 * ... which class to instantiate.
 * - Factory Method lets a class defer instantiation to subclasses."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes

// Google Test Framework
#include <gtest/gtest.h>

// ---------------------------------------------------------------------- Tests
TEST(FactoryMethodPatternTest, DefaultFailingTest) {
	FAIL() << "Unimplemented Test";
}
