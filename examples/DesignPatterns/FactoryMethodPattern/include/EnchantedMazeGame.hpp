#ifndef DESIGNPATTERNS_ENCHANTEDMAZEGAME_HPP
#define DESIGNPATTERNS_ENCHANTEDMAZEGAME_HPP

#include "MazeGame.hpp"
#include "EnchantedRoom.hpp"
#include "DoorNeedingSpell.hpp"

class EnchantedMazeGame : public MazeGame {
public:
    EnchantedMazeGame() = default;

    virtual Door *MakeDoor() const {
        return new DoorNeedingSpell;
    }

    virtual Room *MakeRoom(int n) const {
        return new EnchantedRoom(n);
    }
};

#endif //DESIGNPATTERNS_ENCHANTEDMAZEGAME_HPP
