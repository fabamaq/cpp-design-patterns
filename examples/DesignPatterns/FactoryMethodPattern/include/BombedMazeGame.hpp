#ifndef DESIGNPATTERNS_BOMBEDMAZEGAME_HPP
#define DESIGNPATTERNS_BOMBEDMAZEGAME_HPP

#include "MazeGame.hpp"
#include "BombedWall.hpp"
#include "RoomWithABomb.hpp"

class BombedMazeGame : public MazeGame {
public:
    BombedMazeGame() = default;

    virtual Wall *MakeWall() const {
        return new BombedWall;
    }

    virtual Room *MakeRoom(int n) const {
        return new RoomWithABomb(n);
    }
};


#endif //DESIGNPATTERNS_BOMBEDMAZEGAME_HPP
