/******************************************************************************
 * @file AbstractFactoryPatternTest.cpp
 *
 * @brief Abstract Factory design pattern
 * - "Provide an interface for creating families of related or
 * ... dependent objects without specifying their concrete classes."
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
// Google Test Framework
#include <gtest/gtest.h>

// Project Header Files
#include "BombedMazeFactory.hpp"
#include "EnchantedMazeFactory.hpp"
#include "MazeGame.hpp"


// ---------------------------------------------------------------------- Tests
TEST(MazeFactoryTest, CreateMazeWithBombs) {
    MazeGame game;
    BombedMazeFactory factory;

    game.CreateMaze(factory);
}

TEST(MazeFactoryTest, CreateEnchantedMaze) {
    MazeGame game;
    EnchantedMazeFactory factory;

    game.CreateMaze(factory);
}
