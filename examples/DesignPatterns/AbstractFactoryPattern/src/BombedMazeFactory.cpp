#include "BombedMazeFactory.hpp"
#include "BombedWall.hpp"
#include "RoomWithABomb.hpp"

BombedMazeFactory::BombedMazeFactory() = default;

Wall *BombedMazeFactory::MakeWall() const {
    return new BombedWall;
}

Room *BombedMazeFactory::MakeRoom(int n) const {
    return new RoomWithABomb(n);
}
