#include <BombedWall.hpp>

BombedWall::BombedWall() {

}

BombedWall::BombedWall(const BombedWall &other) : Wall(other) {
    _bomb = other._bomb;
}

Wall *BombedWall::Clone() const {
    return new BombedWall(*this);
}

bool BombedWall::HasBomb() {
    return false;
}
