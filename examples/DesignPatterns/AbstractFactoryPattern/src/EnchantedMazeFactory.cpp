#include <EnchantedMazeFactory.hpp>
#include <DoorNeedingSpell.hpp>
#include <EnchantedRoom.hpp>



EnchantedMazeFactory::EnchantedMazeFactory() = default;

Room *EnchantedMazeFactory::MakeRoom(int n) const { return new EnchantedRoom(n, CastSpell()); }

Door *EnchantedMazeFactory::MakeDoor(Room *r1, Room *r2) const {
    return new DoorNeedingSpell(r1, r2);
}

Spell *EnchantedMazeFactory::CastSpell() const {
    return nullptr;
}
