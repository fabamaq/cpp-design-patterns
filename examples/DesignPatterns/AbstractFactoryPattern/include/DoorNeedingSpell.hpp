#ifndef DESIGN_PATTERNS__DOOR_NEEDING_SPELL__HPP
#define DESIGN_PATTERNS__DOOR_NEEDING_SPELL__HPP

#include "Door.hpp"
#include "Room.hpp"

class DoorNeedingSpell: public Door {
public:
    explicit DoorNeedingSpell(Room*, Room*);
};

#endif //DESIGN_PATTERNS__DOOR_NEEDING_SPELL__HPP
