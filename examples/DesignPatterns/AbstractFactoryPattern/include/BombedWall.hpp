#ifndef DESIGN_PATTERNS__BOMBED_WALL__HPP
#define DESIGN_PATTERNS__BOMBED_WALL__HPP

#include "Wall.hpp"

class BombedWall : public Wall {
public:
    // Prototype Pattern
    BombedWall();
    BombedWall(const BombedWall&);
    virtual Wall* Clone() const;
    bool HasBomb();

private:
    bool _bomb;
};


#endif //DESIGN_PATTERNS__BOMBED_WALL__HPP
