#ifndef DESIGN_PATTERNS__ABSTRACT_FACTORY_PATTERN__MAZE_FACTORY__HPP
#define DESIGN_PATTERNS__ABSTRACT_FACTORY_PATTERN__MAZE_FACTORY__HPP

#include <Maze.hpp>
#include <Wall.hpp>
#include <Room.hpp>
#include <Door.hpp>

class MazeFactory {
public:
    MazeFactory() = default;

    virtual Maze* MakeMaze() const { return new Maze; }
    virtual Wall* MakeWall() const { return new Wall; }
    virtual Room* MakeRoom(int n) const { return new Room(n); }
    virtual Door* MakeDoor(Room* r1, Room* r2) const { return new Door(r1, r2); }
};

#endif //DESIGN_PATTERNS__ABSTRACT_FACTORY_PATTERN__MAZE_FACTORY__HPP
