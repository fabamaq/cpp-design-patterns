#ifndef DESIGN_PATTERNS__ENCHANTED_ROOM__HPP
#define DESIGN_PATTERNS__ENCHANTED_ROOM__HPP

#include "Room.hpp"

class EnchantedRoom : public Room {
public:
    explicit EnchantedRoom(int n, Spell *pSpell);
};


#endif //DESIGN_PATTERNS__ENCHANTED_ROOM__HPP
