#ifndef DESIGN_PATTERNS__ABSTRACT_FACTORY_PATTERN__BOMBED_MAZE_FACTORY__HPP
#define DESIGN_PATTERNS__ABSTRACT_FACTORY_PATTERN__BOMBED_MAZE_FACTORY__HPP

#include "MazeFactory.hpp"
#include "Wall.hpp"
#include "Room.hpp"

class BombedMazeFactory : public MazeFactory {
public:
    BombedMazeFactory();

    virtual Wall *MakeWall() const;

    virtual Room *MakeRoom(int n) const;
};

#endif //DESIGN_PATTERNS__ABSTRACT_FACTORY_PATTERN__BOMBED_MAZE_FACTORY__HPP
