#ifndef DESIGN_PATTERNS__ROOM_WITH_A_BOMB__HPP
#define DESIGN_PATTERNS__ROOM_WITH_A_BOMB__HPP

#include "Room.hpp"

class RoomWithABomb : public Room {
public:
    RoomWithABomb();

    explicit RoomWithABomb(int n);
};

#endif //DESIGN_PATTERNS__ROOM_WITH_A_BOMB__HPP
