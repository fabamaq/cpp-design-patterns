#ifndef DESIGN_PATTERNS__ENCHANTED_MAZE_FACTORY__HPP
#define DESIGN_PATTERNS__ENCHANTED_MAZE_FACTORY__HPP

#include <MazeFactory.hpp>
#include <Spell.hpp>
#include <Room.hpp>
#include <Door.hpp>

class EnchantedMazeFactory : public MazeFactory {
public:
    EnchantedMazeFactory();

    virtual Room *MakeRoom(int n) const;

    virtual Door *MakeDoor(Room *r1, Room *r2) const;

protected:
    Spell* CastSpell() const;
};

#endif //DESIGN_PATTERNS__ENCHANTED_MAZE_FACTORY__HPP
