/******************************************************************************
 * @file LexiTest.cpp
 * @copyright Copyright (c) 2018 Pedro André Oliveira. All Rights Reserved.
 *****************************************************************************/
// ------------------------------------------------------------------- Includes
#include "LexiTest.hpp"

// Google Test Framework
#include <gtest/gtest.h>



// ---------------------------------------------------------------------- Tests
TEST(LexiTest, DefaultFailingTest) {
	FAIL() << "Unimplemented Test";
}
