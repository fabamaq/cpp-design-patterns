#pragma once

// C++ Standard Library
#include "StandardLibrary.hpp"

// Game Engine Library
#include "GameEngine/GameEngine.hpp"

// Maze Game Library
#include "MazeGame/EnemyDesign/AbstractEnemyFactory.hpp" // mEnemyFactory
#include "MazeGame/SceneDesign/AbstractSceneFactory.hpp" // mSceneFactory

// Forward Declared
class StandardSceneBuilder;

/// ===========================================================================
/// @implements Factory Method design pattern
/// @implements Singleton Pattern
/// ===========================================================================

enum class Difficulty { Easy, Hard };

enum class SceneLevel { Bombed, Enchanted };

class MazeGame final : public Game {
  public:
	int OnStaticMain() override;
	int OnStaticExit() override;

	void SetDifficulty();
	void SetScene();

	/// @note Factory Method friendship declaration
	friend Game *CreateGame();

	~MazeGame() = default;

  private:
	MazeGame()					   = default;
	MazeGame(const MazeGame &)	   = delete;
	MazeGame(MazeGame &&) noexcept = default;

	MazeGame &operator=(const MazeGame &) = delete;
	MazeGame &operator=(MazeGame &&) noexcept = default;

	void CreateScene(StandardSceneBuilder &builder);

	std::unique_ptr<AbstractEnemyFactory> mEnemyFactory;
	std::unique_ptr<AbstractSceneFactory> mSceneFactory;

	Difficulty mDifficulty;
	SceneLevel mSceneLevel;
};

/// ---------------------------------------------------------------------------
/// @implements to_string overload for enum class Difficulty and SceneLevel
/// ---------------------------------------------------------------------------
std::string to_string(Difficulty d);
std::string to_string(SceneLevel s);
