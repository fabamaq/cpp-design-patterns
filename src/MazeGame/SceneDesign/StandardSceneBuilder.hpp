#pragma once

#include "StandardLibrary.hpp"

#include "MazeGame/SceneDesign/SceneBuilder.hpp"

class Maze;
class Room;
enum class Direction;

class StandardSceneBuilder : public SceneBuilder {
  public:
	StandardSceneBuilder();

	virtual void BuildMaze();
	virtual void BuildRoom(int);
	virtual void BuildDoor(int, int);

	virtual Maze *GetMaze();

  private:
	Direction CommonWall(Room *, Room *);

	std::shared_ptr<Maze> mCurrentMaze{};
};
