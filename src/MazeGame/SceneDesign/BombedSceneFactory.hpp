#pragma once

#include "MazeGame/SceneDesign/AbstractSceneFactory.hpp"

class Wall;
class Room;

/// @implements Factory Method design pattern
class BombedSceneFactory : public AbstractSceneFactory {
  public:
	BombedSceneFactory();

	/// @note Factory Method
	virtual Wall *MakeWall() const;

	/// @note Factory Method
	virtual Room *MakeRoom(int n) const;
};
