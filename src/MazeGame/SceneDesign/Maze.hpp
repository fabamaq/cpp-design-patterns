#pragma once

class Room;

class Maze {
  public:
	Maze() = default;

	void AddRoom(Room *);

	Room *RoomNo(int) const;
};
