#pragma once

#include "MazeGame/SceneDesign/MapSite.hpp"

/// @implements Prototype design pattern
class Wall : public MapSite {
  public:
	Wall();

	void Enter() override;

  public:
	Wall(const Wall &);
	virtual Wall *Clone() const;
};
