#include "MazeGame/SceneDesign/Wall.hpp"

Wall::Wall() = default;

void Wall::Enter() {}

Wall::Wall(const Wall &) = default;

Wall *Wall::Clone() const { return new Wall(*this); }
