#pragma once

#include "MazeGame/SceneDesign/Wall.hpp"

/// @implements Prototype design pattern
class BombedWall : public Wall {
  public:
	BombedWall();
	BombedWall(const BombedWall &);
	virtual Wall *Clone() const;
	bool HasBomb();

  private:
	bool mBomb;
};
