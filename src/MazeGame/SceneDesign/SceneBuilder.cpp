#include "MazeGame/SceneDesign/SceneBuilder.hpp"

#include "MazeGame/SceneDesign/Maze.hpp"

SceneBuilder::SceneBuilder() = default;
SceneBuilder::~SceneBuilder() = default;

void SceneBuilder::BuildMaze() {}
void SceneBuilder::BuildRoom(int /*room*/) {}
void SceneBuilder::BuildDoor(int /*roomFrom*/, int /*roomTo*/) {}

Maze *SceneBuilder::GetMaze() { return nullptr; }
