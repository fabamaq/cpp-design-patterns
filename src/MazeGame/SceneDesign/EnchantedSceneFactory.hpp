#pragma once

#include "MazeGame/SceneDesign/AbstractSceneFactory.hpp"

class Room;
class Door;
class Spell;

/// @implements Factory Method design pattern
class EnchantedSceneFactory : public AbstractSceneFactory {
  public:
	EnchantedSceneFactory();

	/// @note Factory Method
	virtual Room *MakeRoom(int n) const;

	/// @note Factory Method
	virtual Door *MakeDoor(Room *r1, Room *r2) const;

  protected:
	Spell *CastSpell() const;
};
