#pragma once

#include "MazeGame/SceneDesign/Room.hpp"

class RoomWithABomb : public Room {
  public:
	RoomWithABomb();

	explicit RoomWithABomb(int n);
};
