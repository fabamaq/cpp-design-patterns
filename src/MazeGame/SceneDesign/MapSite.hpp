#pragma once

class MapSite {
  public:
	virtual ~MapSite() = default;
	virtual void Enter() = 0;
};
