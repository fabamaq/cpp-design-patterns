#pragma once

#include "MazeGame/SceneDesign/AbstractSceneFactory.hpp"

class Door;
class Maze;
class Room;
class Wall;

/// @implements Prototype design pattern
class ScenePrototypeFactory : public AbstractSceneFactory {
  public:
	ScenePrototypeFactory(Maze *, Wall *, Room *, Door *);

	Maze *MakeMaze() const override;
	Wall *MakeWall() const override;
	Room *MakeRoom(int) const override;
	Door *MakeDoor(Room *, Room *) const override;

  private:
	Maze *mPrototypeMaze;
	Room *mPrototypeRoom;
	Wall *mPrototypeWall;
	Door *mPrototypeDoor;
};
