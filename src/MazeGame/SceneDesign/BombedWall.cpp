#include "MazeGame/SceneDesign/BombedWall.hpp"

BombedWall::BombedWall() = default;

BombedWall::BombedWall(const BombedWall &other) : Wall(other) {
	mBomb = other.mBomb;
}

Wall *BombedWall::Clone() const { return new BombedWall(*this); }

bool BombedWall::HasBomb() { return false; }
