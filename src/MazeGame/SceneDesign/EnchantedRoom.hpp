#pragma once

#include "MazeGame/SceneDesign/Room.hpp"

class Spell;

class EnchantedRoom : public Room {
  public:
	explicit EnchantedRoom(int n, Spell *pSpell);
};
