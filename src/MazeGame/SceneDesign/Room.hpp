#include "MazeGame/SceneDesign/MapSite.hpp"

enum class Direction;

/// @implements Prototype design pattern
class Room : public MapSite {
  public:
	explicit Room(int roomNo);

	MapSite *GetSide(Direction) const;

	void SetSide(Direction, MapSite *);

	void Enter() override;

  public:
	Room(const Room &);
	virtual Room *Clone() const;

  private:
	MapSite *mSides[4]{};
	int mRoomNumber{};
};
