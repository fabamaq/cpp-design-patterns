#include "MazeGame/SceneDesign/EnchantedSceneFactory.hpp"

#include "MazeGame/SceneDesign/DoorNeedingSpell.hpp"
#include "MazeGame/SceneDesign/EnchantedRoom.hpp"

EnchantedSceneFactory::EnchantedSceneFactory() = default;

Room *EnchantedSceneFactory::MakeRoom(int n) const {
	return new EnchantedRoom(n, CastSpell());
}

Door *EnchantedSceneFactory::MakeDoor(Room *r1, Room *r2) const {
	return new DoorNeedingSpell(r1, r2);
}

Spell *EnchantedSceneFactory::CastSpell() const { return nullptr; }
