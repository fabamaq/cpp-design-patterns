#include "MazeGame/SceneDesign/ScenePrototypeFactory.hpp"

#include "MazeGame/SceneDesign/Door.hpp"
#include "MazeGame/SceneDesign/Maze.hpp"
#include "MazeGame/SceneDesign/Room.hpp"
#include "MazeGame/SceneDesign/Wall.hpp"

ScenePrototypeFactory::ScenePrototypeFactory(Maze *m, Wall *w, Room *r,
											 Door *d) {
	mPrototypeMaze = m;
	mPrototypeWall = w;
	mPrototypeRoom = r;
	mPrototypeDoor = d;
}

Maze *ScenePrototypeFactory::MakeMaze() const { return mPrototypeMaze; }

Wall *ScenePrototypeFactory::MakeWall() const {
	return mPrototypeWall->Clone();
}

Room *ScenePrototypeFactory::MakeRoom(int) const {
	return mPrototypeRoom->Clone();
}

Door *ScenePrototypeFactory::MakeDoor(Room *r1, Room *r2) const {
	Door *door = mPrototypeDoor->Clone();
	door->Initialize(r1, r2);
	return door;
}
