#include "MazeGame/SceneDesign/Door.hpp"

#include "MazeGame/SceneDesign/Room.hpp"

Door::Door(Room *r1, Room *r2) : mRoom1(r1), mRoom2(r2), mIsOpen(false) {}

Room *Door::OtherSideFrom(Room *) { return nullptr; }

void Door::Enter() {}

void Door::Initialize(Room *r1, Room *r2) {
	mRoom1 = r1;
	mRoom2 = r2;
}

Door::Door(const Door &other) {
	mRoom1 = other.mRoom1;
	mRoom2 = other.mRoom2;
}

Door *Door::Clone() const { return new Door(*this); }
