#include "MazeGame/SceneDesign/MapSite.hpp"

class Room;

/// @implements Prototype design pattern
class Door : public MapSite {
  public:
	Door(Room * = nullptr, Room * = nullptr);

	virtual void Initialize(Room *, Room *);

	void Enter() override;

	Room *OtherSideFrom(Room *);

  public:
	Door(const Door &);
	virtual Door *Clone() const;

  private:
	Room *mRoom1{};
	Room *mRoom2{};
	bool mIsOpen{};
};
