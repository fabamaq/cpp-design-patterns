#include "MazeGame/SceneDesign/BombedSceneFactory.hpp"

#include "MazeGame/SceneDesign/BombedWall.hpp"
#include "MazeGame/SceneDesign/RoomWithABomb.hpp"

BombedSceneFactory::BombedSceneFactory() = default;

Wall *BombedSceneFactory::MakeWall() const { return new BombedWall; }

Room *BombedSceneFactory::MakeRoom(int n) const { return new RoomWithABomb(n); }
