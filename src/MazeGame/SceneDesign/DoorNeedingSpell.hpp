#pragma once

#include "MazeGame/SceneDesign/Door.hpp"

class Room;

class DoorNeedingSpell : public Door {
  public:
	explicit DoorNeedingSpell(Room *, Room *);
};
