#include "MazeGame/SceneDesign/AbstractSceneFactory.hpp"

#include "MazeGame/SceneDesign/Door.hpp"
#include "MazeGame/SceneDesign/Maze.hpp"
#include "MazeGame/SceneDesign/Room.hpp"
#include "MazeGame/SceneDesign/Wall.hpp"

AbstractSceneFactory::AbstractSceneFactory() = default;

AbstractSceneFactory::~AbstractSceneFactory() = default;

Maze *AbstractSceneFactory::MakeMaze() const { return new Maze; }

Wall *AbstractSceneFactory::MakeWall() const { return new Wall; }

Room *AbstractSceneFactory::MakeRoom(int n) const { return new Room(n); }

Door *AbstractSceneFactory::MakeDoor(Room *r1, Room *r2) const {
	return new Door(r1, r2);
}
