#include "MazeGame/SceneDesign/Room.hpp"

Room::Room(int roomNo) : mRoomNumber(roomNo) {}

MapSite *Room::GetSide(Direction) const { return nullptr; }

void Room::SetSide(Direction, MapSite *) {}

void Room::Enter() {}

Room::Room(const Room &copy) {
	mSides[0] = copy.mSides[0];
	mSides[1] = copy.mSides[1];
	mSides[2] = copy.mSides[2];
	mSides[3] = copy.mSides[3];
	mRoomNumber = copy.mRoomNumber;
}

Room *Room::Clone() const { return new Room(*this); }
