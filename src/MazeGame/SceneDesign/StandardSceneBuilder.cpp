#include "MazeGame/SceneDesign/StandardSceneBuilder.hpp"

#include "MazeGame/SceneDesign/Direction.hpp"
#include "MazeGame/SceneDesign/Door.hpp"
#include "MazeGame/SceneDesign/Maze.hpp"
#include "MazeGame/SceneDesign/Room.hpp"
#include "MazeGame/SceneDesign/Wall.hpp"

StandardSceneBuilder::StandardSceneBuilder() = default;

void StandardSceneBuilder::BuildMaze() { mCurrentMaze.reset(new Maze()); }

void StandardSceneBuilder::BuildRoom(int n) {
	if (!mCurrentMaze->RoomNo(n)) {
		// Room *room				   = new Room(n);
		std::shared_ptr<Room> room = std::make_shared<Room>(n);
		mCurrentMaze->AddRoom(room.get());

		std::shared_ptr<Wall> northWall = std::make_shared<Wall>();
		std::shared_ptr<Wall> southWall = std::make_shared<Wall>();
		std::shared_ptr<Wall> eastWall	= std::make_shared<Wall>();
		std::shared_ptr<Wall> westWall	= std::make_shared<Wall>();

		room->SetSide(Direction::North, northWall.get());
		room->SetSide(Direction::South, southWall.get());
		room->SetSide(Direction::East, eastWall.get());
		room->SetSide(Direction::West, westWall.get());
	}
}

void StandardSceneBuilder::BuildDoor(int n1, int n2) {
	Room *r1 = mCurrentMaze->RoomNo(n1);
	Room *r2 = mCurrentMaze->RoomNo(n2);
	// Door *d	 = new Door(r1, r2);
	std::shared_ptr<Door> d = std::make_shared<Door>(r1, r2);

	r1->SetSide(CommonWall(r1, r2), d.get());
	r2->SetSide(CommonWall(r2, r1), d.get());
}

Maze *StandardSceneBuilder::GetMaze() { return mCurrentMaze.get(); }

Direction StandardSceneBuilder::CommonWall(Room * /*r1*/, Room * /*r2*/) {
	return Direction::West;
}
