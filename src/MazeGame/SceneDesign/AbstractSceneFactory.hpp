#pragma once

class Door;
class Maze;
class Room;
class Wall;

/// @implements Abstract Factory design pattern
class AbstractSceneFactory {
  public:
	AbstractSceneFactory();
	virtual ~AbstractSceneFactory();
	virtual Maze *MakeMaze() const;
	virtual Wall *MakeWall() const;
	virtual Room *MakeRoom(int n) const;
	virtual Door *MakeDoor(Room *r1, Room *r2) const;
};
