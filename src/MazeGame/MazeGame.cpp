#include "MazeGame/MazeGame.hpp"

// AbstractEnemyFactories
#include "MazeGame/EnemyDesign/EasyLevelEnemyFactory.hpp"
#include "MazeGame/EnemyDesign/HardLevelEnemyFactory.hpp"

// AbstractSceneFactories
#include "MazeGame/SceneDesign/BombedSceneFactory.hpp"
#include "MazeGame/SceneDesign/EnchantedSceneFactory.hpp"

#include "MazeGame/SceneDesign/StandardSceneBuilder.hpp"

/// ===========================================================================
/// UI.hpp
/// ===========================================================================

void ClearScreen() { system("clear"); }

/// ===========================================================================
/// MazeGame.hpp
/// ===========================================================================

int MazeGame::OnStaticMain() {

	bool finished{false};
	do {
		SetDifficulty();
		SetScene();

		bool done{false};
		do {
			ClearScreen();
			std::cout << "Selected Difficulty: " << to_string(mDifficulty)
					  << '\n';
			std::cout << "Selected Scene: " << to_string(mSceneLevel) << '\n';
			std::cout << "Confirm? (Y/N)\n";
			char choice;
			std::cin >> choice;

			if (std::tolower(choice) == 'y') {
				done	 = true;
				finished = true;
			} else if (std::tolower(choice) == 'n') {
				break; // while (!done)
			}
		} while (!done);
	} while (!finished);

	StandardSceneBuilder builder;
	CreateScene(builder);

	return EXIT_SUCCESS;
}

int MazeGame::OnStaticExit() { return EXIT_SUCCESS; }

void MazeGame::SetDifficulty() {
	ClearScreen();

	std::cout << "Select Difficulty:\n";
	std::cout << "1 - Easy\n";
	std::cout << "2 - Hard\n";

	bool done{false};
	while (!done) {
		std::cout << "> ";
		std::string choice;
		std::cin >> choice;
		std::cin.ignore();

		try {
			mDifficulty = static_cast<Difficulty>(std::stoi(choice) - 1);
		} catch (std::invalid_argument &error) {
			std::cerr << "Could not parse choice\n";
			continue;
		}

		switch (mDifficulty) {
		case Difficulty::Easy:
			mEnemyFactory.reset(new EasyLevelEnemyFactory);
			done = true;
			break;
		case Difficulty::Hard:
			done = true;
			mEnemyFactory.reset(new HardLevelEnemyFactory);
			break;
		default:
			std::cerr << "Invalid Difficulty\n";
		}
	}

	std::cout << "MazeGame running on " << to_string(mDifficulty) << '\n';
}

void MazeGame::SetScene() {
	ClearScreen();

	std::cout << "Select Scene:\n";
	std::cout << "1 - Bombed Maze\n";
	std::cout << "2 - Enchanted Maze\n";

	bool done{false};
	while (!done) {
		std::cout << "> ";
		std::string choice;
		std::cin >> choice;
		std::cin.ignore();

		try {
			mSceneLevel = static_cast<SceneLevel>(std::stoi(choice) - 1);
		} catch (std::invalid_argument &error) {
			std::cerr << "Could not parse choice\n";
			continue;
		}

		switch (mSceneLevel) {
		case SceneLevel::Bombed:
			mSceneFactory.reset(new BombedSceneFactory);
			done = true;
			break;
		case SceneLevel::Enchanted:
			mSceneFactory.reset(new EnchantedSceneFactory);
			done = true;
			break;
		default:
			std::cerr << "Invalid Scene\n";
		}
	}

	std::cout << "Starting scene " << to_string(mSceneLevel) << '\n';
}

void MazeGame::CreateScene(StandardSceneBuilder &builder) {
	builder.BuildMaze();
	builder.BuildRoom(1);
	builder.BuildRoom(2);
	builder.BuildDoor(1, 2);
	// return builder.GetMaze();
}

/// ---------------------------------------------------------------------------
/// @implements to_string overload for enum class Difficulty and SceneLevel
/// ---------------------------------------------------------------------------
std::string to_string(Difficulty d) {
	switch (d) {
	case Difficulty::Easy:
		return "Easy";
	case Difficulty::Hard:
		return "Hard";
	}
	return "";
}

std::string to_string(SceneLevel s) {
	switch (s) {
	case SceneLevel::Enchanted:
		return "Enchanted";
	case SceneLevel::Bombed:
		return "Bombed";
	}
	return "";
}

/// ===========================================================================
/// @implements Factory Method design pattern
/// ===========================================================================
Game *CreateGame() { return new MazeGame(); }
