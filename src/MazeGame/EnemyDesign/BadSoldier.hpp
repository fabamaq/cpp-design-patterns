#pragma once

#include "MazeGame/EnemyDesign/Soldier.hpp"

class BadSoldier : public Soldier {
  public:
	BadSoldier() = default;
	BadSoldier(const BadSoldier &) = default;
	BadSoldier *Clone() { return new BadSoldier(*this); }
};
