#pragma once

#include "StandardLibrary.hpp"

/// @implements Prototype design pattern
class Monster {
  public:
	virtual ~Monster()			 = default;
	Monster()					 = default;
	Monster(const Monster &)	 = default;
	Monster(Monster &&) noexcept = default;

	Monster &operator=(const Monster &) = default;
	Monster &operator=(Monster &&) noexcept = default;

	virtual Monster *Clone() = 0;

	virtual int GetHealth() const { return mHealth; }

	// Other stuff
  protected:
	int mHealth;
};
