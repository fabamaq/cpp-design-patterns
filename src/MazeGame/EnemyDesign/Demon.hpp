#pragma once

#include "MazeGame/EnemyDesign/Monster.hpp"

class Demon : public Monster {
  public:
	~Demon() override = default;
	explicit Demon(int health) { mHealth = health; }

	Demon(const Demon &)	 = default;
	Demon(Demon &&) noexcept = default;

	Demon &operator=(const Demon &) = default;
	Demon &operator=(Demon &&) noexcept = default;

	/// @note using Covariant Return Type
	virtual Demon *Clone() { return new Demon(mHealth); }
};
