#pragma once

#include "MazeGame/EnemyDesign/Monster.hpp"

class BadMonster : public Monster {
  public:
	~BadMonster()					   = default;
	BadMonster()					   = default;
	BadMonster(const BadMonster &)	   = default;
	BadMonster(BadMonster &&) noexcept = default;

	BadMonster &operator=(const BadMonster &) = default;
	BadMonster &operator=(BadMonster &&) noexcept = default;

	/// @note Covariant Return Type
	BadMonster *Clone() override { return new BadMonster(*this); }
};
