#pragma once

#include "MazeGame/EnemyDesign/Monster.hpp"

/// @implements Prototype design pattern
class Ghost : public Monster {
  public:
	~Ghost() override		 = default;
	Ghost(const Ghost &)	 = default;
	Ghost(Ghost &&) noexcept = default;

	Ghost(int health, int speed) : mSpeed(speed) { mHealth = health; }

	Ghost &operator=(const Ghost &) = default;
	Ghost &operator=(Ghost &&) noexcept = default;

	/// @note using Covariant Return Type
	virtual Ghost *Clone() { return new Ghost(*this); }

	int GetSpeed() const { return mSpeed; }

  private:
	int mSpeed;
};
