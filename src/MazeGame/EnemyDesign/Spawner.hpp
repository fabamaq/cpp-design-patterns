#pragma once

#include "MazeGame/EnemyDesign/Monster.hpp"

typedef Monster *(*SpawnCallback)();

class Spawner {
  public:
	Spawner() = default;
	Spawner(SpawnCallback spawn) : mSpawn(spawn) {}
	Spawner(Monster *prototype) : mPrototype(prototype) {}
	virtual ~Spawner() = default;

	Monster *CallSpawnFunction() { return mSpawn(); }
	virtual Monster *CloneMonster() { return mPrototype->Clone(); }

	virtual Monster *SpawnTemplate() { return nullptr; }

  private:
	Monster *mPrototype;
	SpawnCallback mSpawn;
};

template <class T> class SpawnerFor : public Spawner {
	int mHealth;

  public:
	SpawnerFor(int health) : mHealth(health) {}
	Monster *SpawnTemplate() { return new T(mHealth); }
};

// class GhostSpawner : public Spawner {
// public:
//    virtual Monster* spawnMonster() {
//        return new Ghost();
//    }
//};
//
// class DemonSpawner : public Spawner {
// public:
//    virtual Monster* spawnMonster() {
//        return new Demon();
//    }
//};

// You get the idea...
