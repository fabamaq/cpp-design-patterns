#pragma once

#include "MazeGame/EnemyDesign/AbstractEnemyFactory.hpp"

class HardLevelEnemyFactory : public AbstractEnemyFactory {
  public:
	Soldier *MakeSoldier() override;
	Monster *MakeMonster() override;
	SuperMonster *MakeSuperMonster() override;
};
