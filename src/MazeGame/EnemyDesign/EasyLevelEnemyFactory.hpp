#pragma once

#include "MazeGame/EnemyDesign/AbstractEnemyFactory.hpp"

class EasyLevelEnemyFactory : public AbstractEnemyFactory {
  public:
	Soldier *MakeSoldier() override;
	Monster *MakeMonster() override;
	SuperMonster *MakeSuperMonster() override;
};
