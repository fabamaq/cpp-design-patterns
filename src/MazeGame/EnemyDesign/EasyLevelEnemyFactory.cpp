#include "MazeGame/EnemyDesign/EasyLevelEnemyFactory.hpp"

#include "MazeGame/EnemyDesign/SillyMonster.hpp"
#include "MazeGame/EnemyDesign/SillySoldier.hpp"
#include "MazeGame/EnemyDesign/SillySuperMonster.hpp"

Soldier *EasyLevelEnemyFactory::MakeSoldier() { return new SillySoldier(); }
Monster *EasyLevelEnemyFactory::MakeMonster() { return new SillyMonster(); }
SuperMonster *EasyLevelEnemyFactory::MakeSuperMonster() {
	return new SillySuperMonster();
}
