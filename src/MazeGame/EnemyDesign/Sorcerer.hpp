#pragma once

#include "MazeGame/EnemyDesign/Monster.hpp"

class Sorcerer : public Monster {
  public:
	Sorcerer(int health) { mHealth = health; }

	virtual Monster *Clone() { return new Sorcerer(mHealth); }
};
