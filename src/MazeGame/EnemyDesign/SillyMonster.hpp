#pragma once

#include "MazeGame/EnemyDesign/Monster.hpp"

class SillyMonster : public Monster {
  public:
	SillyMonster() = default;
	SillyMonster(const SillyMonster &) = default;
	SillyMonster *Clone() { return new SillyMonster(*this); }
};
