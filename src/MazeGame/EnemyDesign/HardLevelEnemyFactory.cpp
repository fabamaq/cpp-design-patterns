#include "MazeGame/EnemyDesign/HardLevelEnemyFactory.hpp"

#include "MazeGame/EnemyDesign/BadMonster.hpp"
#include "MazeGame/EnemyDesign/BadSoldier.hpp"
#include "MazeGame/EnemyDesign/BadSuperMonster.hpp"

Soldier *HardLevelEnemyFactory::MakeSoldier() { return new BadSoldier(); }
Monster *HardLevelEnemyFactory::MakeMonster() { return new BadMonster(); }
SuperMonster *HardLevelEnemyFactory::MakeSuperMonster() {
	return new BadSuperMonster();
}
