#pragma once

class Soldier;
class Monster;
class SuperMonster;

/// @implements Abstract Factory design pattern
class AbstractEnemyFactory {
  public:
	virtual ~AbstractEnemyFactory() = default;
	virtual Soldier *MakeSoldier() = 0;
	virtual Monster *MakeMonster() = 0;
	virtual SuperMonster *MakeSuperMonster() = 0;
};
