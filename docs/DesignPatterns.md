# Design Patterns

Some guidelines from the book "Design Patterns: Elements of of Reusable Object-Oriented Software" from the Gang of Four.

<center><b>Take them with a grain of salt</b></center>

---
---

## Creational Patterns

---

### Abstract Factory

#### Intent

> Provide an interface for creating families of related or dependent objects without specifying their concrete classes.

---

### Builder

#### Intent

> Separate the construction of a complex object from its representation so the same construction process can create different representations.

---

### Factory Method

#### Intent

> Define an interface for creating an object, but let subclasses decide which class to instantiate.
> Factory Method lets a class defer instantiation to subclasses.

---

### Prototype

#### Intent

> Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype.

---

### Singleton

#### Intent

> Ensure a class only has one instance, and provide a global point of access to it.

---
---

## Structural Patterns

---

### Adapter

#### Intent

> Convert the interface of a class into another interface clients expect. Adapter lets classes work together that couldn't otherwise because of incompatible interfaces.

---

### Bridge

#### Intent

> Decouple an abstraction from its implementation so that the two can vary independently.

---

### Composite

#### Intent

> Compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and compositions of objects uniformly.

---

### Decorator

#### Intent

> Attach additional responsibilities to an object dynamically. Decorators provide an alternative to subclassing for extending functionality.

---

### Facade

#### Intent

> Provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use.

---

### Flyweight

#### Intent

> Use sharing to support large numbers of fine-grained objects efficiently.

---

### Proxy

#### Intent

> Provide a surrogate or placeholder for another object to control access to it.

---
---

## Behavioral Patterns

---

### Chain of Responsibility

#### Intent

> Avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request.
> Chain the receiving objects and pass the request along the chain until an object handles it.

---

### Command

#### Intent

> Encapsulate a request as an object, thereby letting you parameterize clients with different requests, queue or log requests, and support undoable operations.

---

### Interpreter

#### Intent

> Given a language, define a representation for its grammar along with an interpreter that uses the representation to interpret sentences in the language.

---

### Iterator

#### Intent

> Provide a way to access the elements of an aggregate object sequentially without exposing its underlying representation.

---

### Mediator

#### Intent

> Define an object that encapsulates how a set of objects interact.
> Mediator promotes loose coupling by keeping objects from referring to each other explicitly, and it lets you vary their interaction independently.

---

### Memento

#### Intent

> Without violating encapsulation, capture and externalize an object's internal state so that the object can be restored to this state later.

---

### Observer

#### Intent

> Define a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically.

---

### State

#### Intent

> Allow an object to alter its behavior when its internal state changes. The object will appear to change its class.

---

### Strategy

#### Intent

> Define a family of algorithms, encapsulate each one, and make them interchangeable.
> Strategy lets the algorithm vary independently from clients that use it.

---

### Template Method

#### Intent

> Define the skeleton of an algorithm in an operation, deferring some steps to subclasses.
> Template Method lets subclasses redefine certain steps of an algorithm without changing the algorithm's structure.

---

### Visitor

#### Intent

> Represent an operation to be performed on the elements of an object structure.
> Visitor lets you defined a new operation without changing the classes of the elements on which it operates.

---

### Bytecode

#### Intent

> Give behavior the flexibility of data by encoding it as instructions for a virtual machine.

---

### Subclass Sandbox

#### Intent

> Define behavior in a subclass using a set of operations provided by its base class.

---

### Type Object

#### Intent

> Allow the flexible creation of new "classes" by creating a class, each instance of which represents a different type of object.

---
---

## Sequencing Patterns

---

### Double Buffer

#### Intent

> Cause a series of sequential operations to appear instantaneous or simultaneous.

---

### Game Loop

#### Intent

> Decouple the progression of game time from user input and processor speed.

---

### Update Method

#### Intent

> Simulate a collection of independent objects by telling each to process one frame of behavior at a time.

---
---

## Decoupling Patterns

---

### Component

#### Intent

> Allow a single entity to span multiple domains without coupling the domains to each other.

---

### Event Queue

#### Intent

> Decouple when a message or event is sent from when it is processed.

---

### Service Locator

#### Intent

> Provide a global point of access to a service without coupling users to the concrete class that implements it.

---
---

## Optimization Patterns

---

### Data Locality

#### Intent

> Accelerate memory access by arranging data to take advantage of CPU caching.

---

### Dirty Flag

#### Intent

> Avoid unnecessary work by deferring it until the result is needed.

---

### Object Pool

#### Intent

> Improve performance and memory use by reusing objects from a fixed pool instead of allocating and freeing them individually.

---

### Spatial Partition

#### Intent

> Efficiently locate objects by storing them in a data structure organized by their positions.
