# C++ Design Patterns KS

A C++ Design Patterns Knowledge Sharing experience.

---
## DISCLAIMER
> This project is under constructions.
---

## Download

Download the zip archive and extract the files or clone the project:

```bash
  git clone --recurse-submodules git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-design-patterns.git
```

### Build Requirements

* [g++](https://gcc.gnu.org/) (4.8 or higher)
* [cmake](https://cmake.org/) (3.20.0 or higher)
* * [make](https://www.gnu.org/software/make/) (4.2.1 or higher)

### Build Process

With `cmake`

```bash
# From the root directory

# Generate the build system
# run cmake . -B <build_directory> -D CMAKE_BUILD_TYPE=[Debug|Release]
# Example
cmake . -B build/Debug/ -D CMAKE_BUILD_TYPE=Debug

# Build the project
# run cmake --build <build_directory> --target <TARGET>
# Example
cmake --build/Debug --target MazeGame
```

Or you can setup your Integrated Development Environment (IDE) with cmake but, instructions for that, are outside the scope of this README (which can be updated with images if you want).

## Main Projects

If using `cmake`, all projects include a pre_compiled_headers library of the C++ Standard Library and a project_global_settings Interface Target to be linked to individual projects. 

### Game Engine

A library with an incomplete set of modules and components from Game Engine Architecture

### Maze Game

An example Maze game that exercises the Game Engine library and its Design Patterns.


## Examples Projects

The examples folder contains example code from three books:

### **Design Patterns: Elements of Reusable Object-Oriented Software**

* Design Patterns is a book from the Gang of Four* that catalogs simple solutions to commonly occuring desing problems.
* You can buy the book at [Amazon](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612/ref=sr_1_1?keywords=design+patterns&qid=1638266542&sr=8-1).

### **Hands-On Design Patterns with C++: Solve common C++ problems with modern design patterns and build robust applications**

* Hands-On Design Patterns with C++ is a book from Fedor G. Pikus that offers an extensive coverage on concepts such as OOP, functional programming, generic programming, and STL.
* You can buy the book at [Amazon](https://www.amazon.com/Hands-Design-Patterns-reusable-maintainable/dp/1788832566/ref=sr_1_3?keywords=design+patterns&qid=1638266621&sr=8-3).

### **Game Programming Patterns**

* Game Programming Patterns is a boo from Robert Nystrom that collects proven patterns to untangle and optimize your game.
* You can buy the book at [Amazon](https://www.amazon.com/Game-Programming-Patterns-Robert-Nystrom/dp/0990582906/ref=sr_1_14?keywords=design+patterns&qid=1638266621&sr=8-14).
* You can also read the book at https://gameprogrammingpatterns.com/

## External Projects

At the moment, the external folder contains the Google Test Framework only, which is added to cmake automatically.

## Authors

- [Pedro André Oliveira](https://www.linkedin.com/in/pedroandreoliveira/)

## Contributing

Contributions are always welcome!

## Related Knowledge Sharings

* [C++ Test-Driven Development](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-test-driven-development.git)
* [C++ Design Patterns KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-design-patterns.git)
* [C++ Clean Architecture KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-clean-architecture.git)

* [C++ Fundamentals KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-fundamentals.git)
* [C++ Intermediate KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-intermediate.git)
* [C++ Advanced KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-advanced.git)
* [C++ Mastery KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-mastery.git)
